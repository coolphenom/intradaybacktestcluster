/*
 * DatabaseProcessing.h
 *
 *  Created on: Mar 6, 2019
 *      Author: pavlotkachenko
 */

#ifndef DATABASEPROCESSING_H_
#define DATABASEPROCESSING_H_

#include "../IntradayMaintenance/StockInfo.h"
#include "../IntradayMaintenance/Timeframe.h"

#include "../src/include_libs/mysql/mysql_connection.h"
#include "../src/include_libs/mysql/cppconn/driver.h"
#include "../src/include_libs/mysql/cppconn/exception.h"
#include "../src/include_libs/mysql/cppconn/resultset.h"
#include "../src/include_libs/mysql/cppconn/statement.h"

#include <string>
#include <list>
#include <ctime>
#include <vector>
#include <boost/date_time/gregorian/gregorian.hpp>


namespace IntradayBacktest{


enum ReturnsEnum
{
	PositiveReturns,
	NegativeReturns,
	Neutral
};

class DatabaseProcessing{
public:
	DatabaseProcessing() : m_name("PAVLO"),
							m_dbIp("10.1.1.1"),
							m_port(65000){};
	DatabaseProcessing(const std::string& name, const std::string& ip, const int& port);
	virtual ~DatabaseProcessing() {};
	boost::gregorian::date GetTickerTradingPeriodStart(const std::string& database, const std::string& table, const std::string& columnName);
	boost::gregorian::date GetTickerTradingPeriodFinish(const std::string& database, const std::string& table, const std::string& columnName);

	int GetNumberOfEntries(const std::string& table, std::string& database);
	std::vector<std::string> GetDatabases();
	std::vector<std::tuple<int,int,double>> GetTickerReturns(const std::string& ticker, const IntradayMaintenance::Timeframe& timeframe,
			std::string& database, const boost::gregorian::date& st, const ReturnsEnum& ret);
	std::vector<std::tuple<int,double>> GetDailyQuotes(const std::string& ticker);
	std::vector<std::string> GetTables(const std::string& database);
	std::vector<std::tuple<int,int,IntradayMaintenance::StockInfo>> GetTickerHistory(const std::string& ticker,
			const std::string& database, const IntradayMaintenance::Timeframe& timeframe, const boost::gregorian::date& timeToStart, const boost::gregorian::date& timeToFinish);
	std::vector<std::tuple<int,int,double>> GetTickerHistory(const std::string& ticker,
			const std::string& database, const IntradayMaintenance::Timeframe& timeframe, const IntradayMaintenance::StockInfoFieldsEnum& field, const boost::gregorian::date& timeToStart, const boost::gregorian::date& timeToFinish);
	std::vector<std::string> GetTickers(const std::string& database, const std::string& table);
	bool PerformBasicDatabaseQuery(const std::string& query);
	std::vector<long double> GetDoubleValueQuery(const std::string& query, const std::string& database);

private:
	std::string m_name;
	std::string m_dbIp;
	int m_port;

	template <typename T>
	std::vector<T> PerformDatabaseQuery(const std::string& query, const std::string& database);

	template <typename T>
	std::vector<T> DefineResult(const std::string& sqlQuery, const std::string& database = "", const IntradayMaintenance::StockInfoFieldsEnum& column = IntradayMaintenance::StockInfoFieldsEnum::No, const IntradayMaintenance::Timeframe& timeframe = IntradayMaintenance::Timeframe::OneMinute);

};
}





#endif /* DATABASEPROCESSING_H_ */
