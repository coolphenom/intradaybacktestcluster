/*
 * DatabaseAnalysis.h
 *
 *  Created on: Mar 7, 2019
 *      Author: pavlotkachenko
 */

#ifndef DATABASEANALYSIS_H_
#define DATABASEANALYSIS_H_

#include "DatabaseProcessing.h"
#include "../IntradayMaintenance/StockInfo.h"
#include "../IntradayMaintenance/Timeframe.h"
#include "../IntradayMaintenance/CustomExceptionHandler.h"

#include <vector>
#include <string>
#include <tuple>
#include <ctime>
#include <set>
#include <boost/date_time/gregorian/gregorian.hpp>

namespace{
std::pair<int,int> ToOADateAndMinutes(const boost::gregorian::date& time)
{
	boost::gregorian::date dStart{1899, 12, 31};
	boost::gregorian::date_period d{dStart, time};
	boost::gregorian::date_duration dd = d.length();
	return std::make_pair(dd.days(), 0);
}
}

namespace IntradayBacktest {

class DatabaseAnalysis {
public:
	DatabaseAnalysis();
	DatabaseAnalysis(const std::string& name, const std::string& ip, const int& port);
	virtual ~DatabaseAnalysis();
	void GetInternalShareInfos(const std::string& database, const std::string ticker, const boost::gregorian::date& timeStart, const boost::gregorian::date& timeFinish);
	const std::vector<std::tuple<int,int,double>>& GetValues() const;
	std::vector<double>::const_iterator GetValuesBegin() const;
	const std::vector<std::vector<std::tuple<int,int,IntradayMaintenance::StockInfo>>>& GetCandles() const;
	std::vector<double>::const_iterator GetValuesEnd() const;
	size_t GetValuesCount() const;
	void SetValues(const std::vector<std::tuple<int,int,double>>& values);
	void SetValues(const std::vector<std::tuple<int,int,IntradayMaintenance::StockInfo>>& tuple);
	void SetCandles(const std::vector<std::vector<std::tuple<int,int,IntradayMaintenance::StockInfo>>>& values);
	const std::vector<double>& GetSums() const;
	const std::vector<double>& GetSumsSq() const;
	std::vector<double> CalculateValues(const std::vector<std::tuple<int,int,double>>& values);
	double GetCurrentSMASimple(const int& i, const int& sma) const;
	double GetCurrentStd(const int& i, const int& std) const;
	std::vector<std::tuple<int,int,double>> GetTickerReturns(const std::string& ticker, const IntradayMaintenance::Timeframe& timeframe,
			std::string& database, const boost::gregorian::date& st, const ReturnsEnum& ret);
	std::vector<std::tuple<int, int, IntradayMaintenance::StockInfo>> GetTableCandleInformation(const std::string& ticker, const IntradayMaintenance::Timeframe& timeframe,
			std::string& database, const boost::gregorian::date& st, const bool& forex = false);
	std::vector<IntradayMaintenance::StockInfo> GetTableInformation(const std::string& ticker, const IntradayMaintenance::Timeframe& timeframe,
				std::string& database, const boost::gregorian::date& st, const bool& forex = false);
	std::vector<std::tuple<int,int,double>> GetTableInformation(const std::string& ticker, const IntradayMaintenance::StockInfoFieldsEnum& field, const IntradayMaintenance::Timeframe& timeframe,
				std::string& database, const boost::gregorian::date& st, const bool& forex = false);
	std::vector<IntradayMaintenance::StockInfo> Normalize(const std::vector<IntradayMaintenance::StockInfo>& info, const IntradayMaintenance::Timeframe& timeframe);

	template <typename Type>
	std::vector<std::tuple<int,int,Type>> Normalize(const std::vector<std::tuple<int,int,Type>>& info, const IntradayMaintenance::Timeframe& timeframe, const bool& stock = true)
	{
		function = __func__;
		std::vector<std::tuple<int,int, Type>> list;
		if (info.size() > 0)
		{
			int daysStart = std::get<0>(info[0]);
			int minStart = std::get<1>(info[0]);
			std::pair<int,int> start = std::make_pair(daysStart, minStart);
			int daysFinish = std::get<0>(*prev(info.end()));
			int minFinish = std::get<1>(*prev(info.end()));
			std::pair<int,int> finish = std::make_pair(daysFinish, minFinish);
			//int check = 1440 - start.second + (finish.first - start.first - 1)*1440 + finish.second + 1;
			int step = IntradayMaintenance::TimeframeHelperMultiplier(timeframe);
			int day = 0;
			int min = 0;

			if (!stock)
			{
				int d = 0;
				int m = 0;
				if (minStart - step >= 0)
				{
					m = minStart - step;
					d = daysStart;
				}
				else
				{
					m = 1440 + (minStart - step);
					d = daysStart - 1;
				}
				auto val = std::make_tuple(d, m, Type());
				list.push_back(val);
			}

			Type temp;
			for (size_t i = 0; i < info.size(); ++i)
			{
				if (day == 0 && min == 0)
				{
					list.push_back(info[i]);
					temp = stock ? std::get<2>(info[i]) : Type();
					day = std::get<0>(info[i]);
					min = std::get<1>(info[i]);
					min += step;
					continue;
				}
				Start:
				if (std::get<0>(info[i]) == day)
				{
					if (std::get<1>(info[i]) == min)
					{
						list.push_back(info[i]);
						temp = stock ? std::get<2>(info[i]) : Type();
						min += step;
						continue;
					}
					//double val = std::get<2>(*(prev(list.end())));
					//Type val = std::get<2>(*(prev(list.end())));
					temp = std::get<2>(*(prev(list.end())));
					while (std::get<1>(info[i]) != min && min < 1440)
					{
						Type prox = stock ? InterpolateData(temp, false, step) : Type();
						list.push_back(std::make_tuple(day, min, prox));
						temp = prox;
						min += step;
					}
					list.push_back(info[i]);
					temp = stock ? std::get<2>(info[i]) : Type();
					min += step;
				}
				else
				{
					//double val = std::get<2>(*(prev(list.end())));
					temp = std::get<2>(*(prev(list.end())));
					while (min < 1440)
					{
						Type prox = stock ? InterpolateData(temp, false, step) : Type();
						list.push_back(std::make_tuple(day, min, prox));
						temp = prox;
						min += step;
					}
					Type prox = stock ? InterpolateData(temp, true, step) : Type();
					temp = prox;
					min = 0;
					++day;
					goto Start;
				}
			}
		}

		/*if (list.size() != check)
		{
			std::cout << std::get<0>((*list.begin())) << " " << std::get<1>((*list.begin())) << std::endl;
			std::cout << std::get<0>((*prev(list.end()))) << " " << std::get<1>((*prev(list.end()))) << std::endl;
			std::cout << check << " " << list.size() << std::endl;
			for (const auto& item: list)
				std::cout << std::get<0>(item) << "," << std::get<1>(item)<< std::endl;
		}*/
		return list;
	}


	template <typename Type>
	void SynchronizeVectors(std::vector<std::vector<std::tuple<int,int,Type>>>& info, const boost::gregorian::date& startFromTime)
	{
		std::set<std::pair<int,int>> leftBorder;
		std::set<std::pair<int,int>> rightBorder;
		std::pair<int,int> startTime = ToOADateAndMinutes(startFromTime);
		for (const auto& item : info)
		{
			leftBorder.insert(std::make_pair(std::get<0>(*item.begin()),std::get<1>(*item.begin())));
			rightBorder.insert(std::make_pair(std::get<0>(*prev(item.end())),std::get<1>(*prev(item.end()))));
		}
		leftBorder.insert(startTime);
		std::pair<int,int> left = *prev(leftBorder.end());
		std::pair<int,int> right = *rightBorder.begin();
		std::vector<std::vector<std::tuple<int,int,Type>>> infoNew;
		for (const std::vector<std::tuple<int,int,Type>>& item : info)
		{
			std::vector<std::tuple<int,int,Type>> inf;
			for (const std::tuple<int,int,Type>& it: item)
			{
				int day = std::get<0>(it);
				int minute = std::get<1>(it);
				std::pair<int,int> temp = std::make_pair(day, minute);
				if (left < temp && temp < right)
					inf.push_back(it);
			}
			infoNew.push_back(inf);
		}
		info = infoNew;
		if (info[0].size() != info[1].size())
		{
			for (const auto& item: info[0])
				std::cout << std::get<1>(item)<< std::endl;
			for (const auto& item: info[1])
				std::cout << std::get<1>(item)<< std::endl;

			std::cout << "Synchronization failed!" << std::endl;
			std::cout << "1st :" << std::get<0>(*info[0].begin()) << " " << std::get<1>(*info[0].begin()) << " " <<
					std::get<0>(*prev(info[0].end())) << " " << std::get<1>(*prev(info[0].end())) << std::endl;
			std::cout << "2nd :" << std::get<0>(*info[1].begin()) << " " << std::get<1>(*info[1].begin()) << " " <<
							std::get<0>(*prev(info[1].end())) << " " << std::get<1>(*prev(info[1].end())) << std::endl;
		}
	}
	void SynchronizeVectors(std::vector<std::vector<std::tuple<int,int,IntradayMaintenance::StockInfo>>>& info, const boost::gregorian::date& startFromTime);
private:
	IntradayMaintenance::StockInfo InterpolateStockInfo(const IntradayMaintenance::StockInfo& info, const bool& nextD, const int& step);
	//stub
	double InterpolateStockInfo(const double& info, const bool& nextD, const int& step) {return 0;};
	template <typename Type>
	Type InterpolateData(const Type& type, const bool& nextD, const int& step)
	{
		if (typeid(Type) == typeid(double))
			return type;
		else if (typeid(Type) == typeid(IntradayMaintenance::StockInfo))
		{
			return InterpolateStockInfo(type, nextD, step);
		}

	}
	DatabaseProcessing* m_processor;
	std::vector<IntradayMaintenance::StockInfo> m_shares;
	std::vector<std::tuple<int,int,double>> m_tupValues;
	std::vector<std::tuple<int,int,IntradayMaintenance::StockInfo>> m_tupStocks;
	std::vector<double> m_values;
	std::vector<std::vector<std::tuple<int,int, IntradayMaintenance::StockInfo>>> m_candles;
	std::vector<double> m_sums;
	std::vector<double> m_sumsSq;
};

} /* namespace IntradayBacktest */

#endif /* DATABASEANALYSIS_H_ */
