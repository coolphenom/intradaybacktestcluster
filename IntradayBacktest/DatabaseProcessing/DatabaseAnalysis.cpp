/*
 * DatabaseAnalysis.cpp
 *
 *  Created on: Mar 7, 2019
 *      Author: pavlotkachenko
 */

#include "DatabaseAnalysis.h"
#include "DatabaseProcessing.h"
#include "../src/LogDuration.h"

#include <tuple>
#include <math.h>
#include <set>

namespace IntradayBacktest {

DatabaseAnalysis::DatabaseAnalysis(const std::string& name, const std::string& ip, const int& port)
{
	function = __func__;
	SIGNALEXCEPTION(signalHandler);
	m_processor = new DatabaseProcessing(name, ip, port);
}

DatabaseAnalysis::DatabaseAnalysis() {
	function = __func__;
	SIGNALEXCEPTION(signalHandler);
	// TODO Auto-generated constructor stub
	m_processor = new DatabaseProcessing();
}

/*void DatabaseAnalysis::GetInternalShareInfos(const std::string& database, const std::string ticker, const boost::gregorian::date& timeStart, const boost::gregorian::date& timeFinish)
{
	std::vector<IntradayMaintenance::StockInfo> lst = m_processor->GetTickerHistory(ticker, database, timeStart, timeFinish);
	m_shares = std::vector<IntradayMaintenance::StockInfo>(std::make_move_iterator(lst.begin()), std::make_move_iterator(lst.end()));
}*/

void DatabaseAnalysis::SetValues(const std::vector<std::tuple<int,int,double>>& tuple)
{
	m_tupValues = tuple;
	m_sums = std::vector<double>(m_tupValues.size());
	m_sumsSq = std::vector<double>(m_tupValues.size());
	m_values = std::vector<double>(m_tupValues.size());
	for(size_t i = 0; i < m_tupValues.size(); ++i)
	{
		m_values[i] = std::get<2>(m_tupValues[i]);
		m_sums[i] = i == 0 ? 0 : m_sums[i-1] + std::get<2>(m_tupValues[i-1]);
		m_sumsSq[i] = i == 0 ? 0 : m_sumsSq[i-1] + std::pow(std::get<2>(m_tupValues[i-1]), 2);
	}
}

size_t DatabaseAnalysis::GetValuesCount() const
{
	return m_values.size();
}

void DatabaseAnalysis::SetValues(const std::vector<std::tuple<int,int,IntradayMaintenance::StockInfo>>& tuple)
{
	m_tupStocks = tuple;
	m_tupValues = std::vector<std::tuple<int,int,double>>(m_tupStocks.size());
	m_sums = std::vector<double>(m_tupStocks.size());
	m_sumsSq = std::vector<double>(m_tupStocks.size());
	m_values = std::vector<double>(m_tupStocks.size());
	for(size_t i = 0; i < m_tupStocks.size(); ++i)
	{
		m_tupValues[i] = std::make_tuple(std::get<0>(m_tupStocks[i]), std::get<1>(m_tupStocks[i]),
				std::get<2>(m_tupStocks[i]).GetClose());
		m_values[i] = std::get<2>(m_tupStocks[i]).GetClose();
		m_sums[i] = i == 0 ? 0 : m_sums[i-1] + std::get<2>(m_tupStocks[i-1]).GetClose();
		m_sumsSq[i] = i == 0 ? 0 : m_sumsSq[i-1] + std::pow(std::get<2>(m_tupStocks[i-1]).GetClose(), 2);
	}
}

void DatabaseAnalysis::SetCandles(const std::vector<std::vector<std::tuple<int,int,IntradayMaintenance::StockInfo>>>& values)
{
	m_candles = values;
}

const std::vector<std::tuple<int,int,double>>& DatabaseAnalysis::GetValues() const
{
	return m_tupValues;
}

std::vector<double>::const_iterator DatabaseAnalysis::GetValuesBegin() const
{
	return m_values.begin();
}

const std::vector<std::vector<std::tuple<int,int,IntradayMaintenance::StockInfo>>>& DatabaseAnalysis::GetCandles() const
{
	return m_candles;
}

std::vector<double>::const_iterator DatabaseAnalysis::GetValuesEnd() const
{
	return m_values.end();
}

const std::vector<double>& DatabaseAnalysis::GetSums() const
{
	return m_sums;
}

const std::vector<double>& DatabaseAnalysis::GetSumsSq() const
{
	return m_sumsSq;
}

double DatabaseAnalysis::GetCurrentSMASimple(const int& i, const int& minutes) const
{
	return i < minutes ? std::get<2>(GetValues()[i]) : (GetSums()[i] - GetSums()[i - minutes])/minutes;
}

double DatabaseAnalysis::GetCurrentStd(const int& i, const int& std) const
{
	return i < std ? 0 : std::sqrt((GetSumsSq()[i] - GetSumsSq()[i - std] -
			2*GetCurrentSMASimple(i, std)*(GetSums()[i] - GetSums()[i-std]) +
			std*std::pow(GetCurrentSMASimple(i,std), 2))/(std - 1));
}

//std::vector<IntradayMaintenance::StockInfo> DatabaseAnalysis::GetTableInformation(const std::string& ticker, const IntradayMaintenance::Timeframe& timeframe,
//				std::string& database, const boost::gregorian::date& st, const bool& forex)
//{
//	return Normalize(m_processor->GetTickerHistory(ticker, database, st, boost::gregorian::date()), timeframe);
//}

std::vector<std::tuple<int,int,double>> DatabaseAnalysis::GetTickerReturns(const std::string& ticker, const IntradayMaintenance::Timeframe& timeframe,
		std::string& database, const boost::gregorian::date& st, const ReturnsEnum& ret)
{
	auto vec = m_processor->GetTickerReturns(ticker, timeframe, database, st, ret);
	return Normalize(vec, timeframe, false);
}

std::vector<std::tuple<int, int, IntradayMaintenance::StockInfo>> DatabaseAnalysis::GetTableCandleInformation(const std::string& ticker, const IntradayMaintenance::Timeframe& timeframe,
			std::string& database, const boost::gregorian::date& st, const bool& forex)
{
	std::vector<std::tuple<int,int,IntradayMaintenance::StockInfo>> vec;
	try
	{
		vec = m_processor->GetTickerHistory(ticker, database, timeframe, st, boost::gregorian::date());
		return Normalize(vec, timeframe);
	}
	catch (std::exception& ex)
	{
		return vec;
	}

}

IntradayMaintenance::StockInfo DatabaseAnalysis::InterpolateStockInfo(const IntradayMaintenance::StockInfo& info, const bool& nextD, const int& step)
{
	return IntradayMaintenance::StockInfo(info, nextD, step);
}

std::vector<std::tuple<int,int,double>> DatabaseAnalysis::GetTableInformation(const std::string& ticker, const IntradayMaintenance::StockInfoFieldsEnum& field, const IntradayMaintenance::Timeframe& timeframe,
				std::string& database, const boost::gregorian::date& st, const bool& forex)
{
	std::vector<std::tuple<int,int,double>> vec;
	vec = m_processor->GetTickerHistory(ticker, database, timeframe, field, st, boost::gregorian::date());
	return Normalize(vec, timeframe);
}

bool operator<(const std::pair<int,int>& lhs, const std::pair<int,int>& rhs)
{
	return lhs.first < rhs.first || (lhs.first == rhs.first && lhs.second < rhs.second);
}


std::vector<IntradayMaintenance::StockInfo> DatabaseAnalysis::Normalize(const std::vector<IntradayMaintenance::StockInfo>& info, const enum IntradayMaintenance::Timeframe& timeframe)
{
	function = __func__;
	int step = IntradayMaintenance::TimeframeHelperMultiplier(timeframe);
	std::vector<std::tuple<IntradayMaintenance::StockInfo, int,int>> infoTemp;
	for (size_t i = 0; i < info.size(); ++i)
	{
		infoTemp.push_back(std::make_tuple(info[i], info[i].GetOADate(), info[i].GetTimeMinutes()));
	}
	std::vector<IntradayMaintenance::StockInfo> list;
	int day = 0;
	int min = 0;
	IntradayMaintenance::StockInfo temp;
	for (size_t i = 0; i < infoTemp.size(); ++i)
	{
		if(day == 0 && min == 0)
		{
			list.push_back(std::get<0>(infoTemp[i]));
			temp = std::get<0>(infoTemp[i]);
			day = std::get<1>(infoTemp[i]);
			min = std::get<2>(infoTemp[i]);
			min += step;
			continue;
		}
		Start:
		if (std::get<1>(infoTemp[i]) == day)
		{
			if (std::get<2>(infoTemp[i]) == min)
			{
				list.push_back(std::get<0>(infoTemp[i]));
				temp = std::get<0>(infoTemp[i]);
				min += step;
				continue;
			}
			while (std::get<2>(infoTemp[i]) != min)
			{
				auto prox = IntradayMaintenance::StockInfo(temp, false, step);
				list.push_back(prox);
				temp = prox;
				min += step;
			}
			list.push_back(std::get<0>(infoTemp[i]));
			temp = std::get<0>(infoTemp[i]);
			min += step;
		}
		else
		{
			while (min != 1440)
			{
				auto prox = IntradayMaintenance::StockInfo(temp, false, step);
				list.push_back(prox);
				temp = prox;
				min += step;
			}
			auto prox = IntradayMaintenance::StockInfo(temp, true, step);
			temp = prox;
			min = 0;
			++day;
			goto Start;
		}
	}
	return list;
}

DatabaseAnalysis::~DatabaseAnalysis() {
	delete m_processor;
	// TODO Auto-generated destructor stub
}

} /* namespace IntradayBacktest */
