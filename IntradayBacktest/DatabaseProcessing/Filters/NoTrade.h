/*
 * NoTrade.h
 *
 *  Created on: Mar 13, 2019
 *      Author: pavlotkachenko
 */

#ifndef FILTERS_NOTRADE_H_
#define FILTERS_NOTRADE_H_

#include "AbstractFilter.h"
#include <vector>
#include <utility>
#include <ctime>

class NoTrade: public AbstractFilter {
public:
	NoTrade(const time_t& start, const time_t& finish, const int& startFrame, const int& maxFrame);
	virtual ~NoTrade();
	//Signal GetCloseSignal(const int& time, const Signal& previousSignal) override;
	//Signal GetEntrySignal(const int& time) override;
	bool IsEnabled(const size_t& time) override;
	virtual std::string ToJSON() override;
private:
	std::vector<std::pair<int,int>> m_borders;
	int m_startFrame;
};

#endif /* FILTERS_NOTRADE_H_ */
