/*
 * VolatilityPercentile.h
 *
 *  Created on: Feb 3, 2020
 *      Author: root
 */

#ifndef FILTERS_VOLATILITYPERCENTILE_H_
#define FILTERS_VOLATILITYPERCENTILE_H_

#include "Indicators/AbstractIndicator.h"
#include "../DatabaseAnalysis.h"
#include "../../IntradayMaintenance/StatisticsHelper.h"

#include <vector>
#include <utility>

class VolatilityPercentile: public AbstractIndicator {
public:
	VolatilityPercentile(const int& frames, const IntradayBacktest::DatabaseAnalysis& trader);
	double GetValue(const int& index) const override;
	void Compare(const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& values) override;
	double GetFirstDerivativeValue(const int& index) const override;
	std::string ToJSON() override;
	const std::vector<double>& GetValues() const override;
	virtual ~VolatilityPercentile();
private:
	std::vector<double> m_std;
	std::vector<double> m_stdPercentile;
	int m_frames;
};

#endif /* FILTERS_VOLATILITYPERCENTILE_H_ */
