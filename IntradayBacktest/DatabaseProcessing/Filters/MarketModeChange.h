/*
 * MarketModeChange.h
 *
 *  Created on: Nov 13, 2019
 *      Author: root
 */

#ifndef FILTERS_MARKETMODECHANGE_H_
#define FILTERS_MARKETMODECHANGE_H_

#include "Indicators/AbstractIndicator.h"
#include "AbstractFilter.h"
#include "../../IntradayMaintenance/CustomExceptionHandler.h"

#include <vector>
#include <unordered_map>

class MarketModeChange : public AbstractFilter{
public:
	MarketModeChange(const std::vector<AbstractIndicator*>& indicators, const std::unordered_map<size_t,size_t>& timeframeMapper,
			const StrategyMode& mode);
	virtual bool IsEnabled(const size_t& time) override;
	virtual Signal GetEntrySignal(const size_t& time) override;
	virtual Signal GetCloseSignal(const size_t& time, const Signal& previousSignal) override;
	virtual ~MarketModeChange();
	void Test(const AbstractIndicator* ind, const int& i);
	virtual std::string ToJSON() override;
	virtual void ModifyFilter() override;
private:
	//SSMA, EMA
	std::vector<AbstractIndicator*> m_indicators;
	std::unordered_map<size_t,size_t> m_timeFrameMapper;
};

#endif /* FILTERS_MARKETMODECHANGE_H_ */
