/*
 * TripleMarketModeFilter.h
 *
 *  Created on: Nov 27, 2019
 *      Author: root
 */

#ifndef FILTERS_TRIPLEMARKETMODEFILTER_H_
#define FILTERS_TRIPLEMARKETMODEFILTER_H_


#include "Indicators/AbstractIndicator.h"
#include "AbstractFilter.h"

#include <vector>

class TripleMarketModeFilter : public AbstractFilter{
public:
	TripleMarketModeFilter(const std::vector<AbstractIndicator*>& indicators, const StrategyMode& mode);
	virtual bool IsEnabled(const size_t& time) override;
	virtual Signal GetEntrySignal(const size_t& time) override;
	virtual Signal GetCloseSignal(const size_t& time, const Signal& previousSignal) override;
	virtual ~TripleMarketModeFilter();
	virtual std::string ToJSON() override;
	virtual void ModifyFilter() override;
private:
	//SSMA, EMA
	std::vector<AbstractIndicator*> m_indicators;
};

#endif /* FILTERS_TRIPLEMARKETMODEFILTER_H_ */
