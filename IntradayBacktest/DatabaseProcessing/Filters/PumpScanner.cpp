/*
 * PumpScanner.cpp
 *
 *  Created on: Jul 9, 2019
 *      Author: root
 */

#include "PumpScanner.h"

using namespace IntradayBacktest::IntradayMaintenance;

void PumpScanner::ModifyFilter()
{
	if (m_modificationStatus)
	{
		m_modificationStatus = false;
	}
	else
	{
		m_modificationStatus = true;
	}
}

Signal PumpScanner::GetEntrySignal(const size_t& time, const IntradayBacktest::DatabaseAnalysis& databaseAnalysis, const double& spread)
{
	bool restr = m_restrictions == TradingRestrictions::OnlyBuy;
	const std::vector<std::vector<std::tuple<int,int,StockInfo>>>& candles = databaseAnalysis.GetCandles();
	if (time == 0)
	{
		double len = 0;
		for (size_t i = 0; i < candles.size(); ++i)
		{
			len += fabs(std::get<2>(candles[0][i]).GetClose()-std::get<2>(candles[0][i]).GetOpen());
		}
		m_avCandleLength = len/candles.size();
	}
	std::vector<double> lengths;
	double candleCumLength = 0;
	double candleAbsLength = 0;
	int countMoreThanAv = 0;
	if (candles[0].size()> 0 && candles[1].size() >0)
	{
		const std::vector<std::tuple<int,int,StockInfo>>& shortCandles = candles[0];
		auto iterator = shortCandles.begin() + time;
		bool indicator = true;
		for (int i = 0; i < m_candlesIn; ++i)
		{
			if (iterator - shortCandles.begin() - i > 0)
			{
				auto iter = iterator - i;
				double candleLength = std::get<2>(*iter).GetClose()-std::get<2>(*iter).GetOpen();
				if (fabs(candleLength) > 1.6*m_avCandleLength)
					++countMoreThanAv;
				candleCumLength += candleLength;
				candleAbsLength += fabs(candleLength);
				lengths.push_back(candleLength);
				/*if (i == 0)
					indicator *= candleLength > 0;
				else
				{
					bool ind = restr;
					if (ind == indicator)
						continue;
					else
						return Signal::NoSignal;
				}*/
			}
			else
				return Signal::NoSignal;
		}
		double ind = m_indicators[0]->GetValue(time);
		bool indApproval =  ind > 70;

		if (m_strategyMode == StrategyMode::MeanReversal)
		{
			//MeanReversal
			if (m_restrictions == TradingRestrictions::OnlySell)
			{
				if (candleCumLength/candleAbsLength >= 0.7)
					return Signal::ShortSell;
			}
			else if (m_restrictions == TradingRestrictions::OnlyBuy)
			{
				if (candleCumLength/candleAbsLength <= -0.7)
					return Signal::LongBuy;
			}

		}
		else
		{
			//Momentum
			if (m_restrictions == TradingRestrictions::OnlyBuy)
			{
				if (candleCumLength/candleAbsLength >= 0.7)
					return Signal::LongBuy;
			}
			else if (m_restrictions == TradingRestrictions::OnlySell)
			{
				if (candleCumLength/candleAbsLength <= -0.7)
					return Signal::ShortSell;
			}

		}

/*		if (!m_modificationStatus)
		{
			if (candleCumLength/candleAbsLength > 0.7)
						return Signal::ShortSell;
		}
		else
		{
			if (candleCumLength/candleAbsLength < -0.9)
						return Signal::ShortSell;
		}*/

		//rsi

		/*bool multiplier = lengths[0] > 0;
		for (const auto& item: lengths)
		{
			if ((item > 0) == multiplier)
				continue;
			else
				return Signal::NoSignal;
		}
		switch (m_restrictions)
		{
			case OnlyBuy:
			{
				if (indicator && indApproval)
					return Signal::LongBuy;
				else
					return Signal::NoSignal;
			}
			break;
			case OnlySell:
			{
				if (!indicator & indApproval)
					return Signal::LongBuy;
				else
					return Signal::NoSignal;
			}
			default:
			{
				if (indicator && indApproval)
					return Signal::LongBuy;
				else
					return Signal::ShortSell;
			}
		}*/
//		auto previous = iterator - 1;
//		auto prePrevious = iterator -2;
//		//long signal
//		double currentCandleLength = std::get<2>(*iterator).GetClose()-std::get<2>(*iterator).GetOpen();
//		double previousCandleLength = std::get<2>(*previous).GetClose()-std::get<2>(*previous).GetOpen();
//		switch (m_restrictions)
//		{
//			case OnlyBuy:
//			{
//				if (currentCandleLength > 0 && previousCandleLength > 0 && currentCandleLength/previousCandleLength >= m_coef)
//				{
//					return Signal::LongBuy;
//				}
//				else
//				{
//					return Signal::NoSignal;
//				}
//			}
//			case OnlySell:
//			{
//				if (currentCandleLength < 0 && previousCandleLength < 0 && currentCandleLength/previousCandleLength >=m_coef)
//				{
//					return Signal::ShortSell;
//				}
//				else
//				{
//					return Signal::NoSignal;
//				}
//			}
//			default:
//			{
//				if (currentCandleLength > 0 && previousCandleLength > 0 && currentCandleLength/previousCandleLength >= m_coef)
//				{
//					return Signal::LongBuy;
//				}
//				else if (currentCandleLength < 0 && previousCandleLength < 0 && currentCandleLength/previousCandleLength >=m_coef)
//				{
//					return Signal::ShortSell;
//				}
//			}
//			return Signal::NoSignal;
//		}
	}
	return Signal::NoSignal;
}

Signal PumpScanner::GetCloseSignal(const size_t& time, const Signal& previousSignal, const IntradayBacktest::DatabaseAnalysis& databaseAnalysis)
{
	const std::vector<std::vector<std::tuple<int,int,StockInfo>>>& candles = databaseAnalysis.GetCandles();
	if (candles[0].size()> 0 && candles[1].size() >0)
	{
		const std::vector<std::tuple<int,int,StockInfo>>& longCandles = candles[1];
		auto iterator = longCandles.begin() + time;
		auto previous = iterator - 1;
		double currentCandleLength = std::get<2>(*iterator).GetClose()-std::get<2>(*iterator).GetOpen();
		double previousCandleLength = std::get<2>(*previous).GetClose()-std::get<2>(*previous).GetOpen();
		switch (previousSignal)
		{
			case Signal::LongBuy:
			{
				//if (currentCandleLength > 0 && previousCandleLength < 0) //opposite for momentum
				if (currentCandleLength > 0 && previousCandleLength < 0)
				{
					return Signal::LongSell;
				}
				else
				{
					return Signal::NoSignal;
				}
			}
			case ShortSell:
			{
				if (currentCandleLength < 0 && previousCandleLength > 0)
				{
					return Signal::ShortBuy;
				}
				else
				{
					return Signal::NoSignal;
				}
			}
		}
	}
	return Signal::NoSignal;
}

std::string PumpScanner::ToJSON()
{
	std::string json;
	json = json + "{\"" + "name" + "\"" + ":" +"\"" + "PumpScanner" + "\"" + ",";
	json = json + "\"" + "coef" + "\"" + ":" +"\"" + std::to_string(m_coef) + "\"" + ",";
	json = json + "\"" + "candlesIn" + "\"" + ":" +"\"" + std::to_string(m_candlesIn) + "\"" + ",";
	json = json + "\"" + "candlesOut" + "\"" + ":" +"\"" + std::to_string(m_candlesOut) + "\"" + ",";
	json = json + "\"" + "restriction" + "\"" + ":" +"\"" + TradingRestrictionsToString(m_restrictions) + "\"";
	json = json + "}";
	return json;
}

PumpScanner::~PumpScanner()
{
	while (m_indicators.size() > 0)
	{
		size_t size = m_indicators.size();
		auto item = m_indicators[size - 1];
		m_indicators.pop_back();
		delete item;
	}
}


