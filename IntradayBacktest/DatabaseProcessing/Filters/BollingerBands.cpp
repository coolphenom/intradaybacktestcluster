/*
 * BollingerBands.cpp
 *
 *  Created on: Mar 13, 2019
 *      Author: pavlotkachenko
 */

#include "BollingerBands.h"

void BollingerBands::ModifyFilter()
{

}

BollingerBands::BollingerBands(const std::tuple<size_t,size_t,double,double,TradingRestrictions>& args) : BollingerBands(std::get<0>(args),std::get<1>(args), std::get<2>(args),std::get<3>(args),std::get<4>(args))
{
}

BollingerBands::BollingerBands(const size_t& maMinutes, const size_t& sdMinutes, const double& stdNumEntry,
		const double& stdNumClose, const TradingRestrictions& restrictions) :
		AbstractFilter(StrategyMode::MeanReversal),
		m_maMinutes(maMinutes),
		m_stdMinutes(sdMinutes),
		m_stdNumEntry(stdNumEntry),
		m_stdNumClose(stdNumClose),
		m_restrictions(restrictions){
	// TODO Auto-generated constructor stub

}

Signal BollingerBands::GetEntrySignal(const size_t& time, const IntradayBacktest::DatabaseAnalysis& databaseAnalysis, const double& spread)
{
	if (databaseAnalysis.GetValues().size() > 0)
	{
		double val = std::get<2>(databaseAnalysis.GetValues()[time]);
		double ma = databaseAnalysis.GetCurrentSMASimple(time, m_maMinutes);
		double std = databaseAnalysis.GetCurrentStd(time, m_stdMinutes);
		switch (m_restrictions)
		{
		case TradingRestrictions::OnlyBuy:
		{
			return val < ma - m_stdNumEntry - spread ? Signal::LongBuy : Signal::NoSignal;
		}
		case TradingRestrictions::OnlySell:
		{
			return val > ma + m_stdNumEntry* std + spread ? Signal::ShortSell : Signal::NoSignal;
		}
		}
	}
	return Signal::NoSignal;
}

Signal BollingerBands::GetCloseSignal(const size_t& time, const Signal& previousSignal, const IntradayBacktest::DatabaseAnalysis& databaseAnalysis)
{
	if (databaseAnalysis.GetValues().size() > 0)
	{
		double val = std::get<2>(databaseAnalysis.GetValues()[time]);
		double ma = databaseAnalysis.GetCurrentSMASimple(time, m_maMinutes);
		double std = databaseAnalysis.GetCurrentStd(time, m_stdMinutes);
		switch (previousSignal)
		{
		case Signal::LongBuy:
			return val > ma + m_stdNumClose * std ? Signal::LongSell : Signal::NoSignal;
		case Signal::ShortSell:
			return val < ma + m_stdNumClose * std ? Signal::ShortBuy : Signal::NoSignal;
		default:
			break;
		}
		return Signal::NoSignal;
	}
	return Signal::NoSignal;
}

bool BollingerBands::IsEnabled(const size_t& time)
{
	return true;
}

std::string BollingerBands::ToJSON()
{
	std::string json;
	json = json + "{\"" + "name" + "\"" + ":" +"\"" + "BollingerBands" + "\"" + ",";
	json = json + "\"" + "ma" + "\"" + ":" +"\"" + std::to_string(m_maMinutes) + "\"" + ",";
	json = json + "\"" + "std" + "\"" + ":" +"\"" + std::to_string(m_stdMinutes) + "\"" + ",";
	json = json + "\"" + "stdEntry" + "\"" + ":" +"\"" + std::to_string(m_stdNumEntry) + "\"" + ",";
	json = json + "\"" + "stdClose" + "\"" + ":" +"\"" + std::to_string(m_stdNumClose) + "\"" + ",";
	json = json + "\"" + "restriction" + "\"" + ":" +"\"" + TradingRestrictionsToString(m_restrictions) + "\"";
	json = json + "}";
	return json;
}

