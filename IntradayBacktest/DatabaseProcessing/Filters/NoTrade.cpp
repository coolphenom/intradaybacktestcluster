/*
 * NoTrade.cpp
 *
 *  Created on: Mar 13, 2019
 *      Author: pavlotkachenko
 */

#include "NoTrade.h"

#include <exception>

NoTrade::NoTrade(const time_t& start, const time_t& finish, const int& startFrame, const int& maxFrame) :
	AbstractFilter(StrategyMode::MeanReversal)
{
	// TODO Auto-generated constructor stub
	m_startFrame = startFrame;
	std::vector<std::pair<int,int>> borders;
	int min1 = start / 60;
	int min2 = finish / 60;
	if (min1 > min2)
	{
		borders.push_back(std::make_pair(0, min2));
		min2 += startFrame;
	}
	while (min2 < maxFrame)
	{
		borders.push_back(std::make_pair(min1, min2));
		min1 += startFrame;
		min2 += startFrame;
	}
	m_borders = borders;
}

/*Signal NoTrade::GetCloseSignal(const int& time, const Signal& previousSignal)
{
	throw "not yet implemented";
}

Signal NoTrade::GetEntrySignal(const int& time)
{
	throw "not yet implemented";
}*/

bool NoTrade::IsEnabled(const size_t& time)
{
	int num = time/m_startFrame;
	return m_borders[num].first <= time && time <= m_borders[num].second;
}

NoTrade::~NoTrade() {
	// TODO Auto-generated destructor stub
}

std::string NoTrade::ToJSON()
{
	std::string json;
	json = json + "{\"" + "name" + "\"" + ":" +"\"" + "NoTrade" + "\"" + ",";
	json = json + "\"" + "frames" + "\"" + ":" +"\"" + std::to_string(m_startFrame) + "\"" + ",";
	json = json + "\"" + "borders" + "\"" + ": [";
	for (const auto& item: m_borders)
	{
		json = json +"{\"" + "borderLow" + "\":" + "\"" + std::to_string(item.first) + "\",";
		json = json + "\"" + "borderTop" + "\":" + "\"" + std::to_string(item.second) + "\"},";
	}
	json = std::string(json.begin(), json.end() -1);
	json = json + "]}";
	return json;
}

