/*
 * TradingRestrictions.cpp
 *
 *  Created on: Oct 13, 2019
 *      Author: root
 */

#include "TradingRestrictions.h"


std::string TradingRestrictionsToString(TradingRestrictions r)
{
	std::string ret;
	switch (r)
	{
		case Default:
			ret = "Default";
			break;
		case OnlyBuy:
			ret = "OnlyBuy";
			break;
		case OnlySell:
			ret = "OnlySell";
			break;
	}
	return ret;
}

