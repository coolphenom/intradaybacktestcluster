/*
 * PumpScanner.h
 *
 *  Created on: Jul 9, 2019
 *      Author: root
 */

#ifndef FILTERS_PUMPSCANNER_H_
#define FILTERS_PUMPSCANNER_H_

#include "../../IntradayMaintenance/StockInfo.h"
#include "Indicators/AbstractIndicator.h"
#include "TradingRestrictions.h"
#include "AbstractFilter.h"

#include <tuple>

class PumpScanner: public AbstractFilter {
public:
	PumpScanner(const std::vector<AbstractIndicator*> indicators, const IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum& field,const double& coef,
			const int& consecutiveCandlesIn, const int& consecutiveCandlesOut, const TradingRestrictions& restrictions,
			const StrategyMode& mode) :
				AbstractFilter{mode},
		m_coef(coef),
		m_restrictions(restrictions),
		m_candlesIn(consecutiveCandlesIn),
		m_candlesOut(consecutiveCandlesOut),
		m_indicators(indicators),
		m_avCandleLength(0){};
	bool IsEnabled(const size_t& time) override {return true;};
	Signal GetEntrySignal(const size_t& size_t, const IntradayBacktest::DatabaseAnalysis& databaseAnalysis, const double& spread) override;
	Signal GetCloseSignal(const size_t& size_t, const Signal& previousSignal, const IntradayBacktest::DatabaseAnalysis& databaseAnalysis) override;
	virtual ~PumpScanner();
	virtual std::string ToJSON() override;
	virtual void ModifyFilter() override;
private:
	double m_coef;
	int m_candlesIn;
	int m_candlesOut;
	TradingRestrictions m_restrictions;
	std::vector<AbstractIndicator*> m_indicators;
	double m_avCandleLength;
};

#endif /* FILTERS_PUMPSCANNER_H_ */
