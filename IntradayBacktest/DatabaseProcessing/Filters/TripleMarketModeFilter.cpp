/*
 * TripleMarketModeFilter.cpp
 *
 *  Created on: Nov 27, 2019
 *      Author: root
 */

#include "TripleMarketModeFilter.h"

void TripleMarketModeFilter::ModifyFilter()
{

}

TripleMarketModeFilter::TripleMarketModeFilter(const std::vector<AbstractIndicator*>& indicators, const StrategyMode& mode) :
						AbstractFilter{mode} {
	// TODO Auto-generated constructor stub
	m_indicators = indicators;
}

bool TripleMarketModeFilter::IsEnabled(const size_t& time)
{
	return true;
}

Signal TripleMarketModeFilter::GetEntrySignal(const size_t& time)
{
	double emaFast = m_indicators[0]->GetValue(time);
	double emaFastPrevious = m_indicators[0]->GetValue(time-1);
	double emaMed = m_indicators[1]->GetValue(time);
	double emaMedPrevious = m_indicators[1]->GetValue(time-1);
	double emaSlow = m_indicators[2]->GetValue(time);
	double emaSlowPrevious = m_indicators[2]->GetValue(time-1);
	if (emaFast > emaMed && emaFast > emaSlow && emaMed>emaSlow && emaMedPrevious < emaSlowPrevious)
	{
		return Signal::LongBuy;
	}
	else if (emaFast < emaMed && emaFast < emaSlow && emaMed < emaSlow && emaMedPrevious > emaSlowPrevious)
	{
		return Signal::ShortSell;
	}

	return Signal::NoSignal;
}

Signal TripleMarketModeFilter::GetCloseSignal(const size_t& time, const Signal& previousSignal)
{
	double emaFast = m_indicators[0]->GetValue(time);
	double emaFastPrevious = m_indicators[0]->GetValue(time-1);
	double emaMed = m_indicators[1]->GetValue(time);
	double emaMedPrevious = m_indicators[1]->GetValue(time-1);
	double emaSlow = m_indicators[2]->GetValue(time);
	double emaSlowPrevious = m_indicators[2]->GetValue(time-1);
	switch (previousSignal)
	{
		case Signal::LongBuy:
		{
/*			if (emaFast < emaMed && emaFast < emaSlow && emaMed < emaSlow && emaMedPrevious > emaSlowPrevious)
			{
				return Signal::LongSell;
			}*/
			if (emaFast < emaMed)
			{
				return Signal::LongSell;
			}
		}
		break;
		case Signal::ShortSell:
		{
			/*if (emaFast > emaMed && emaFast > emaSlow && emaMed>emaSlow && emaMedPrevious < emaSlowPrevious)
			{
				return Signal::ShortBuy;
			}*/
			if (emaFast > emaMed)
			{
				return Signal::ShortBuy;
			}
		}
		/*	case Signal::LongBuy:
			{
				if (emaFast > emaMed && emaFast > emaSlow && emaMed>emaSlow && emaMedPrevious < emaSlowPrevious)
				{
					return Signal::LongSell;
				}
			}
			break;
			case Signal::ShortSell:
			{
				if (emaFast < emaMed && emaFast < emaSlow && emaMed < emaSlow && emaMedPrevious > emaSlowPrevious)
				{
					return Signal::ShortBuy;
				}
			}*/
		break;
		default:
			break;
	}
	return Signal::NoSignal;
}

std::string TripleMarketModeFilter::ToJSON()
{
	std::string json;
	json = json + "{\"" + "name" + "\"" + ":" +"\"" + "TripleMarketModeFilter" + "\"" + ",";
	json = json + "\"" + "indicators" + "\"" + ":" +"[";
	for (const auto& item: m_indicators)
	{
		json = json +item->ToJSON() + ",";
	}

	json.pop_back();
	json = json + "]" + ",";
	json.pop_back();
	json = json + "}";
	return json;
}

TripleMarketModeFilter::~TripleMarketModeFilter(){
	while (m_indicators.size() > 0)
	{
		size_t size = m_indicators.size();
		auto item = m_indicators[size - 1];
		m_indicators.pop_back();
		delete item;
	}
}
