/*
 * AbstractFilter.cpp
 *
 *  Created on: Mar 13, 2019
 *      Author: pavlotkachenko
 */

#include "AbstractFilter.h"

AbstractFilter::AbstractFilter(const StrategyMode& mode) {
	// TODO Auto-generated constructor stub
	m_modificationStatus = false;
	m_strategyMode = mode;
}

bool AbstractFilter::Modified() const
{
	return m_modificationStatus;
}

AbstractFilter::~AbstractFilter() {
	// TODO Auto-generated destructor stub
}

