/*
 * BollingerBands.h
 *
 *  Created on: Mar 13, 2019
 *      Author: pavlotkachenko
 */

#ifndef FILTERS_BOLLINGERBANDS_H_
#define FILTERS_BOLLINGERBANDS_H_

#include "AbstractFilter.h"
#include "TradingRestrictions.h"

#include <tuple>

class BollingerBands: public AbstractFilter {
public:
	//template <typename ... Types>
	//BollingerBands(const std::tuple<Types...>& args);
	BollingerBands(const std::tuple<size_t,size_t,double,double,TradingRestrictions>& tuple);
	BollingerBands(const size_t& maMinutes, const size_t& sdMinutes, const double& stdNumEntry, const double& stdNumClose, const TradingRestrictions& restrictions);
	bool IsEnabled(const size_t& time) override;
	virtual Signal GetEntrySignal(const size_t& time, const IntradayBacktest::DatabaseAnalysis& databaseAnalysis, const double& spread) override;
	virtual Signal GetCloseSignal(const size_t& time, const Signal& previousSignal, const IntradayBacktest::DatabaseAnalysis& databaseAnalysis) override;
	virtual ~BollingerBands() {};
	virtual std::string ToJSON() override;
	virtual void ModifyFilter() override;
private:
	size_t m_maMinutes;
	size_t m_stdMinutes;
	double m_stdNumEntry;
	double m_stdNumClose;
	TradingRestrictions m_restrictions;
};

#endif /* FILTERS_BOLLINGERBANDS_H_ */
