/*
 * DonchianChannel.cpp
 *
 *  Created on: Mar 14, 2019
 *      Author: pavlotkachenko
 */

#include "DonchianChannel.h"
#include <algorithm>

//template <typename ... Types>
//DonchianChannel::DonchianChannel(const std::tuple<Types...>& args) : DonchianChannel(std::get<0>(args),std::get<1>(args), std::get<2>(args))
//{
//
//}

DonchianChannel::DonchianChannel(const int& frames, const int& lag, const TradingRestrictions& restrictions) :
AbstractFilter(StrategyMode::MeanReversal),
m_frames(frames),
m_lag(lag),
m_restrictions(restrictions){
	// TODO Auto-generated constructor stub

}

bool DonchianChannel::IsEnabled(const size_t& time)
{
	return true;
}

Signal DonchianChannel::GetEntrySignal(const size_t& time, const IntradayBacktest::DatabaseAnalysis& databaseAnalysis, const double& spread)
{
	if (time < m_frames + m_lag)
	{
		return Signal::NoSignal;
	}
	else
	{
		auto begin = databaseAnalysis.GetValuesBegin();
		auto bufferBegin = begin + time - m_frames - m_lag;
		auto bufferEnd = bufferBegin + m_frames;
		double locMin = *std::min(bufferBegin, bufferEnd);
		double locMax = *std::max(bufferBegin, bufferEnd);
		double val = std::get<2>(databaseAnalysis.GetValues()[time]);
		if (val > locMax + spread)
		{
			return (int)m_restrictions != 1 ? Signal::ShortSell : Signal::NoSignal;
		}
		else if (val < locMin - spread)
		{
			return (int)m_restrictions != 2 ? Signal::LongBuy : Signal::NoSignal;
		}
		else
		{
			return Signal::NoSignal;
		}
	}
	return Signal::NoSignal;
}

Signal DonchianChannel::GetCloseSignal(const size_t& time, const Signal& previousSignal, const IntradayBacktest::DatabaseAnalysis& databaseAnalysis)
{
	if (time < databaseAnalysis.GetValues().size())
	{
		auto begin = databaseAnalysis.GetValuesBegin();
		auto bufferBegin = begin + time - m_frames - m_lag;
		auto bufferEnd = bufferBegin + m_frames;
		double locMin = *std::min(bufferBegin, bufferEnd);
		double locMax = *std::max(bufferBegin, bufferEnd);
		double val = std::get<2>(databaseAnalysis.GetValues()[time]);
		switch (previousSignal)
		{
		case Signal::LongBuy:
			return val > (locMin + locMax)/2 ? Signal::LongSell : Signal::NoSignal;
		case Signal::ShortSell:
			return val < (locMin + locMax)/2 ? Signal::ShortBuy : Signal::NoSignal;
		default:
			return Signal::NoSignal;
		}
	}
	return Signal::NoSignal;
}

DonchianChannel::~DonchianChannel() {
	// TODO Auto-generated destructor stub
}

std::string DonchianChannel::ToJSON()
{
	std::string json;
	json = json + "{\"" + "name" + "\"" + ":" +"\"" + "DonchianChannel" + "\"" + ",";
	json = json + "\"" + "frames" + "\"" + ":" +"\"" + std::to_string(m_frames) + "\"" + ",";
	json = json + "\"" + "lag" + "\"" + ":" +"\"" + std::to_string(m_lag) + "\"" + ",";
	json = json + "\"" + "restriction" + "\"" + ":" +"\"" + TradingRestrictionsToString(m_restrictions) + "\"";
	json = json + "}";
	return json;
}

