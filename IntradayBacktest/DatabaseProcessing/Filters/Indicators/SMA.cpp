/*
 * SMA.cpp
 *
 *  Created on: Feb 15, 2020
 *      Author: root
 */

#include "SMA.h"

#include <bits/stdc++.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <assert.h>

SMA::SMA(const IntradayBacktest::DatabaseAnalysis& trader, const int& length){
	// TODO Auto-generated constructor stub
	m_values = std::vector<double>(trader.GetValuesCount());
	m_length = length;
	for (size_t i = 0; i < m_values.size(); ++i)
	{
		if (i < m_length - 1)
			m_values[i] = trader.GetSums()[i]/i;
		else
			m_values[i] = (trader.GetSums()[i] - trader.GetSums()[i - m_length])/m_length;
	}
}

const std::vector<double>& SMA::GetValues() const
{
	return m_values;
}

double SMA::GetValue(const int& index) const
{
	return m_values[index];
}

double SMA::GetFirstDerivativeValue(const int& index) const
{
	throw new std::runtime_error("not implemented");
}

double SMA::GetSMA(const int& index) const
{
	return m_values[index];
}

void SMA::Compare(const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& values)
{
	if (values.size() > 0 && m_values.size() > 0 && values.size() == m_values.size())
	{
		std::string dir = "/media/sf_database/Export/export";
		if (mkdir(dir.c_str(), 0777) == -1)
		{
			std::cerr << "Error " << std::strerror(errno);
			std::cerr << std::endl;
		}

		std::string name = dir + "/valuesSMA.csv";
		std::ofstream file(name);
		for (size_t i = 0; i < values.size(); ++i)
		{
			file << std::get<0>(values[i]) << "," << std::get<1>(values[i]) << ","
				 <<	std::get<2>(values[i]).GetClose() << "," << m_values[i] << "\n";
		}
		file.close();
	}
}

void SMA::Compare(const std::vector<double>& values, const std::string nam = "")
{
	std::cout << values.size() << std::endl << m_values.size() << std::endl;
	if (values.size() > 0 && m_values.size() > 0 && values.size() == m_values.size())
	{
		std::string dir = "/media/sf_database/Export/export";
		if (mkdir(dir.c_str(), 0777) == -1)
		{
			std::cerr << "Error " << std::strerror(errno);
			std::cerr << std::endl;
		}

		std::string name = dir + (nam == "" ? "/valuesSMA.csv" : "/" + nam + ".csv");
		std::ofstream file(name);
		for (size_t i = 0; i < values.size(); ++i)
		{
			file << m_values[i] << "," << values[i] << "\n";
		}
		file.close();
	}
}

std::string SMA::ToJSON()
{
	std::string json;
	json = json + "{\"" + "name" + "\"" + ":" +"\"" + "SMA" + "\"" + ",";
	json = json + "\"" + "candles" + "\"" + ":" +"\"" + std::to_string(m_length) + "\"";
	json = json + "}";
	return json;
}

SMA::~SMA() {
	// TODO Auto-generated destructor stub
}

