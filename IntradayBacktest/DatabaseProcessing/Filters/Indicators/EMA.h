/*
 * EMA.h
 *
 *  Created on: Nov 13, 2019
 *      Author: root
 */

#ifndef FILTERS_INDICATORS_EMA_H_
#define FILTERS_INDICATORS_EMA_H_

#include "AbstractIndicator.h"
#include "../../IntradayMaintenance/StockInfo.h"
#include "../../DatabaseAnalysis.h"

#include <vector>
#include <tuple>

class EMA : public AbstractIndicator{
public:
	EMA(const IntradayBacktest::DatabaseAnalysis& trader, const int& length, const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& values, const double& coefficient = 0);
	double GetValue(const int& index) const override;
	double GetFirstDerivativeValue(const int& index) const override;
	double GetEMA(const int& index) const;
	void Compare(const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& values) override;
	const std::vector<double>& GetValues() const override;
	virtual ~EMA();
	std::string ToJSON() override;
private:
	std::vector<double> m_values;
	int m_length;
};

#endif /* FILTERS_INDICATORS_EMA_H_ */
