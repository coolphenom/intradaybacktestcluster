/*
 * AbstractIndicator.cpp
 *
 *  Created on: Nov 13, 2019
 *      Author: root
 */

#include "AbstractIndicator.h"

AbstractIndicator::AbstractIndicator() {
	// TODO Auto-generated constructor stub

}

double AbstractIndicator::GetSecondDerivativeValue(const int& index)
{
	return index >= 3 ? (11*GetValue(index) - 18*GetValue(index-1) + 9*GetValue(index - 2) - 2*GetValue(index - 3))/6 : 0;
}

bool AbstractIndicator::IsGrowthInflection(const int& index)
{
	double scndDer = GetSecondDerivativeValue(index);
	double prevScndDer = GetSecondDerivativeValue(index);

}

bool AbstractIndicator::IsDeclineInflection(const int& index)
{

}

bool AbstractIndicator::IsFlatteningTop(const int& index)
{
	double der = this->GetFirstDerivativeValue(index);
	double prevDer = this->GetFirstDerivativeValue(index - 1);
	double prevPrevDer = this->GetFirstDerivativeValue(index - 2);
	if (der < 0 && prevDer > 0 && prevPrevDer > 0)
		return true;
	return false;
}

bool AbstractIndicator::IsFlatteningBottom(const int& index)
{
	double der = this->GetFirstDerivativeValue(index);
	double prevDer = this->GetFirstDerivativeValue(index - 1);
	double prevPrevDer = this->GetFirstDerivativeValue(index - 2);
	if (der > 0 && prevDer < 0 && prevPrevDer < 0)
		return true;
	return false;
}

AbstractIndicator::~AbstractIndicator() {
	// TODO Auto-generated destructor stub
}

