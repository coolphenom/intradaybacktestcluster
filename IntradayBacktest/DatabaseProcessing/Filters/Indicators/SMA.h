/*
 * SMA.h
 *
 *  Created on: Feb 15, 2020
 *      Author: root
 */

#ifndef FILTERS_INDICATORS_SMA_H_
#define FILTERS_INDICATORS_SMA_H_

#include "AbstractIndicator.h"
#include "../../IntradayMaintenance/StockInfo.h"
#include "../../DatabaseAnalysis.h"

class SMA: public AbstractIndicator {
public:
public:
	SMA(const IntradayBacktest::DatabaseAnalysis& trader, const int& length);
	double GetSMA(const int& index) const;
	double GetValue(const int& index) const override;
	double GetFirstDerivativeValue(const int& index) const override;
	void Compare(const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& values) override;
	void Compare(const std::vector<double>& values, const std::string name = "");
	const std::vector<double>& GetValues() const override;
	virtual ~SMA();
	std::string ToJSON() override;
private:
	std::vector<double> m_values;
	int m_length;
};

#endif /* FILTERS_INDICATORS_SMA_H_ */
