/*
 * AbstractIndicator.h
 *
 *  Created on: Nov 13, 2019
 *      Author: root
 */

#ifndef FILTERS_INDICATORS_ABSTRACTINDICATOR_H_
#define FILTERS_INDICATORS_ABSTRACTINDICATOR_H_

#include "../../IntradayMaintenance/StockInfo.h"

#include <string>

class AbstractIndicator {
public:
	AbstractIndicator();
	bool IsFlatteningTop(const int& index);
	bool IsFlatteningBottom(const int& index);
	bool IsGrowthInflection(const int& index);
	bool IsDeclineInflection(const int& index);
	virtual double GetValue(const int& index) const = 0;
	virtual double GetFirstDerivativeValue(const int& index) const = 0;
	double GetSecondDerivativeValue(const int& index);
	virtual const std::vector<double>& GetValues() const = 0;
	virtual ~AbstractIndicator();
	virtual void Compare(const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& values){}
	virtual std::string ToJSON() {return "";};
};

#endif /* FILTERS_INDICATORS_ABSTRACTINDICATOR_H_ */
