/*
 * SSMA.cpp
 *
 *  Created on: Nov 13, 2019
 *      Author: root
 */

#include "SSMA.h"
#include "EMA.h"

#include <bits/stdc++.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <assert.h>

SSMA::SSMA(const IntradayBacktest::DatabaseAnalysis& trader,const int& length, const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& values)
{
	m_length = length;
	m_values = std::vector<double>(values.size());
	EMA ema(trader, length*2, values);
	for (size_t i = 0; i < values.size(); ++i)
	{
		m_values[i] = ema.GetValue(i);
	}
}

double SSMA::GetValue(const int& index) const
{
	return m_values[index];
}

const std::vector<double>& SSMA::GetValues() const
{
	return m_values;
}

double SSMA::GetFirstDerivativeValue(const int& index) const
{
	throw new std::runtime_error("not implemented");
}

double SSMA::GetSSMA(const int& index) const
{
	return m_values[index];
}

void SSMA::Compare(const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& values)
{
	if (values.size() > 0 && m_values.size() > 0 && values.size() == m_values.size())
	{
		std::string dir = "/media/sf_database/Export/export";
		if (mkdir(dir.c_str(), 0777) == -1)
		{
			std::cerr << "Error " << std::strerror(errno);
			std::cerr << std::endl;
		}

		std::string name = dir + "/valuesSSMA.csv";
		std::ofstream file(name);
		for (size_t i = 0; i < values.size(); ++i)
		{
			file << std::get<0>(values[i]) << "," << std::get<1>(values[i]) << ","
				 <<	std::get<2>(values[i]).GetClose() << "," << m_values[i] << "\n";
		}
		file.close();
	}
}

std::string SSMA::ToJSON()
{
	std::string json;
	json = json + "{\"" + "name" + "\"" + ":" +"\"" + "SSMA" + "\"" + ",";
	json = json + "\"" + "candles" + "\"" + ":" +"\"" + std::to_string(m_length) + "\"";
	json = json + "}";
	return json;
}

SSMA::~SSMA() {
	// TODO Auto-generated destructor stub
}

