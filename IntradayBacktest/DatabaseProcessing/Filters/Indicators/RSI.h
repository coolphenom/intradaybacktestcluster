/*
 * RSI.h
 *
 *  Created on: Jan 18, 2020
 *      Author: root
 */

#ifndef FILTERS_INDICATORS_RSI_H_
#define FILTERS_INDICATORS_RSI_H_

#include "AbstractIndicator.h"
#include "EMA.h"

class RSI: public AbstractIndicator {
public:
	RSI(const IntradayBacktest::DatabaseAnalysis& trader, const int& length, const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& values);
	double GetValue(const int& index) const override;
	double GetRSI(const int& index) const;
	double GetFirstDerivativeValue(const int& index) const override;
	void Compare(const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& values) override;
	const std::vector<double>& GetValues() const override;
	virtual ~RSI();
	std::string ToJSON() override;
private:
	std::vector<double> m_values;
	int m_length;

};

#endif /* FILTERS_INDICATORS_RSI_H_ */
