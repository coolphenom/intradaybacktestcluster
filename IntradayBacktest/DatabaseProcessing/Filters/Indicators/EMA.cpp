/*
 * EMA.cpp
 *
 *  Created on: Nov 13, 2019
 *      Author: root
 */

#include "EMA.h"

#include <bits/stdc++.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <assert.h>

EMA::EMA(const IntradayBacktest::DatabaseAnalysis& trader, const int& length, const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& values, const double& coefficient) {
	m_values = std::vector<double>(values.size());
	m_length = length;
	double coeff = coefficient == 0 ? (double)2/((double)length + 1) : coefficient;
	for (size_t i = 0; i < values.size(); ++i)
	{
		if (i == 0)
			m_values[i] = std::get<2>(values[i]).GetClose();
		else
		{
			m_values[i] = m_values[i-1]*(1-coeff)  + coeff*std::get<2>(values[i]).GetClose();
		}
	}
}

double EMA::GetValue(const int& index) const
{
	return GetEMA(index);
}

double EMA::GetFirstDerivativeValue(const int& index) const
{
	return index > 2 ? (3*GetEMA(index) - 4*GetEMA(index - 1) + GetEMA(index-2))/2 : 0;
}

double EMA::GetEMA(const int& index) const
{
	return m_values[index];
}

const std::vector<double>& EMA::GetValues() const
{
	return m_values;
}

void EMA::Compare(const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& values)
{
	if (values.size() > 0 && m_values.size() > 0 && values.size() == m_values.size())
	{
		std::string dir = "/media/sf_database/Export/export";
		if (mkdir(dir.c_str(), 0777) == -1)
		{
			std::cerr << "Error " << std::strerror(errno);
			std::cerr << std::endl;
		}

		std::string name = dir + "/valuesEMA.csv";
		std::ofstream file(name);
		for (size_t i = 0; i < values.size(); ++i)
		{
			file << std::get<0>(values[i]) << "," << std::get<1>(values[i]) << ","
				 <<	std::get<2>(values[i]).GetClose() << "," << m_values[i] << "\n";
		}
		file.close();
	}
}

std::string EMA::ToJSON()
{
	std::string json;
	json = json + "{\"" + "name" + "\"" + ":" +"\"" + "EMA" + "\"" + ",";
	json = json + "\"" + "candles" + "\"" + ":" +"\"" + std::to_string(m_length) + "\"";
	json = json + "}";
	return json;
}

EMA::~EMA() {
	// TODO Auto-generated destructor stub
}

