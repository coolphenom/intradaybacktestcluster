/*
 * RSI.cpp
 *
 *  Created on: Jan 18, 2020
 *      Author: root
 */

#include "RSI.h"

#include <bits/stdc++.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <assert.h>
#include <exception>

RSI::RSI(const IntradayBacktest::DatabaseAnalysis& trader, const int& length, const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& values) {
	// TODO Auto-generated constructor stub
	m_values = std::vector<double>(values.size());
	m_length = length;
	std::vector<double> uEmaValues = std::vector<double>(values.size());
	std::vector<double> dEmaValues = std::vector<double>(values.size());
	double coeff = (double)1/((double)length);
	for (size_t i = 0; i < values.size(); ++i)
	{
		double val = std::get<2>(values[i]).GetClose();
		double prevVal = i == 0 ?  val : std::get<2>(values[i - 1]).GetClose();
		double uEma = (fabs(val - prevVal) + val - prevVal)/2;
		double dEma = (fabs(val - prevVal) - val + prevVal)/2;
		if (i == 0)
		{
			uEmaValues[i] = uEma;
			dEmaValues[i] = dEma;
			m_values[i] = 100 - 100/(1 + uEmaValues[i]/dEmaValues[i]);
		}
		else
		{
			uEmaValues[i] = uEmaValues[i-1]*(1-coeff)  + coeff*uEma;
			dEmaValues[i] = dEmaValues[i-1]*(1-coeff)  + coeff*dEma;
			m_values[i] = 100 - 100/(1 + uEmaValues[i]/dEmaValues[i]);
		}
	}
}

const std::vector<double>& RSI::GetValues() const
{
	return m_values;
}

double RSI::GetValue(const int& index) const
{
	return GetRSI(index);
}

double RSI::GetRSI(const int& index) const
{
	return m_values[index];
}

double RSI::GetFirstDerivativeValue(const int& index) const
{
	throw new std::runtime_error("not implemented");
}

void RSI::Compare(const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& values)
{
	if (values.size() > 0 && m_values.size() > 0 && values.size() == m_values.size())
	{
		std::string dir = "/media/sf_database/Export/export";
		if (mkdir(dir.c_str(), 0777) == -1)
		{
			std::cerr << "Error " << std::strerror(errno);
			std::cerr << std::endl;
		}

		std::string name = dir + "/valuesRSI.csv";
		std::ofstream file(name);
		for (size_t i = 0; i < values.size(); ++i)
		{
			file << std::get<0>(values[i]) << "," << std::get<1>(values[i]) << ","
				 <<	std::get<2>(values[i]).GetClose() << "," << m_values[i] << "\n";
		}
		file.close();
	}
}

std::string RSI::ToJSON()
{
	std::string json;
	json = json + "{\"" + "name" + "\"" + ":" +"\"" + "RSI" + "\"" + ",";
	json = json + "\"" + "candles" + "\"" + ":" +"\"" + std::to_string(m_length) + "\"";
	json = json + "}";
	return json;
}

RSI::~RSI() {
	// TODO Auto-generated destructor stub
}

