/*
 * Signal.h
 *
 *  Created on: Mar 13, 2019
 *      Author: pavlotkachenko
 */

#ifndef SIGNAL_H_
#define SIGNAL_H_


enum Signal
{
	LongBuy = 0,
	LongSell = 1,
	ShortSell = 2,
	ShortBuy = 3,
	NoSignal = 4
};


#endif /* SIGNAL_H_ */
