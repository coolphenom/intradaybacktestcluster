/*
 * AbstractFilter.h
 *
 *  Created on: Mar 13, 2019
 *      Author: pavlotkachenko
 */

#ifndef ABSTRACTFILTER_H_
#define ABSTRACTFILTER_H_

#include "Signal.h"
#include "../DatabaseAnalysis.h"

enum StrategyMode
{
	MomentumMode = 0,
	MeanReversal = 1
};

class AbstractFilter {
public:
	AbstractFilter(const StrategyMode& mode);
	virtual ~AbstractFilter();
	virtual bool IsEnabled(const size_t& time) = 0;
	virtual bool IsEnabled(const IntradayBacktest::DatabaseAnalysis& databaseAnalysis, const size_t& time) {return true;}
	virtual Signal GetEntrySignal(const size_t& time, const IntradayBacktest::DatabaseAnalysis& databaseAnalysis, const double& spread = 0) {return Signal::NoSignal;}
	virtual Signal GetEntrySignal(const size_t& time) {return Signal::NoSignal;}
	virtual Signal GetCloseSignal(const size_t& time, const Signal& previousSignal, const IntradayBacktest::DatabaseAnalysis& databaseAnalysis) { return Signal::NoSignal;}
	virtual Signal GetCloseSignal(const size_t& time, const Signal& previousSignal) { return Signal::NoSignal;}
	virtual Signal GetCloseSignalUniversal(const size_t& time, const IntradayBacktest::DatabaseAnalysis& databaseAnalysis) { return Signal::NoSignal;}
	virtual std::string ToJSON() = 0;
	virtual void ModifyFilter() { }
	bool Modified() const;
protected:
	bool m_modificationStatus;
	StrategyMode m_strategyMode;
};

enum FilterRole
{
	Primary = 0,
	Secondary = 1
};

enum AbstractFilterEnum
{
	BollingerBandsFilter= 0,
	SelfLongVolPercentile = 1,
	SelfShortVolPercentile= 2,
	DonchianChannelFilter = 3,
	PumpScannerFilter = 4,
	MarketModeFilter = 5,
	TripleMarketModeFilt = 6
};

#endif /* ABSTRACTFILTER_H_ */
