/*
 * MarketModeChange.cpp
 *
 *  Created on: Nov 13, 2019
 *      Author: root
 */

#include "MarketModeChange.h"

void MarketModeChange::ModifyFilter()
{

}

MarketModeChange::MarketModeChange(const std::vector<AbstractIndicator*>& indicators, const std::unordered_map<size_t,size_t>& timeframeMapper,
		const StrategyMode& mode) :
						AbstractFilter{mode} {
	// TODO Auto-generated constructor stub
	function = __func__;
	SIGNALEXCEPTION(signalHandler);
	m_indicators = indicators;
	m_timeFrameMapper = timeframeMapper;
}

bool MarketModeChange::IsEnabled(const size_t& time)
{
	return true;
}

//SSMA, EMA
Signal MarketModeChange::GetEntrySignal(const size_t& time)
{
/*	double ssma = m_indicators[0]->GetValue(time);
	double ema = m_indicators[1]->GetValue(time);
	double ssmaPrevious = m_indicators[0]->GetValue(time - 1);
	double emaPrevious = m_indicators[1]->GetValue(time - 1);
	if (emaPrevious < ssmaPrevious && ema > ssma)
	{
		return Signal::LongBuy;
	}
	else if (emaPrevious > ssmaPrevious && ema < ssma)
	{
		return Signal::ShortSell;
	}
	else
	{
		return Signal::NoSignal;
	}*/
	//size_t timeSenior = m_timeFrameMapper[time];
	//auto rsi = m_indicators[1] ->GetValue(time);
	//auto rsiSenior = m_indicators[2] ->GetValue(timeSenior);
	//if (m_indicators[0]->IsFlatteningTop(time) &&  rsi > 65
	//		&& rsiSenior > 60)
	//{
		//return Signal::ShortSell;
	//}
	if (m_indicators[2]->GetValue(time) < 0.3)
	{
		return Signal::LongBuy;
	}

	return Signal::NoSignal;
}

/*Signal MarketModeChange::GetEntrySignal(const size_t& time)
{
	if(time > 2)
	{
		size_t timeSenior = m_timeFrameMapper[time];
		double emaQuick = m_indicators[0]->GetValue(time);
		double rsiSlow = m_indicators[1]->GetValue(time);
		double rsiQuick = m_indicators[2]->GetValue(time);
		double rsiQuickPrev = m_indicators[2]->GetValue(time - 1);
		double rsiQuickPrevPrev = m_indicators[2]->GetValue(time - 2);
		double rsiSenior = m_indicators[3]->GetValue((int)timeSenior);
		if (rsiQuick < 30 && rsiQuickPrev < 30 && rsiQuickPrevPrev < 30)
		{
			return Signal::LongBuy;
		}
		//else if (rsiQuick > 70 && rsiQuickPrev > 70 && rsiQuickPrevPrev > 70)
		//{
		//	return Signal::ShortSell;
		//}
		else
		{
			return Signal::NoSignal;
		}
	}
	return Signal::NoSignal;
}*/

Signal MarketModeChange::GetCloseSignal(const size_t& time, const Signal& previousSignal)
{
	double ssma = m_indicators[0]->GetValue(time);
	double ema = m_indicators[1]->GetValue(time);
	double ssmaPrevious = m_indicators[0]->GetValue(time - 1);
	double emaPrevious = m_indicators[1]->GetValue(time - 1);
	if (previousSignal == Signal::LongBuy)
	{
/*		if (emaPrevious > ssmaPrevious && ema < ssma)
		{
			return Signal::LongSell;
		}
		else
		{
			return Signal::NoSignal;
		}*/
		if (m_indicators[2] ->GetValue(time) >= 1)
			return Signal::LongSell;
		else
			return Signal::NoSignal;
	}
	else if (previousSignal == Signal::ShortSell)
	{
/*		if (emaPrevious < ssmaPrevious && ema > ssma)
		{
			return Signal::ShortBuy;
		}
		else
		{
			return Signal::NoSignal;
		}*/
		if (m_indicators[0] ->IsFlatteningBottom(time))
			return Signal::ShortBuy;
		else
			return Signal::NoSignal;
	}
}

/*
Signal MarketModeChange::GetCloseSignal(const size_t& time, const Signal& previousSignal)
{
	double emaQuickDer = m_indicators[0]->GetFirstDerivativeValue(time);
	double emaQuickDerPrev = m_indicators[0]->GetFirstDerivativeValue(time - 1);
	double rsiSlow = m_indicators[1]->GetValue(time);
	if (previousSignal == Signal::LongBuy)
	{
		if (rsiSlow > 55 && emaQuickDer < 0 && emaQuickDerPrev > 0)
		{
			return Signal::LongSell;
		}
		else
		{
			return Signal::NoSignal;
		}
	}
	else if (previousSignal == Signal::ShortSell)
	{
		if (rsiSlow < 45 && emaQuickDer > 0 && emaQuickDerPrev < 0)
		{
			return Signal::ShortBuy;
		}
		else
		{
			return Signal::NoSignal;
		}
	}
}
*/

void MarketModeChange::Test(const AbstractIndicator* ind, const int& i)
{
	double val = ind->GetValue(i);
	std::cout << "test" << std::endl;
}

std::string MarketModeChange::ToJSON()
{
	std::string json;
	json = json + "{\"" + "name" + "\"" + ":" +"\"" + "MarketModeChange" + "\"" + ",";
	json = json + "\"" + "indicators" + "\"" + ":" +"[";
	for (const auto& item: m_indicators)
	{
		json = json +item->ToJSON() + ",";
	}

	json.pop_back();
	json = json + "]" + ",";
	json.pop_back();
	json = json + "}";
	return json;
}

MarketModeChange::~MarketModeChange()
{
	while (m_indicators.size() > 0)
	{
		size_t size = m_indicators.size();
		auto item = m_indicators[size - 1];
		m_indicators.pop_back();
		delete item;
	}
}

