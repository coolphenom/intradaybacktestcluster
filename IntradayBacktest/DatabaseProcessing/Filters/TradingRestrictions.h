/*
 * TradingRestrictions.h
 *
 *  Created on: Mar 13, 2019
 *      Author: pavlotkachenko
 */

#ifndef TRADINGRESTRICTIONS_H_
#define TRADINGRESTRICTIONS_H_


#include <string>

enum TradingRestrictions
{
	Default = 0,
	OnlyBuy = 1,
	OnlySell = 2
};

std::string TradingRestrictionsToString(TradingRestrictions r);



#endif /* TRADINGRESTRICTIONS_H_ */
