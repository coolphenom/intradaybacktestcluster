/*
 * VolatilityPercentile.cpp
 *
 *  Created on: Feb 3, 2020
 *      Author: root
 */

#include "VolatilityPercentile.h"

#include <bits/stdc++.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <assert.h>
#include <exception>

VolatilityPercentile::VolatilityPercentile(const int& frames, const IntradayBacktest::DatabaseAnalysis& trader){
	// TODO Auto-generated constructor stub
	size_t size = trader.GetValuesCount();
	m_std = std::vector<double>(size);
	m_stdPercentile = std::vector<double>(size);
	for (size_t i = 0; i < size; ++i)
	{
		if (i == 0)
			m_std[i] = 0;
		else
			m_std[i] = trader.GetCurrentStd(i, frames);
		if (i >= frames)
		{
			StatisticsHelper s(std::vector<double>(m_std.begin() + i - frames, m_std.begin() + i));
			m_stdPercentile[i] = s.GetQuantileOfCDF(m_std[i]);
		}
		else
		{
			m_stdPercentile[i] = 0;
		}
	}
	m_frames = frames;
}

const std::vector<double>& VolatilityPercentile::GetValues() const
{
	return m_stdPercentile;
}

double VolatilityPercentile::GetValue(const int& index) const
{
	return m_stdPercentile[index];
}

double VolatilityPercentile::GetFirstDerivativeValue(const int& index) const
{
	throw std::runtime_error("not implemented");
}

void VolatilityPercentile::Compare(const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& values)
{
	if (values.size() > 0 && m_std.size() > 0 && values.size() == m_std.size())
	{
		std::string dir = "/media/sf_database/Export/export";
		if (mkdir(dir.c_str(), 0777) == -1)
		{
			std::cerr << "Error " << std::strerror(errno);
			std::cerr << std::endl;
		}

		std::string name = dir + "/valuesStdVol.csv";
		std::ofstream file(name);
		for (size_t i = 0; i < values.size(); ++i)
		{
			file << std::get<0>(values[i]) << "," << std::get<1>(values[i]) << ","
				 <<	std::get<2>(values[i]).GetClose() << "," << m_std[i] << "," << m_stdPercentile[i]  << "\n";
		}
		file.close();
	}
	else
	{
		std::cout << "Wrong dimensions, values = " << std::to_string(values.size()) << ", std = " << std::to_string(m_std.size()) << std::endl;
	}
}

std::string VolatilityPercentile::ToJSON()
{
	std::string json;
	json = json + "{\"" + "name" + "\"" + ":" +"\"" + "VolatilityPercentile" + "\"" + ",";
	json = json + "\"" + "candles" + "\"" + ":" +"\"" + std::to_string(m_frames) + "\"";
	json = json + "}";
	return json;
}

VolatilityPercentile::~VolatilityPercentile() {
	// TODO Auto-generated destructor stub
}

