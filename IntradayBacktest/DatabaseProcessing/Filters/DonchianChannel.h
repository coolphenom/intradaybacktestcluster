/*
 * DonchianChannel.h
 *
 *  Created on: Mar 14, 2019
 *      Author: pavlotkachenko
 */

#ifndef FILTERS_DONCHIANCHANNEL_H_
#define FILTERS_DONCHIANCHANNEL_H_

#include "AbstractFilter.h"
#include "TradingRestrictions.h"

class DonchianChannel: public AbstractFilter {
public:
	template <typename ... Types>
	DonchianChannel(const std::tuple<Types...>& args) :
	AbstractFilter(StrategyMode::MeanReversal)
	{
		//stub
	}
	DonchianChannel(const int& frames, const int& lag, const TradingRestrictions& restrictions);
	bool IsEnabled(const size_t& time) override;
	virtual Signal GetEntrySignal(const size_t& time, const IntradayBacktest::DatabaseAnalysis& databaseAnalysis, const double& spread) override;
	virtual Signal GetCloseSignal(const size_t& time, const Signal& previousSignal, const IntradayBacktest::DatabaseAnalysis& databaseAnalysis) override;
	virtual ~DonchianChannel();
	virtual std::string ToJSON() override;
private:
	int m_frames;
	int m_lag;
	TradingRestrictions m_restrictions;
};

#endif /* FILTERS_DONCHIANCHANNEL_H_ */
