/*
 * DatabaseProcessing.cpp
 *
 *  Created on: Mar 6, 2019
 *      Author: pavlotkachenko
 */
#include "DatabaseProcessing.h"
#include "../IntradayMaintenance/StockInfo.h"
#include "../IntradayMaintenance/Timeframe.h"
#include "../IntradayMaintenance/CustomExceptionHandler.h"

#include "../src/include_libs/mysql/mysql_connection.h"
#include "../src/include_libs/mysql/cppconn/driver.h"
#include "../src/include_libs/mysql/cppconn/exception.h"
#include "../src/include_libs/mysql/cppconn/resultset.h"
#include "../src/include_libs/mysql/cppconn/statement.h"

#include <unistd.h>
#include <boost/format.hpp>
#include <boost/regex.hpp>
#include <vector>
#include <utility>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <cstdint>


namespace{

int StringToTime(const std::string& dbString)
{
	std::string hours, minutes, seconds;
	std::istringstream iss(dbString);
	std::getline(iss, hours, ':');
	std::getline(iss, minutes, ':');
	std::getline(iss, seconds);
	return stoi(hours)*60+stoi(minutes);
}

/*std::tm StringToTMStruct(const std::string& dbString)
{
	std::tm tm;
	std::istringstream iss(dbString);
	iss >> std::get_time(&tm, "%H:%M:%S");
	return tm;
}*/

boost::gregorian::date StringToGregorian(const std::string& dbString)
{
	return boost::gregorian::from_string(dbString);
}

int ToOADate(const boost::gregorian::date& time)
{
	boost::gregorian::date dStart{1899, 12, 31};
	boost::gregorian::date_period d{dStart, time};
	boost::gregorian::date_duration dd = d.length();
	return dd.days();
}

int StringToOADate(const std::string& dbString)
{
	return ToOADate(boost::gregorian::from_string(dbString));
}



template <typename T>
void PopulateList(std::vector<T>& list, sql::ResultSet*& res, const int& columnCount, const IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum& field = IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum::ClosePrice,
		const IntradayBacktest::IntradayMaintenance::Timeframe& timeframe = IntradayBacktest::IntradayMaintenance::Timeframe::OneMinute)
{
	  if (columnCount == 8)
	  {
		  if (typeid(T) == typeid(IntradayBacktest::IntradayMaintenance::StockInfo))
		  {
			  PopulateList<IntradayBacktest::IntradayMaintenance::StockInfo>(list,res,columnCount, timeframe);
		  }
		  else if (typeid(T) == typeid(std::tuple<int,int,double>))
		  {
			  PopulateList<std::tuple<int,int,double>>(list, res, columnCount, field, timeframe);
		  }
		  else
		  {
			  PopulateList<std::tuple<int,double>>(list, res, columnCount, field, timeframe);
		  }
	  }
	  else if (columnCount == 1)
	  {
		  if (typeid(T) == typeid(boost::gregorian::date))
		  {
			  PopulateList<boost::gregorian::date>(list,res,columnCount, timeframe);
		  }
		  else if (typeid(T) == typeid(long double))
		  {
			  PopulateList<long double>(list,res,columnCount, timeframe);
		  }
		  else if (typeid(T) == typeid(int))
		  {
			  PopulateList<int>(list,res,columnCount, timeframe);
		  }
		  else if (typeid(T) == typeid(std::string))
		  {
			  PopulateList<std::string>(list,res,columnCount, timeframe);
		  }
	  }
}

template<>
void PopulateList(std::vector<std::tuple<int,double>>& list, sql::ResultSet*& res, const int& columnCount,const IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum& field, const IntradayBacktest::IntradayMaintenance::Timeframe& timeframe)
{
	switch (field)
	{
	case IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum::ClosePrice:
	{
		list.push_back(std::make_tuple(StringToOADate(res->getString(1)),res->getDouble(6)));
	}
	break;
	default:
		break;
	}
}

template<>
void PopulateList(std::vector<std::tuple<int,int,double>>& list, sql::ResultSet*& res, const int& columnCount,const IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum& field, const IntradayBacktest::IntradayMaintenance::Timeframe& timeframe)
{
	auto day = StringToOADate(res->getString(1));
	auto minutes = StringToTime(res->getString(2));
	int multiplier = IntradayBacktest::IntradayMaintenance::TimeframeHelperMultiplier(timeframe);
	if (timeframe != IntradayBacktest::IntradayMaintenance::Timeframe::OneMinute && minutes % multiplier != 0)
	{
		return;
	}
	switch (field)
	{
	case IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum::OpenPrice:
	{
		list.push_back(std::make_tuple(day, minutes, res->getDouble(3)));
	}
	break;
	case IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum::HighPrice:
	{
		list.push_back(std::make_tuple(day, minutes,res->getDouble(4)));
	}
	break;
	case IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum::LowPrice:
	{
		list.push_back(std::make_tuple(day, minutes,res->getDouble(5)));
	}
	break;
	case IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum::ClosePrice:
	{
		list.push_back(std::make_tuple(day, minutes,res->getDouble(6)));
	}
	break;
	default:
		break;
	}

}

template<>
void PopulateList(std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& list, sql::ResultSet*& res, const int& columnCount,const IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum& field, const IntradayBacktest::IntradayMaintenance::Timeframe& timeframe)
{
	auto day = StringToOADate(res->getString(1));
	auto minutes = StringToTime(res->getString(2));
	int multiplier = IntradayBacktest::IntradayMaintenance::TimeframeHelperMultiplier(timeframe);
	if (timeframe != IntradayBacktest::IntradayMaintenance::Timeframe::OneMinute && minutes % multiplier != 0)
	{
		return;
	}
	list.push_back(std::make_tuple(day, minutes,
			IntradayBacktest::IntradayMaintenance::StockInfo(day, minutes,
					StringToGregorian(res->getString(1)),
					  res->getDouble(3), res->getDouble(4), res->getDouble(5), res->getDouble(6), res->getDouble(7))));
}

template<>
void PopulateList(std::vector<boost::gregorian::date>& list, sql::ResultSet*& res, const int& columnCount,const IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum& field,const IntradayBacktest::IntradayMaintenance::Timeframe& timeframe)
{
	list.push_back(StringToGregorian(res->getString(1)));
}

template<>
void PopulateList(std::vector<long double>& list, sql::ResultSet*& res, const int& columnCount,const IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum& field,const IntradayBacktest::IntradayMaintenance::Timeframe& timeframe)
{
	list.push_back(res->getDouble(1));
}

template<>
void PopulateList(std::vector<int>& list, sql::ResultSet*& res, const int& columnCount, const IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum& field,const IntradayBacktest::IntradayMaintenance::Timeframe& timeframe)
{
	list.push_back(res->getInt(1));
}

template<>
void PopulateList(std::vector<std::string>& list, sql::ResultSet*& res, const int& columnCount, const IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum& field,const IntradayBacktest::IntradayMaintenance::Timeframe& timeframe)
{
	list.push_back(res->getString(1));
}
}

namespace IntradayBacktest{

DatabaseProcessing::DatabaseProcessing(const std::string& name, const std::string& ip, const int& port)
{
	m_name = name;
	m_dbIp = ip;
	m_port = port;
}
std::vector<std::tuple<int,int,double>> DatabaseProcessing::GetTickerReturns(const std::string& ticker, const IntradayMaintenance::Timeframe& timeframe,
		std::string& database, const boost::gregorian::date& st, const ReturnsEnum& ret)
{
	std::string returns;
	switch (ret)
	{
		case PositiveReturns:
			returns = "IF(TOD.ClosePrice/YES.ClosePrice > 1, (TOD.ClosePrice/YES.ClosePrice - 1)*100, 0)";
			break;
		case NegativeReturns:
			returns = "IF(TOD.ClosePrice/YES.ClosePrice < 1, (TOD.ClosePrice/YES.ClosePrice - 1)*100, 0)";
			break;
		case Neutral:
			returns = "(TOD.ClosePrice/YES.ClosePrice - 1)*100";
	}
	std::string tb = database + "." + ticker;
	std::string timeStart = boost::gregorian::to_iso_extended_string(st);
	std::string sqlQuery = "SELECT TOD.Date, TOD.Time, TOD.OpenPrice, TOD.HighPrice, TOD.LowPrice," + returns + " FROM " + tb + " TOD "+
			"JOIN " + tb + " YES " +
			"ON TOD.ID = YES.ID + 1 " +
			 "WHERE TOD.Date>='" + timeStart + "' ORDER BY 1,2;";
	//std::cout << sqlQuery << std::endl;
	return DefineResult<std::tuple<int,int,double>>(sqlQuery, database, IntradayMaintenance::StockInfoFieldsEnum::ClosePrice);
}

std::vector<std::tuple<int,double>> DatabaseProcessing::GetDailyQuotes(const std::string& ticker)
{
	std::string sqlQuery = "SELECT * FROM binance_24h." + ticker +"usdt24;";
	return DefineResult<std::tuple<int,double>>(sqlQuery, "binance_24h", IntradayMaintenance::StockInfoFieldsEnum::ClosePrice);
}

std::vector<long double> DatabaseProcessing::GetDoubleValueQuery(const std::string& query, const std::string& database)
{
	return PerformDatabaseQuery<long double>(query, database);
}

boost::gregorian::date DatabaseProcessing::GetTickerTradingPeriodStart(const std::string& database, const std::string& table, const std::string& columnName)
{
	std::string sqlQuery = "SELECT Min("+columnName+") FROM " + database + "." + table + ";";
	return *(DefineResult<boost::gregorian::date>(sqlQuery).begin());
}

boost::gregorian::date DatabaseProcessing::GetTickerTradingPeriodFinish(const std::string& database, const std::string& table, const std::string& columnName)
{
	std::string sqlQuery = "SELECT M("+columnName+") FROM " + database + "." + table + ";";
		return *(DefineResult<boost::gregorian::date>(sqlQuery).begin());
}

std::vector<std::string> DatabaseProcessing::GetDatabases()
{
	std::string query = "SHOW DATABASES;";
	return DefineResult<std::string>(query);
}

std::vector<std::string> DatabaseProcessing::GetTables(const std::string& database)
{
	std::string sqlQuery = "SHOW TABLES";
	return PerformDatabaseQuery<std::string>(sqlQuery, database);
}

std::vector<std::tuple<int,int,IntradayMaintenance::StockInfo>> DatabaseProcessing::GetTickerHistory(const std::string& ticker, const std::string& database,
		const IntradayMaintenance::Timeframe& timeframe, const boost::gregorian::date& timeToStart, const boost::gregorian::date& timeToFinish)
{
	std::string timeStart = boost::gregorian::to_iso_extended_string(timeToStart);
	std::string timeFinish = boost::gregorian::to_iso_extended_string(timeToFinish);
	std::string sqlQuery = "SELECT * FROM " + ticker + (!timeToStart.is_not_a_date() ? " WHERE Date>='" + timeStart : "") + (!timeToFinish.is_not_a_date() ? "' AND Date<='" + timeFinish : "") + "';";
	return DefineResult<std::tuple<int,int,IntradayMaintenance::StockInfo>>(sqlQuery, database, IntradayMaintenance::StockInfoFieldsEnum::ClosePrice, timeframe);
}

std::vector<std::tuple<int,int,double>> DatabaseProcessing::GetTickerHistory(const std::string& ticker,
		const std::string& database, const IntradayMaintenance::Timeframe& timeframe, const IntradayMaintenance::StockInfoFieldsEnum& field, const boost::gregorian::date& timeToStart, const boost::gregorian::date& timeToFinish)
{
	std::string timeStart = boost::gregorian::to_iso_extended_string(timeToStart);
	std::string timeFinish = boost::gregorian::to_iso_extended_string(timeToFinish);
	std::string sqlQuery = "SELECT * FROM " + database + "." + ticker + (!timeToStart.is_not_a_date() ? " WHERE Date>='" + timeStart : "") + (!timeToFinish.is_not_a_date() ? "' AND Date<='" + timeFinish : "") + "';";
	return DefineResult<std::tuple<int,int,double>>(sqlQuery, database, IntradayMaintenance::StockInfoFieldsEnum::ClosePrice);
}


std::vector<std::string> DatabaseProcessing::GetTickers(const std::string& database, const std::string& table)
{
	std::string sqlQuery = "SELECT DISTINCT(Ticker) FROM " + table + "." + database + ";";
	return DefineResult<std::string>(sqlQuery);
}

bool DatabaseProcessing::PerformBasicDatabaseQuery(const std::string& query)
{
	function = __func__;
	SIGNALEXCEPTION(signalHandler);
	  sql::Driver *driver;
	  sql::Connection *con;
	  sql::Statement *stmt;

	  /* Create a connection */
	  driver = get_driver_instance();
	  std::stringstream ss;
	  ss <<  "tcp://" << m_dbIp << ":" <<  m_port << std::endl;
	  std::string connStr = ss.str();
	  con = driver->connect(connStr, this->m_name, "1234");
	  stmt = con->createStatement();
	  bool result = stmt->execute(query);
	  delete stmt;
	  delete con;
	  return result;
}

template <typename T>
std::vector<T> DatabaseProcessing::PerformDatabaseQuery(const std::string& query, const std::string& database)
{
	Retry:
	int attempt = 0;
	function = __func__;
	SIGNALEXCEPTION(signalHandler);
	sql::Driver *driver = nullptr;
	sql::Connection *con = nullptr;
	sql::Statement *stmt = nullptr;
	sql::ResultSet *res = nullptr;
	std::vector<T> result;
	try{

			  /* Create a connection */
			  driver = get_driver_instance();
			  std::stringstream ss;
			  ss <<  "tcp://" << m_dbIp << ":" <<  m_port << std::endl;
			  std::string connStr = ss.str();
			  con = driver->connect(connStr, this->m_name, "1234");
			  con->setSchema(database);
			  stmt = con->createStatement();
			  res = stmt->executeQuery(query);
			  while (res->next())
			  {
				  PopulateList<T>(result, res, 1);
			  }
			  delete res;
			  delete stmt;
			  delete con;
			  return result;
	}
	catch (sql::SQLException &e) {
		try
		{
			if (res != nullptr)
				delete res;
			if (stmt != nullptr)
				delete stmt;
			if (con != nullptr)
				delete con;
		}
		catch (std::exception& ex)
		{
			function = "you won't believe this";
		}
		std::cout << "(" << __FUNCTION__ << ") on line "<< __LINE__ << std::endl;
							  std::cout << "# ERR: " << e.what();
							  std::cout << " (MySQL error code: " << e.getErrorCode();
							  std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
		usleep(1000000);
		if (attempt < 10)
		{
		    ++attempt;
		    goto Retry;
		}
	}
	return result;
}

template <typename T>
std::vector<T> DatabaseProcessing::DefineResult(const std::string& sqlQuery, const std::string& database, const IntradayMaintenance::StockInfoFieldsEnum& field, const IntradayMaintenance::Timeframe& timeframe)
{
	int attempt = 0;
	Retry:
	function = __func__;
	std::vector<T> results;
	sql::Driver *driver = nullptr;
	sql::Connection *con = nullptr;
	sql::Statement *stmt = nullptr;
	sql::ResultSet *res = nullptr;
	SIGNALEXCEPTION(signalHandler);
	try {

		  /* Create a connection */
		  driver = get_driver_instance();
		  std::stringstream ss;
		  ss <<  "tcp://" << m_dbIp << ":" <<  m_port;
		  std::string connStr = ss.str();
		  con = driver->connect(connStr, this->m_name, "1234");

		  con->setSchema("information_schema");
		  stmt = con->createStatement();
		  res = stmt->executeQuery("SELECT COUNT(*) FROM information_schema.columns WHERE table_schema='binance' AND table_name='btcusdt';");
		  int32_t columnCount;
		  while(res->next())
			  columnCount = res->getInt(1);

		  if (!database.empty())
			  con->setSchema(database);

		  stmt = con->createStatement();
		  res = stmt->executeQuery(sqlQuery);
		  while (res->next()) {
			  PopulateList<T>(results, res, columnCount, field, timeframe);
		  }
		  delete res;
		  delete stmt;
		  delete con;
		  return results;
		} catch (sql::SQLException &e) {
			try
			{
				if (res != nullptr)
					delete res;
				if (stmt != nullptr)
					delete stmt;
				if (con != nullptr)
					delete con;
			}
			catch (std::exception& ex)
			{
				function = "you won't believe this...";
			}

			 std::cout << "(" << __FUNCTION__ << ") on line "<< __LINE__ << std::endl;
					  std::cout << "# ERR: " << e.what();
					  std::cout << " (MySQL error code: " << e.getErrorCode();
					  std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
			  if (e.getErrorCode() == 1146)
				  return results;
		  usleep(1000000);
		  if (attempt < 10)
		  {
			  ++attempt;
			  goto Retry;
		  }

		}
		return results;
}

}

