/*
 * Strategy.cpp
 *
 *  Created on: Oct 6, 2019
 *      Author: root
 */

#include "Strategy.h"

Strategy::Strategy() {};

Strategy::Strategy(const std::string& json)
{
	auto endd = json.end();
	auto colon1 = json.find(':',1);
	auto colon2 = json.find(':', colon1+1);
	auto colon3 = json.find(':', colon2+1);
	std::string id = std::string(json.begin() + colon1 + 3,json.begin() + colon2 - 8);
	std::string name = std::string(json.begin() + colon2 + 2, json.begin() + colon3 - 14);
	std::string parameters = std::string(json.begin() + colon3 +2,endd-2);
	m_id = id;
	m_name = name;
	m_parameters = parameters;
}

Strategy::Strategy(const std::string& id, const std::string& name, const std::string& parameters):
m_id(id),
m_name(name),
m_parameters(parameters)
{
	// TODO Auto-generated constructor stub

}

Strategy::Strategy(const StrategyEnum& strEnum, const std::string& id, const std::string& parameters)
{
	std::string name;
	switch (strEnum)
	{
		case StatArbitrage:
		{
			name = "StatArb";
		}
		break;
		case Momentum:
		{
			name = "Momentum";
		}
		break;
	}
	m_name = name;
	m_id = id;
	m_parameters = parameters;
}

std::string Strategy::ToJSON()
{
	std::string json = "{";
	json = json + "\"Id\":" + "\""+m_id + "\",";
	json = json + "\"Name\":" +"\""+ m_name + + "\",";
	if (m_parameters[0] != '{')
	{
		json = json + "\"Parameters\":" + "\""+m_parameters;
		json = json + "\"}";
	}
	else
	{
		json = json + "\"Parameters\":" +m_parameters;
		json = json + "}";
	}
	return json;
}

Strategy::~Strategy() {
	// TODO Auto-generated destructor stub
}

