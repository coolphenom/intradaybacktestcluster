/*
 * Strategy.h
 *
 *  Created on: Oct 6, 2019
 *      Author: root
 */

#ifndef STRATEGY_H_
#define STRATEGY_H_

#include "../Strategies/StrategyEnum.h"

#include <string>

class Strategy {
public:
	Strategy();
	Strategy(const StrategyEnum& strEnum, const std::string& id, const std::string& parameters);
	Strategy(const std::string& json);
	Strategy(const std::string& id, const std::string& name, const std::string& parameters);
	std::string ToJSON();
	virtual ~Strategy();
private:
	std::string m_id;
	std::string m_name;
	std::string m_parameters;
};

#endif /* STRATEGY_H_ */
