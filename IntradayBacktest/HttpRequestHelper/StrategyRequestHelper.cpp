/*
 * StrategyRequestHelper.cpp
 *
 *  Created on: Oct 6, 2019
 *      Author: root
 */

#include <cpr/cpr.h>
#include <iostream>
#include <string>

#include "StrategyRequestHelper.h"

StrategyRequestHelper::StrategyRequestHelper():
m_address("http://192.168.0.3"),
m_port(62000),
m_endpoint("/api/strategy")
{
	// TODO Auto-generated constructor stub
	std::cout <<"Initialized StrategyRequestHelper with default parameters" << std::endl;
}

StrategyRequestHelper::StrategyRequestHelper(const std::string& address, const int& port, const std::string& endpoint) :
m_address(address),
m_port(port),
m_endpoint(endpoint)
{
	std::cout <<"Initialized StrategyRequestHelper with custom parameters" << std::endl;
}

std::vector<Strategy> StrategyRequestHelper::GetRequest()
{
	std::vector<Strategy> strategies;
	auto r = cpr::Get(cpr::Url{m_address + ":" + std::to_string(m_port) + m_endpoint});
	if (r.status_code == 200)
	{
		std::string jArray = r.text;
		if (jArray[0] == '[')
		{
			std::string subjArray = std::string(jArray.begin()+1, jArray.end()-1);
			auto begin = subjArray.begin();
			size_t len = 0;
			auto end = subjArray.end();
			auto brace = subjArray.find('{');
			while (begin + brace != end)
			{
				if (*(begin +brace - 1) == ',')
				{
					std::string json = std::string(begin + len, begin + brace - 1);
					Strategy str(json);
					len +=  json.length();
					strategies.push_back(str);
				}
				brace = subjArray.find('{', brace + 1);
				if (brace == std::string::npos)
				{
					std::string json = std::string(begin + len + 1, end);
					Strategy str(json);
					strategies.push_back(str);
					break;
				}
			}
		}
	}
	return strategies;
}

Strategy StrategyRequestHelper::GetRequest(const std::string& id)
{
	Strategy strategy;
	auto r = cpr::Get(cpr::Url{m_address + ":" + std::to_string(m_port) + m_endpoint +"/id=" + id});
	if (r.status_code == 200)
	{
		std::string json = r.text;
		strategy = Strategy(json);
	}
	return strategy;
}

void StrategyRequestHelper::PostRequest(const std::string& json)
{
	auto r = cpr::Post(cpr::Url{m_address + ":" + std::to_string(m_port) + m_endpoint},
			cpr::Header{{"Content-Type","application/json"}},
			cpr::Body{json});
	if (r.status_code >= 400)
		throw std::runtime_error("bad post request body");
}

void StrategyRequestHelper::PutRequest(const std::string& id, const std::string& json)
{
	auto r = cpr::Put(cpr::Url{m_address + ":" + std::to_string(m_port) + m_endpoint +"/id=" + id},
			cpr::Header{{"Content-Type","application/json"}},
			cpr::Body{json});
	if (r.status_code >= 400)
			throw std::runtime_error("bad put request");
}

void StrategyRequestHelper::DeleteRequest(const std::string& id)
{
	auto r = cpr::Delete(cpr::Url{m_address + ":" + std::to_string(m_port) + m_endpoint +"/id=" + id});
	if (r.status_code >= 400)
		throw std::runtime_error("bad delete request");
}

StrategyRequestHelper::~StrategyRequestHelper() {
	// TODO Auto-generated destructor stub
}

