/*
 * StrategyRequestHelper.h
 *
 *  Created on: Oct 6, 2019
 *      Author: root
 */

#ifndef STRATEGYREQUESTHELPER_H_
#define STRATEGYREQUESTHELPER_H_

#include <string>

#include "Strategy.h"

class StrategyRequestHelper {
public:
	StrategyRequestHelper();
	StrategyRequestHelper(const std::string& address, const int& port, const std::string& endpoint);
	std::vector<Strategy> GetRequest();
	Strategy GetRequest(const std::string& id);
	void PostRequest(const std::string& json);
	void PutRequest(const std::string& id, const std::string& json);
	void DeleteRequest(const std::string& id);
	virtual ~StrategyRequestHelper();
private:
	std::string m_address;
	int m_port;
	std::string m_endpoint;

};

#endif /* STRATEGYREQUESTHELPER_H_ */
