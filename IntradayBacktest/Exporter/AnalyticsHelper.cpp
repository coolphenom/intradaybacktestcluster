/*
 * AnalyticsHelper.cpp
 *
 *  Created on: Nov 20, 2019
 *      Author: root
 */

#include "AnalyticsHelper.h"

#include <cstdlib>
#include <cmath>
#include <iostream>

namespace IntradayBacktest {

AnalyticsHelper::AnalyticsHelper(const std::vector<ExportData>& values, const std::string& dir):
	m_exportedDeals(values),
	m_profitFactor(0),
	m_sharpeRatio(0),
	m_successfulDealsPercent(0),
	m_averageProfit(0),
	m_averageLoss(0),
	m_averageReturn(0),
	m_dir(dir){
	// TODO Auto-generated constructor stub
	m_fee = 0.001;
}

double AnalyticsHelper::GetProfitFactor()
{
	if (m_profitFactor == 0)
	{
		const std::vector<ExportData>& exportData = m_exportedDeals;
		m_pnl = std::vector<double>(exportData.size());
		if (exportData.size() > 0)
			m_ticker = std::string(exportData[0].ticker);
		double profit = 0;
		double loss = 0;
		for (size_t i = 0; i < exportData.size(); ++i)
		{
			double pnl = exportData[i].volume > 0 ?
					(exportData[i].closePrice*(1-m_fee) - exportData[i].enterPrice*(1+m_fee))*exportData[i].volume :
					(-exportData[i].closePrice*(1+m_fee) + exportData[i].enterPrice*(1-m_fee))*(-exportData[i].volume);
			m_pnl[i] = pnl;
			if (pnl > 0)
				profit+=pnl;
			else
				loss+=-pnl;
		}
		std::cout << "Profit " << std::to_string(profit) << "\n" << "Loss " << std::to_string(loss) << std::endl;
		m_profitFactor = profit/loss;
		return m_profitFactor;
	}
	else
	{
		return m_profitFactor;
	}
}

double AnalyticsHelper::GetSharpeRatio()
{
	if (m_sharpeRatio == 0)
	{
		m_returns = std::vector<double>(m_exportedDeals.size());
		const std::vector<ExportData>& exportData = m_exportedDeals;
		double avReturn = 0;
		double std = 0;
		for (size_t i = 0; i < exportData.size(); ++i)
		{
			double ret = exportData[i].volume > 0 ?
						exportData[i].closePrice*(1-m_fee)/exportData[i].enterPrice*(1+m_fee) -1 :
						exportData[i].enterPrice*(1-m_fee)/exportData[i].closePrice*(1+m_fee) - 1;
			avReturn += ret;
			m_returns[i] = ret*100;
			if (i == exportData.size() - 1)
			{
				m_lastDealDay = exportData[i].timeDayOADateEnter;
				m_lastDealMinute = exportData[i].timeMinuteEnter;
			}
		}
		avReturn = avReturn/exportData.size();
		for (size_t i = 0; i < exportData.size(); ++i)
		{
			double ret = exportData[i].volume > 0 ?
						exportData[i].closePrice*(1-m_fee)/exportData[i].enterPrice*(1+m_fee) -1 :
						exportData[i].enterPrice*(1-m_fee)/exportData[i].closePrice*(1+m_fee) - 1;
			std += std::pow(ret-avReturn, 2);

		}
		std = std::sqrt(std/(exportData.size() - 1));
		m_sharpeRatio = avReturn/std;
		return m_sharpeRatio;
	}
	else
	{
		return m_sharpeRatio;
	}
}

std::vector<double> AnalyticsHelper::GetEquityCurve()
{
	if (m_equityCurve.size() == 0)
	{
		std::vector<double> equityCurve(m_exportedDeals.size()+1);
		int i = 0;
		for (const auto& item : m_exportedDeals)
		{
			double pnl = item.volume > 0 ?
							(item.closePrice*(1-m_fee) - item.enterPrice*(1+m_fee))*item.volume :
							(-item.closePrice*(1+m_fee) + item.enterPrice*(1-m_fee))*(-item.volume);
			if (i == 0)
			{
				equityCurve[i] = pnl;
			}
			else
			{
				equityCurve[i] = equityCurve[i - 1] + pnl;
			}
			++i;
		}
		m_equityCurve = equityCurve;
		return m_equityCurve;
	}
	else
	{
		return m_equityCurve;
	}
}

double AnalyticsHelper::GetSuccessfulDealsPercent()
{
	if (m_successfulDealsPercent == 0)
	{
		int count = 0;
		for (const auto& item : m_pnl)
		{
			if (item > 0)
				++count;
		}
		m_successfulDealsPercent =  (double)count/m_pnl.size()*100;
		return m_successfulDealsPercent;
	}
	else
	{
		return m_successfulDealsPercent;
	}
}

double AnalyticsHelper::GetAverageReturn()
{
	if (m_averageReturn == 0)
	{
		double sum = 0;
		for(const auto& item: m_returns)
			sum+=item;
		m_averageReturn = sum/m_returns.size();
		return m_averageReturn;
	}
	else
	{
		return m_averageReturn;
	}
}

double AnalyticsHelper::GetAverageProfit()
{
	if (m_averageProfit == 0)
	{
		double sum = 0;
		for(const auto& item: m_returns)
			if(item > 0)
				sum+=item;
		m_averageProfit = sum/m_returns.size();
		return m_averageProfit;
	}
	else
	{
		return m_averageProfit;
	}
}

double AnalyticsHelper::GetAverageLoss()
{
	if (m_averageLoss == 0)
	{
		double sum = 0;
		for(const auto& item: m_returns)
			if(item < 0)
				sum+=-item;
		m_averageLoss = sum/m_returns.size();
		return m_averageLoss;
	}
	else
	{
		return m_averageLoss;
	}
}

std::string AnalyticsHelper::GetDirectory()
{
	return m_dir;
}

std::string AnalyticsHelper::GetTicker()
{
	return m_ticker;
}

int AnalyticsHelper::GetLastDealDay()
{
	return m_lastDealDay;
}

int AnalyticsHelper::GetLastDealMinute()
{
	return m_lastDealMinute;
}

AnalyticsHelper::~AnalyticsHelper() {
	// TODO Auto-generated destructor stub
}

} /* namespace IntradayBacktest */
