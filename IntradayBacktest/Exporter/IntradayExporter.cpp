/*
 * IntradayExporter.cpp
 *
 *  Created on: Apr 2, 2019
 *      Author: pavlotkachenko
 */

#include "IntradayExporter.h"
#include "AnalyticsHelper.h"
#include "../IntradayMaintenance/Timeframe.h"
#include "../IntradayMaintenance/stl_extension.h"

#include <bits/stdc++.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <assert.h>
#include <boost/regex.hpp>
#include <string>

namespace IntradayBacktest {

IntradayExporter::IntradayExporter() {
	// TODO Auto-generated constructor stub

}

void IntradayExporter::Export(const std::vector<ExportData>& exportData, const std::string& strategy, std::vector<AnalyticsHelper*>& analytics,
		const std::vector<std::vector<std::vector<double>>>& indicators,
		const std::vector<std::unordered_map<std::tuple<int,int>, int>>& indexMappers,
		const std::vector<std::string>& addColumnsNames,
		const std::unordered_map<std::tuple<int,int>, std::tuple<int,int>>& swapMapper)
{
	double fee = 0.001;
	if (exportData.size() > 0)
	{
		bool addData = indicators.size() > 0 && indexMappers.size() > 0;
		std::cout << exportData.size() << std::endl;
		std::string ticker1(exportData[0].ticker);
		std::string ticker2(exportData[1].ticker);
		auto dir = "/media/sf_database/Export/export"+strategy;
		if (mkdir(dir.c_str(), 0777) == -1)
		{
			std::cerr << "Error " << std::strerror(errno);
			std::cerr << std::endl;
		}

		std::string name = dir + "/"+strategy+"_"+ticker1+"_" + ticker2 + ".csv";
		std::ofstream file(name,std::ios_base::trunc);
		AnalyticsHelper* h = new AnalyticsHelper(exportData, dir);
		file << "Profit factor " << std::to_string(h->GetProfitFactor()) << std::endl;
		file << "Sharpe ratio " << std::to_string(h->GetSharpeRatio()) << std::endl;
		auto equity = h->GetEquityCurve();
		file << "Average Return "<< std::to_string(h->GetAverageReturn()) << "%" << std::endl;
		file << "Average Profit "<< std::to_string(h->GetAverageProfit()) << "%" << std::endl;
		file << "Average Loss "<< std::to_string(h->GetAverageLoss()) << "%" << std::endl;
		file << "Successful Deals " << std::to_string(h->GetSuccessfulDealsPercent()) <<"%" << std::endl;
		std::cout << "Successful Deals " << std::to_string(h->GetSuccessfulDealsPercent()) <<"%" << std::endl;
		file << "Ticker" << "," << "DateEntry" << "," << "TimeEntry" << ","
				<< "DateClose" << "," << "TimeClose" << "," << "Volume" << ","
				<< "OpenPrice" << "," << "ClosePrice" << "," << "Ma" << ","
				<< "Std" << "," << "Sigma" << "," << "Coef1" << "," << "Coef2" << "," << "PnL" <<"," << "Sum";
		if (addColumnsNames.size() == 0)
		{
			file << "\n";
		}
		if (addColumnsNames.size() > 0)
		{
			for (const auto& item: addColumnsNames)
			{
				file << "," << item;
			}
			file << ",\n";
		}
		analytics.push_back(h);
		for (size_t i = 0; i < exportData.size();++i)
		{
			{
				auto line = exportData[i];
				double deal = line.volume > 0 ? (line.closePrice*(1-fee) - line.enterPrice*(1+fee))*line.volume : (line.enterPrice*(1-fee) - line.closePrice*(1+fee))*(-line.volume);
				//double deal = line.volume > 0 ? (line.closePrice - line.enterPrice)*line.volume : 0;
				file << line.ticker << "," << line.timeDayOADateEnter << "," << line.timeMinuteEnter << "," << line.timeDayOADateClose << "," << line.timeMinuteClose << "," << line.volume <<
						"," << line.enterPrice << "," << line.closePrice << "," << line.ma << "," << line.std << "," << line.sigma <<","
						<< line.coefficients[0] << "," << line.coefficients[1] << "," << deal << "," << equity[i] << ",";
				if (addData)
				{

					for (size_t j = 0; j < indexMappers.size(); ++j)
					{
						auto key = std::make_tuple(line.timeDayOADateEnter, line.timeMinuteEnter);
						int index = 0;
						if (j == 0)
						{
							index = indexMappers[j][key];
						}
						else
						{

							key = swapMapper[key];
							index = indexMappers[j][key];
						}
						{
							for (const auto& item: indicators[j])
							{
								file << item[index] << ",";
							}
						}

					}
					file << std::endl;
				}
				else
				{

					file << "\n";
				}
			}

		}
		file.close();
		std::cout << "Exported \n";
	}
}

void IntradayExporter::DataForLiveTrading(const std::vector<std::vector<ExportData>>& exportData, const double& fee, const double& pfThreshold)
{
	std::vector<std::string> strategyData;
	for (const auto& list: exportData)
	{
		std::string ticker1(list[0].ticker);
		std::string ticker2(list[1].ticker);
		auto tick1 = list[0].ticker;
		auto tick2 = list[1].ticker;
		double profit1 = 0;
		double loss1 = 0;
		double profit2 = 0;
		double loss2 = 0;
		for (const auto& item : list)
		{
			double result = item.volume > 0 ? (item.closePrice - item.enterPrice)* item.volume -
						fee*item.volume*(item.closePrice+item.enterPrice) : 0;
			if (result >= 0)
				if (item.ticker == tick1)
					profit1+=result;
				else
					profit2+=result;
			else
				if (item.ticker == tick1)
					loss1+= -result;
				else
					loss2+= -result;

		}
		double pf1 = profit1/loss1;
		double pf2 = profit2/loss2;
		std::vector<double> pfs = {pf1, pf2};
		for (size_t i = 0; i < pfs.size(); ++i)
		{
			if (pfs[i] > pfThreshold)
			{
				std::vector<ExportData> infoForStrategy;
				ExportData longData;
				ExportData shortData;
				auto it = *list.begin();
				std::transform(ticker1.begin(), ticker1.end(), ticker1.begin(), ::toupper);
				std::transform(ticker2.begin(), ticker2.end(), ticker2.begin(), ::toupper);
				ExportData oppData;
				for (const auto& item : list)
				{
					if (item.ticker == it.ticker && item.volume/abs(item.volume) != it.volume/abs(it.volume))
					{
						oppData = item;
						break;
					}
				}
				if (oppData.ticker == it.ticker)
				{
					if (it.volume > 0)
					{
						longData = it;
						shortData = oppData;
					}
					else
					{
						longData = oppData;
						shortData = it;
					}
					infoForStrategy.push_back(longData);
					infoForStrategy.push_back(shortData);
				}
				else
				{
					if (it.volume > 0)
					{
						longData = it;
						infoForStrategy.push_back(longData);
					}
					else
					{
						shortData = it;
					}

				}
				for (const auto& item : infoForStrategy)
				{
					auto t1 = GetCurrencies(ticker1);
					auto t2 = GetCurrencies(ticker2);
					std::stringstream s;
					s << "Binance" << "," << (item.volume > 0 ? "Long" : "Short") << "," << "Bollinger" << "," << t1.first << "/" << t1.second << ","
							<< t2.first << "/" << t2.second << "," << item.coefficients[0] << "," << item.coefficients[1] << "," <<
						item.ma <<  "," << item.std << "," << item.sigma << "," << i+1 << "," << "1min" << "," << "0.1" << "," << "-2" << "," << "3";
					strategyData.push_back(s.str());
				}
		}


		}
	}
	std::string nameStrat = "/media/sf_database/Export/export/statarb_strategy.csv";
	std::ofstream file(nameStrat);
	for (const auto& item : strategyData)
	{
		file << item << std::endl;
	}
	file.close();
}

std::pair<std::string,std::string> IntradayExporter::GetCurrencies(const std::string& ticker)
{
	std::string tick = ticker;
	std::vector<std::string> keyWords = {"BTC","USDT","ETH","BNB","PAX","USDC","XRP","TUSD"};
	for (const auto& keyWord : keyWords)
	{
		boost::smatch result;
		std::string regPattern = "(\\D+)(" + keyWord + ")";
		boost::regex regEx(regPattern);
		if (boost::regex_match(tick, result, regEx))
		{
			return std::make_pair(result[1], keyWord);
		}
	}
	return std::make_pair("","");
}

IntradayExporter::~IntradayExporter() {
	// TODO Auto-generated destructor stub
}

} /* namespace IntradayBacktest */
