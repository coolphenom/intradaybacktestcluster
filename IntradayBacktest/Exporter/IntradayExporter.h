/*
 * IntradayExporter.h
 *
 *  Created on: Apr 2, 2019
 *      Author: pavlotkachenko
 */

#ifndef INTRADAYEXPORTER_H_
#define INTRADAYEXPORTER_H_

#include "../IntradayMaintenance/ExportData.h"
#include "../IntradayMaintenance/stl_extension.h"
#include "AnalyticsHelper.h"
#include "../src/LogDuration.h"

#include <vector>
#include <map>
#include <string>
#include <unordered_map>

namespace IntradayBacktest {

class IntradayExporter {
public:
	IntradayExporter();
	void Export(const std::vector<ExportData>& exportData, const std::string& strategy, std::vector<AnalyticsHelper*>& analytics,
			const std::vector<std::vector<std::vector<double>>>& indicators = std::vector<std::vector<std::vector<double>>>(),
			const std::vector<std::unordered_map<std::tuple<int,int>, int>>& indexMappers = std::vector<std::unordered_map<std::tuple<int,int>, int>>(),
			const std::vector<std::string>& addColumnsNames = std::vector<std::string>(),
			const std::unordered_map<std::tuple<int,int>, std::tuple<int,int>>& swapMapper= std::unordered_map<std::tuple<int,int>, std::tuple<int,int>>());
	void DataForLiveTrading(const std::vector<std::vector<ExportData>>& exportData, const double& fee, const double& pfThreshold);
	void Analyze(const std::string& folderPath);
	virtual ~IntradayExporter();

private:
	std::pair<std::string,std::string> GetCurrencies(const std::string& ticker);
};

} /* namespace IntradayBacktest */

#endif /* INTRADAYEXPORTER_H_ */
