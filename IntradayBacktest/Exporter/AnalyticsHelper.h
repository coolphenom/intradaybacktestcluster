/*
 * AnalyticsHelper.h
 *
 *  Created on: Nov 20, 2019
 *      Author: root
 */

#ifndef ANALYTICSHELPER_H_
#define ANALYTICSHELPER_H_

#include "../IntradayMaintenance/ExportData.h"

#include <vector>
#include <string>

namespace IntradayBacktest {

class AnalyticsHelper {
public:
	AnalyticsHelper(const std::vector<ExportData>& values, const std::string& dir);
	double GetProfitFactor();
	double GetSharpeRatio();
	double GetSuccessfulDealsPercent();
	double GetAverageReturn();
	double GetAverageProfit();
	double GetAverageLoss();
	std::vector<double> GetEquityCurve();
	std::string GetDirectory();
	std::string GetTicker();
	int GetLastDealDay();
	int GetLastDealMinute();
	virtual ~AnalyticsHelper();
private:
	double m_fee;
	std::vector<ExportData> m_exportedDeals;
	std::vector<double> m_pnl;
	std::vector<double> m_returns;
	std::vector<double> m_equityCurve;
	double m_profitFactor;
	double m_sharpeRatio;
	double m_successfulDealsPercent;
	double m_averageProfit;
	double m_averageLoss;
	double m_averageReturn;
	std::string m_dir;
	std::string m_ticker;
	int m_lastDealDay;
	int m_lastDealMinute;
};

} /* namespace IntradayBacktest */

#endif /* ANALYTICSHELPER_H_ */
