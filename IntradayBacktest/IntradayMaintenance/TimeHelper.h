/*
 * TimeHelper.h
 *
 *  Created on: Nov 11, 2019
 *      Author: root
 */

#ifndef TIMEHELPER_H_
#define TIMEHELPER_H_

#include "CustomExceptionHandler.h"
#include "Timeframe.h"
#include "StockInfo.h"
#include "stl_extension.h"

#include <boost/date_time/gregorian/gregorian.hpp>
#include <locale>
#include <iostream>
#include <sstream>
#include <tuple>
#include <unordered_map>

boost::gregorian::date Today();
std::unordered_map<size_t,size_t> GetTimeframeMapper(const IntradayBacktest::IntradayMaintenance::Timeframe& in,
		const IntradayBacktest::IntradayMaintenance::Timeframe& out, const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& framesIn,
		const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& framesOut);
std::unordered_map<std::tuple<int,int>,int> GetTimeframeMapper(const IntradayBacktest::IntradayMaintenance::Timeframe& in,
		const std::tuple<int,int>& start, const std::tuple<int,int>& finish, const int& count);
std::unordered_map<std::tuple<int,int>,std::tuple<int,int>> GetSwapTimeframeMapper(
		const IntradayBacktest::IntradayMaintenance::Timeframe& in,
		const IntradayBacktest::IntradayMaintenance::Timeframe& out,
		const std::tuple<int,int>& start, const std::tuple<int,int>& finish);


#endif /* TIMEHELPER_H_ */
