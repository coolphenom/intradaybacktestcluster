/*
 * StatisticsHelper.h
 *
 *  Created on: Feb 4, 2020
 *      Author: root
 */

#ifndef STATISTICSHELPER_H_
#define STATISTICSHELPER_H_

#include "../../DatabaseProcessing/Filters/Indicators/SMA.h"

#include <vector>

class StatisticsHelper {
public:
	StatisticsHelper(const std::vector<double>& timeSeries);
	StatisticsHelper(const IntradayBacktest::DatabaseAnalysis& trader, const int& length);
	virtual ~StatisticsHelper();
	double GetQuantileOfCDF(const double& value);
	std::vector<double> GetStd();
	std::vector<double> GetZScoreBollingerTest();
private:
	std::vector<double> m_values;
	std::vector<double> m_rawValues;
	SMA* m_sma;
	std::vector<double> m_zScores;
};

#endif /* STATISTICSHELPER_H_ */
