#include "Timeframe.h"

#include <boost/regex.hpp>

namespace IntradayBacktest{

namespace IntradayMaintenance{

int TimeframeHelperMultiplier(const Timeframe& timeframe)
{
	switch(timeframe)
	{
	case OneMinute:
		return 1;
	case FiveMinutes:
		return 5;
	case ThreeMinutes:
		return 3;
	case FifteenMinutes:
		return 15;
	case HalfHour:
		return 30;
	case Hours:
		return 60;
	case TwoHours:
		return 120;
	case ThreeHours:
		return 180;
	case FourHours:
		return 240;
	case SixHours:
		return 360;
	case HalfDay:
		return 720;
	case Days:
		return 1440;
	default:
		return 0;
	}
}

int GetFrameMultiplier(const Timeframe& in, const Timeframe& out)
{
	int m1 = TimeframeHelperMultiplier(in);
	int m2 = TimeframeHelperMultiplier(out);
	return m2/m1;
}

Timeframe GetSeniorTimeframe(const Timeframe& in)
{
	switch (in)
	{
		case OneMinute:
			return FiveMinutes;
		case ThreeMinutes:
			return FiveMinutes;
		case FiveMinutes:
			return FifteenMinutes;
		case FifteenMinutes:
			return HalfHour;
		case HalfHour:
			return Hours;
		case Hours:
			return TwoHours;
		default:
			return Days;
	}
}

std::string TimeframeHelperSuffix(const Timeframe& timeframe)
{
	switch(timeframe)
	{
	case OneMinute:
		return "";
	case ThreeMinutes:
		return "_3m";
	case FiveMinutes:
		return "_5m";
	case FifteenMinutes:
		return "_15m";
	case HalfHour:
		return "_30m";
	case Hours:
		return "_1h";
	case TwoHours:
		return "_2h";
	case ThreeHours:
		return "_3h";
	case FourHours:
		return "_4h";
	case SixHours:
		return "_6h";
	case HalfDay:
		return "_12h";
	case Days:
		return "_24h";
	default:
		return "";
	}
}

Timeframe ConvertStringToTimeframe(const std::string& timeframe)
{

	if(timeframe ==std::string("3m"))
		return Timeframe::ThreeMinutes;
	else if (timeframe == std::string("5m"))
		return Timeframe::FiveMinutes;
	else if(timeframe ==std::string("15m"))
		return Timeframe::FifteenMinutes;
	else if(timeframe ==std::string("30m"))
		return Timeframe::HalfHour;
	else if(timeframe ==std::string("1h"))
		return Timeframe::Hours;
	else if(timeframe ==std::string("2h"))
		return Timeframe::TwoHours;
	else if(timeframe ==std::string("3h"))
		return Timeframe::ThreeHours;
	else if(timeframe ==std::string("4h"))
		return Timeframe::FourHours;
	else if(timeframe ==std::string("6h"))
		return Timeframe::SixHours;
	else if(timeframe ==std::string("12h"))
		return Timeframe::HalfDay;
	else if(timeframe ==std::string("24h"))
		return Timeframe::Days;
	else
		return Timeframe::OneMinute;
}

std::string ConvertTimeframeToString(const Timeframe& timeframe)
{
	switch(timeframe)
		{
		case OneMinute:
			return "1m";
		case ThreeMinutes:
			return "3m";
		case FiveMinutes:
			return "5m";
		case FifteenMinutes:
			return "15m";
		case HalfHour:
			return "30m";
		case Hours:
			return "1h";
		case TwoHours:
			return "2h";
		case ThreeHours:
			return "3h";
		case FourHours:
			return "4h";
		case SixHours:
			return "6h";
		case HalfDay:
			return "12h";
		case Days:
			return "1d";
		default:
			return "";
		}
}

Timeframe ParseTimeframeFromDatabaseName(const std::string& db)
{
	boost::smatch result;
	std::string regPattern = "(\\D+)_(.+)";
	boost::regex regEx(regPattern);
	if (boost::regex_match(db, result, regEx))
	{
		auto res = result[2];
		return ConvertStringToTimeframe(res);
	}
	return Timeframe::OneMinute;
}


}
}
