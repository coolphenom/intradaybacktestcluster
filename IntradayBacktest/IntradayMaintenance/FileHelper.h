/*
 * FileHelper.h
 *
 *  Created on: Nov 23, 2019
 *      Author: root
 */

#ifndef FILEHELPER_H_
#define FILEHELPER_H_

#include <vector>
#include <string>

namespace IntradayBacktest {

std::vector<std::string> GetSubDirectories(const char* path);
std::vector<std::string> GetSubDirectories(const std::string& path);
} /* namespace IntradayBacktest */

#endif /* FILEHELPER_H_ */
