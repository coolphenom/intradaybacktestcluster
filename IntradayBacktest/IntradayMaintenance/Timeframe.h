/*
 * Timeframe.h
 *
 *  Created on: Mar 7, 2019
 *      Author: pavlotkachenko
 */



#ifndef TIMEFRAME_H_
#define TIMEFRAME_H_

#include <string>

namespace IntradayBacktest{

namespace IntradayMaintenance{

enum Timeframe{
	OneMinute=0,
	ThreeMinutes = 1,
	FiveMinutes=2,
	FifteenMinutes=3,
	HalfHour=4,
	Hours=5,
	TwoHours=6,
	ThreeHours=7,
	FourHours=8,
	SixHours=9,
	HalfDay=10,
	Days=11,
	Seconds=12
};

std::string ConvertTimeframeToString(const Timeframe& timeframe);
int TimeframeHelperMultiplier(const Timeframe& timeframe);
std::string TimeframeHelperSuffix(const Timeframe& timeframe);
Timeframe ParseTimeframeFromDatabaseName(const std::string& db);
Timeframe ConvertStringToTimeframe(const std::string& timeframe);
int GetFrameMultiplier(const Timeframe& in, const Timeframe& out);
Timeframe GetSeniorTimeframe(const Timeframe& in);
}
}





#endif //* TIMEFRAME_H_ */
