/*
 * StringHelper.h
 *
 *  Created on: Apr 25, 2020
 *      Author: root
 */

#ifndef STRINGHELPER_H_
#define STRINGHELPER_H_

#include "../DatabaseProcessing/Filters/AbstractFilter.h"
#include "../DatabaseProcessing/Filters/TradingRestrictions.h"

#include <string>

std::string ConvertRestrictionsToString(const TradingRestrictions& restr);
std::string ConvertStrategyModeToString(const StrategyMode& mode);


#endif /* STRINGHELPER_H_ */
