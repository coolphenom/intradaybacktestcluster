/*
 * FileHelper.cpp
 *
 *  Created on: Nov 23, 2019
 *      Author: root
 */

#include "FileHelper.h"

#include <dirent.h>
#include <sys/types.h>
#include <iostream>

namespace IntradayBacktest {

std::vector<std::string> GetSubDirectories(const std::string& path)
{
	return GetSubDirectories(path.c_str());
}

std::vector<std::string> GetSubDirectories(const char* path)
{
	std::vector<std::string> dirs;
	struct dirent* entry;
	DIR* dir = opendir(path);

	if (dir == NULL)
	{
		return dirs;
	}
	else
	{
		while((entry = readdir(dir)) != NULL)
			dirs.push_back(std::string(entry->d_name));
		return dirs;
	}
}
} /* namespace IntradayBacktest */
