/*
 * StringHelper.cpp
 *
 *  Created on: Apr 25, 2020
 *      Author: root
 */

#include "StringHelper.h"

std::string ConvertRestrictionsToString(const TradingRestrictions& restr)
{
	switch (restr)
	{
		case TradingRestrictions::OnlyBuy:
			return "onlybuy";
		case TradingRestrictions::OnlySell:
			return "onlysell";
		default:
			return "df";
	}
}

std::string ConvertStrategyModeToString(const StrategyMode& mode)
{
	switch (mode)
	{
		case StrategyMode::MomentumMode:
			return "mom";
		case StrategyMode::MeanReversal:
			return "meanreversal";
		default:
			return "default";
	}
}




