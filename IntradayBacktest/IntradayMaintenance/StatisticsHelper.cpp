/*
 * StatisticsHelper.cpp
 *
 *  Created on: Feb 4, 2020
 *      Author: root
 */

#include "StatisticsHelper.h"

#include <algorithm>

StatisticsHelper::StatisticsHelper(const std::vector<double>& timeSeries) {
	// TODO Auto-generated constructor stub
	m_values = std::vector<double>(timeSeries.begin(), timeSeries.end());
	std::sort(m_values.begin(), m_values.end());
	m_rawValues = std::vector<double>(timeSeries.begin(), timeSeries.end());
	m_sma = nullptr;
}

StatisticsHelper::StatisticsHelper(const IntradayBacktest::DatabaseAnalysis& trader, const int& length)
{
	auto tr = trader.GetValues();
	m_rawValues = std::vector<double>(tr.size());
	m_values = std::vector<double>(tr.size());
	m_sma = new SMA(trader, length);
	m_zScores = std::vector<double>(tr.size());
	for (size_t i = 0; i < m_rawValues.size(); ++i)
	{
		m_rawValues[i] = std::get<2>(tr[i]);
		m_values[i] = trader.GetCurrentStd(i, length);
		m_zScores[i] = (m_rawValues[i] - m_sma->GetSMA(i))/m_values[i];
	}

}

std::vector<double> StatisticsHelper::GetStd()
{
	return m_values;
}

//Based on linear interpolation, this function returns the quantile of the value
//given as the input
double StatisticsHelper::GetQuantileOfCDF(const double& value)
{
	int size = m_values.size();
	return value/m_values[size-1];
}

std::vector<double> StatisticsHelper::GetZScoreBollingerTest()
{
	return m_zScores;
}

StatisticsHelper::~StatisticsHelper() {
	delete m_sma;
}

