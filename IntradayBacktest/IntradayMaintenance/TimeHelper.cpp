#include "TimeHelper.h"

boost::gregorian::date Today()
{
	return boost::gregorian::day_clock::local_day();
}

std::unordered_map<size_t,size_t> GetTimeframeMapper(const IntradayBacktest::IntradayMaintenance::Timeframe& in,
			const IntradayBacktest::IntradayMaintenance::Timeframe& out, const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& framesIn,
			const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& framesOut)
{
	function = __func__;
	SIGNALEXCEPTION(signalHandler);
	std::unordered_map<size_t,size_t> mapping;
	size_t j = 0;
	size_t i = 0;
	for(const auto& item : framesIn)
	{
		if (i > framesIn.size() - 1 || j > framesOut.size() -1)
			break;
		if (std::get<0>(item) != std::get<0>(framesOut[j]))
		{
			++i;
			continue;
		}
		else
		{
			if (std::get<1>(item) != std::get<1>(framesOut[j]))
			{
				mapping[i] = j;
			}
			else
			{
				++j;
				mapping[i] = j;
			}
			++i;
		}
	}

	return mapping;
}

std::unordered_map<std::tuple<int,int>,int> GetTimeframeMapper(const IntradayBacktest::IntradayMaintenance::Timeframe& in,
		const std::tuple<int,int>& start, const std::tuple<int,int>& finish, const int& count)
{
	function = __func__;
	SIGNALEXCEPTION(signalHandler);
	int multiplier = IntradayBacktest::IntradayMaintenance::TimeframeHelperMultiplier(in);
	std::unordered_map<std::tuple<int,int>,int> mapping;
	if (std::get<0>(start) < std::get<0>(finish) ||
		(std::get<0>(start) == std::get<0>(finish) && std::get<1>(start) < std::get<1>(finish)))
	{
		int day = std::get<0>(start);
		int minute = std::get<1>(start);
		for (size_t i = 0; i < count; ++i)
		{
			mapping[std::make_tuple(day, minute)] = i;
			if (i != count - 1)
				minute += multiplier;
			if (minute != 1440)
				continue;
			else
			{
				minute = 0;
				++day;
			}
		}
		if (day != std::get<0>(finish) || minute != std::get<1>(finish))
			throw std::runtime_error("index mismatch");
	}
	else
		throw std::runtime_error("wrong timeframe mapper parameters");
	return mapping;
}

std::unordered_map<std::tuple<int,int>,std::tuple<int,int>> GetSwapTimeframeMapper(const IntradayBacktest::IntradayMaintenance::Timeframe& in,
		const IntradayBacktest::IntradayMaintenance::Timeframe& out,
		const std::tuple<int,int>& start, const std::tuple<int,int>& finish)
{
	std::unordered_map<std::tuple<int,int>,std::tuple<int,int>> mapper;
	int multiplier = IntradayBacktest::IntradayMaintenance::TimeframeHelperMultiplier(out);
	int multiplierLower =  IntradayBacktest::IntradayMaintenance::TimeframeHelperMultiplier(in);
	int dayFinish = std::get<0>(finish);
	int minuteFinish = std::get<1>(finish);
	int day = std::get<0>(start);
	int minute = std::get<1>(start);
	int minuteCopy = minute;
	while (minuteCopy % multiplier != 0)
		--minuteCopy;
	auto proxy = std::make_tuple(day, minuteCopy);
	while (day < dayFinish || (day == dayFinish && minute <= minuteFinish))
	{
		if (minute % multiplier == 0)
		{
			if (minute == 1440)
			{
				minute = 0;
				++day;
			}
			proxy = std::make_tuple(day, minute);
		}

		mapper[std::make_tuple(day, minute)] = proxy;
		minute += multiplierLower;
	}
	return mapper;
}
