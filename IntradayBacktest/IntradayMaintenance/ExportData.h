/*
 * ExportData.h
 *
 *  Created on: May 10, 2019
 *      Author: root
 */

#ifndef EXPORTDATA_H_
#define EXPORTDATA_H_


struct ExportData
{
	char ticker[12];
	int timeDayOADateEnter;
	int timeMinuteEnter;
	int timeDayOADateClose;
	int timeMinuteClose;
	double volume;
	double enterPrice;
	double closePrice;
	int ma;
	int std;
	double sigma;
	double coefficients[2];
};


#endif /* EXPORTDATA_H_ */
