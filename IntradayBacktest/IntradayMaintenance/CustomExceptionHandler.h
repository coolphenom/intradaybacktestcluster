/*
 * CustomExceptionHandler.h
 *
 *  Created on: Apr 30, 2019
 *      Author: root
 */

#ifndef CUSTOMEXCEPTIONHANDLER_H_
#define CUSTOMEXCEPTIONHANDLER_H_

#include <csignal>
#include <iostream>
#include <exception>
#include <utility>
#include <stdexcept>

namespace
{
	std::string m_localDbIp = "10.1.1.1";
	int m_localDbPort = 65000;
	static size_t m_minDealsBnb = 20;
	static size_t m_minDealsBtc = 10;
	static size_t m_minDealsEth = 10;
	static size_t m_minDealsUsdt = 10;
	std::string function;
	int rankGlobal;

	void SetLocalNetworkSettings(const std::string ip, const int& port)
	{
		m_localDbIp = ip;
		m_localDbPort = port;
	}

	std::pair<std::string,int> GetLocalNetworkSettings()
	{
		auto pair = std::make_pair(m_localDbIp, m_localDbPort);
		return pair;
	}


	void signalHandler(int signalNumber)
	{
		std::cout << "Signal number " << signalNumber << " has interrupted the program in rank "<< rankGlobal << std::endl;
		std::cout << "Signal caught in " << function << std::endl;
		std::string message = std::string(__FILE__)+std::string(" runtime error");
		//throw std::runtime_error(message);
		std::cout << "Skipped" << std::endl;
	}

	void sigterm_handler(int signal, siginfo_t *info, void *_unused)
	{
	  fprintf(stderr, "Received SIGTERM from process with pid = %u\n",
	      info->si_pid);
	  exit(0);
	}
}

#define SIGNALEXCEPTION(handler)\
{								\
	signal(SIGHUP,handler);		\
	signal(SIGINT, handler);	\
	signal(SIGQUIT, handler);	\
	signal(SIGILL, handler);	\
	signal(SIGTRAP, handler);	\
	signal(SIGABRT, handler);	\
	signal(SIGIOT, handler);	\
	signal(SIGBUS, handler);	\
	signal(SIGFPE, handler);	\
	signal(SIGKILL, handler);	\
	signal(SIGUSR1, handler);	\
	signal(SIGSEGV, handler);	\
	signal(SIGUSR2, handler);	\
	signal(SIGPIPE, handler);	\
	signal(SIGALRM, handler);	\
	signal(SIGTERM, handler);	\
	signal(SIGSTKFLT, handler);	\
	signal(SIGCHLD, handler);	\
	signal(SIGCONT, handler);	\
	signal(SIGSTOP, handler);	\
	signal(SIGTSTP, handler);	\
	signal(SIGTTIN, handler);	\
	signal(SIGTTOU, handler);	\
	signal(SIGURG, handler);	\
	signal(SIGXCPU, handler);	\
	signal(SIGXFSZ, handler);	\
	signal(SIGPWR, handler);	\
	signal(SIGSYS, handler); 	\
								\
}



#endif /* CUSTOMEXCEPTIONHANDLER_H_ */
