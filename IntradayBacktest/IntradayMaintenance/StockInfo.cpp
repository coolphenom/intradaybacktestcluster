/*
 * StockInfo.cpp
 *
 *  Created on: Mar 7, 2019
 *      Author: pavlotkachenko
 */

#include "StockInfo.h"

namespace{

int ToOADate(const boost::gregorian::date& time)
{
	return boost::gregorian::date_period(time, boost::gregorian::date(1899, 12, 31)).length().days();
}

}

namespace IntradayBacktest{
namespace IntradayMaintenance {

StockInfo::StockInfo() {
	// TODO Auto-generated constructor stub

}

StockInfo::StockInfo(const StockInfo& prox, const bool& nextDay, const int& step)
{
	if(!nextDay)
	{
		m_date = prox.GetDate();
		m_oaDate = prox.GetOADate();
		auto tmp = prox.GetTime();
		time_t t = mktime(&tmp);
		t += step*60;
		m_time =  *localtime(&t);
		m_min = prox.GetTimeMinutes() + step;
	}
	else
	{
		m_date = prox.GetDate() + boost::gregorian::date_duration(1);
		m_oaDate = prox.GetOADate() + 1;
		struct tm empty = {0};
		m_time = empty;
		m_min = 0;
	}
	m_open = prox.GetOpen();
	m_high = prox.GetHigh();
	m_low = prox.GetLow();
	m_close = prox.GetClose();
	m_volume = 0;
}

StockInfo::~StockInfo() {
	// TODO Auto-generated destructor stub
}
/* namespace IntradayMaintenance */
}
}


