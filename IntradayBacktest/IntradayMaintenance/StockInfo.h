/*
 * StockInfo.h
 *
 *  Created on: Mar 7, 2019
 *      Author: pavlotkachenko
 */

#ifndef STOCKINFO_H_
#define STOCKINFO_H_

#include <ctime>
#include <boost/date_time/gregorian/gregorian.hpp>

namespace IntradayBacktest{

namespace IntradayMaintenance {

class StockInfo {
public:
	StockInfo();
	StockInfo(const StockInfo& prox, const bool& nextDay, const int& step);
	StockInfo(const int& oaDate, const int& minute, const boost::gregorian::date& date, const double& open,
			const double& high, const double& low, const double& close, const double& volume) :
				m_date(date),
				m_open(open),
				m_high(high),
				m_low(low),
				m_close(close),
				m_volume(volume),
				m_oaDate(oaDate),
				m_min(minute){};
	StockInfo(const boost::gregorian::date& date, const std::tm& time, const double& open, const double& high, const double& low,
			const double& close, const double& volume) :
				m_date(date),
				m_time(time),
				m_open(open),
				m_high(high),
				m_low(low),
				m_close(close),
				m_volume(volume),
				m_oaDate(boost::gregorian::date_period(date, boost::gregorian::date(1899, 12, 31)).length().days()),
				m_min(time.tm_hour*60 + time.tm_min){};
	boost::gregorian::date GetDate() const {return m_date;}
	std::tm GetTime() const {return m_time;}
	int GetTimeMinutes() const {return m_min;}
	double GetOpen() const {return m_open;}
	double GetHigh() const {return m_high;}
	double GetLow() const {return m_low;}
	double GetClose() const {return m_close;}
	double GetVolume() const {return m_volume;}
	int GetOADate() const {return m_oaDate;}
	virtual ~StockInfo();
private:
	boost::gregorian::date m_date;
	std::tm m_time;
	double m_open;
	double m_high;
	double m_low;
	double m_close;
	double m_volume;
	int m_oaDate;
	int m_min;
};

enum StockInfoFieldsEnum{
	OpenPrice=0,
	HighPrice=1,
	LowPrice=2,
	ClosePrice=3,
	No = 4
};

}/* namespace IntradayMaintenance */
}



#endif /* STOCKINFO_H_ */
