/*
 * LogDuration.cpp
 *
 *  Created on: Mar 31, 2019
 *      Author: pavlotkachenko
 */

#include "LogDuration.h"

namespace IntradayBacktest {

LogDuration::LogDuration(const std::string& msg) :
		message(msg + ": "),
		start(std::chrono::steady_clock::now())
{
	// TODO Auto-generated constructor stub

}

LogDuration::~LogDuration() {
	auto finish = std::chrono::steady_clock::now();
	auto dur = finish - start;
	std::cerr << message << std::chrono::duration_cast<std::chrono::milliseconds>(dur).count() << " ms"<< std::endl;
	// TODO Auto-generated destructor stub
}

} /* namespace IntradayBacktest */
