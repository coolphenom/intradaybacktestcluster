/*
 * LogDuration.h
 *
 *  Created on: Mar 31, 2019
 *      Author: pavlotkachenko
 */

#ifndef LOGDURATION_H_
#define LOGDURATION_H_

#include <chrono>
#include <iostream>
#include <string>

#define UNIQ_ID_IMPL(lineno) _a_local_var_##lineno
#define UNIQ_ID(lineno) UNIQ_ID_IMPL(lineno)

namespace IntradayBacktest {

class LogDuration {
public:
	explicit LogDuration(const std::string& msg = "");
	virtual ~LogDuration();
private:
	std::string message;
	std::chrono::steady_clock::time_point start;
};

#define LOG_DURATION(message) \
	IntradayBacktest::LogDuration UNIQ_ID(__LINE__){message};

} /* namespace IntradayBacktest */

#endif /* LOGDURATION_H_ */
