/*
 ============================================================================
 Name        : IntradayBacktest.c
 Author      : Pavlo Tkachenko
 Version     :
 Copyright   : Millennial Asset Management 2019
 Description : Compute Pi in MPI C++
 ============================================================================
 */

#include "../DatabaseProcessing/DatabaseAnalysis.h"
#include "../IntradayMaintenance/Timeframe.h"
#include "../IntradayMaintenance/TimeHelper.h"
#include "../IntradayMaintenance/StringHelper.h"
#include "../IntradayMaintenance/StockInfo.h"
#include "../IntradayMaintenance/CustomExceptionHandler.h"
#include "LogDuration.h"
#include "../Strategies/StrategyLauncher.h"

#include <vector>
#include <string>
#include <iostream>

using namespace std;
 
int main(int argc, char *argv[]) {
/*	{
	    int i = 0;
	    char hostname[256];
	    gethostname(hostname, sizeof(hostname));
	    printf("PID %d on %s ready for attach\n", getpid(), hostname);
	    fflush(stdout);
	    while (0 == i)
	        sleep(5);
	}*/

	IntradayBacktest::IntradayMaintenance::Timeframe t = IntradayBacktest::IntradayMaintenance::ParseTimeframeFromDatabaseName("binance_4h");
	StrategyLauncher launcher(2.5,m_localDbIp, m_localDbPort);
	std::string databaseName = "binance";
	IntradayBacktest::IntradayMaintenance::Timeframe timeframe;
	size_t minMa;
	size_t maxMa;
	launcher.m_stepMa = 40;
	size_t minStd;
	size_t maxStd;
	launcher.m_stepStd = 40;
	double minSigma;
	double maxSigma;
	launcher.m_stepSigma = 0.25;
	m_minDealsBnb = 20;
	m_minDealsBtc = 10;
	m_minDealsEth = 10;
	m_minDealsUsdt = 10;
	boost::gregorian::date startSample = boost::gregorian::date(2018,01,01);
	//std::string t = launcher.GetMainCurrency("zilbnb");
	boost::gregorian::date finishSample = Today();//boost::gregorian::date(2018,11,01);
	boost::gregorian::date_duration duration(30);
	boost::gregorian::date_duration trading(14);
	bool onlyExport = true;
	//std::cout << finishSample << std::endl;
	//finishSample += trading;
	//std::cout << finishSample << std::endl;
	//launcher.Test();
	//auto deals = launcher.MPIProxyBalanced(std::string("binance"), startSample, finishSample, static_cast<size_t>(30), static_cast<size_t>(14), true);
	//auto deals = launcher.MPIProxy(std::string("binance"), startSample, finishSample, static_cast<size_t>(30), static_cast<size_t>(14));
	//exp.Export(deals);
	//		databaseName, timeframe, minMa, maxMa, minStd, maxStd, minSigma, maxSigma, startSample,
	//		finishSample, true, false, false);
	std::vector<IntradayBacktest::IntradayMaintenance::Timeframe> v = {//IntradayBacktest::IntradayMaintenance::Timeframe::OneMinute,
			IntradayBacktest::IntradayMaintenance::Timeframe::FiveMinutes,
			//IntradayBacktest::IntradayMaintenance::Timeframe::FifteenMinutes,
			//IntradayBacktest::IntradayMaintenance::Timeframe::HalfHour,
			//IntradayBacktest::IntradayMaintenance::Timeframe::Hours,
			//IntradayBacktest::IntradayMaintenance::Timeframe::TwoHours,
			//IntradayBacktest::IntradayMaintenance::Timeframe::FourHours,
			//IntradayBacktest::IntradayMaintenance::Timeframe::SixHours
			};

	std::vector<std::pair<IntradayBacktest::IntradayMaintenance::Timeframe,IntradayBacktest::IntradayMaintenance::Timeframe>> vv = {
	/*	std::make_pair(IntradayBacktest::IntradayMaintenance::Timeframe::FiveMinutes,IntradayBacktest::IntradayMaintenance::Timeframe::FifteenMinutes),
		std::make_pair(IntradayBacktest::IntradayMaintenance::Timeframe::FifteenMinutes,IntradayBacktest::IntradayMaintenance::Timeframe::Hours),
		std::make_pair(IntradayBacktest::IntradayMaintenance::Timeframe::OneMinute,IntradayBacktest::IntradayMaintenance::Timeframe::FiveMinutes),
		std::make_pair(IntradayBacktest::IntradayMaintenance::Timeframe::FiveMinutes,IntradayBacktest::IntradayMaintenance::Timeframe::Hours),
		std::make_pair(IntradayBacktest::IntradayMaintenance::Timeframe::FiveMinutes,IntradayBacktest::IntradayMaintenance::Timeframe::HalfHour),
			std::make_pair(IntradayBacktest::IntradayMaintenance::Timeframe::FifteenMinutes,IntradayBacktest::IntradayMaintenance::Timeframe::HalfHour),
			std::make_pair(IntradayBacktest::IntradayMaintenance::Timeframe::OneMinute,IntradayBacktest::IntradayMaintenance::Timeframe::FifteenMinutes),
			std::make_pair(IntradayBacktest::IntradayMaintenance::Timeframe::OneMinute,IntradayBacktest::IntradayMaintenance::Timeframe::ThreeMinutes),
			std::make_pair(IntradayBacktest::IntradayMaintenance::Timeframe::ThreeMinutes,IntradayBacktest::IntradayMaintenance::Timeframe::FiveMinutes),
			std::make_pair(IntradayBacktest::IntradayMaintenance::Timeframe::ThreeMinutes,IntradayBacktest::IntradayMaintenance::Timeframe::FifteenMinutes),
			std::make_pair(IntradayBacktest::IntradayMaintenance::Timeframe::ThreeMinutes,IntradayBacktest::IntradayMaintenance::Timeframe::HalfHour),
			std::make_pair(IntradayBacktest::IntradayMaintenance::Timeframe::ThreeMinutes,IntradayBacktest::IntradayMaintenance::Timeframe::Hours),*/
			std::make_pair(IntradayBacktest::IntradayMaintenance::Timeframe::ThreeMinutes,IntradayBacktest::IntradayMaintenance::Timeframe::TwoHours)
			//std::make_pair(IntradayBacktest::IntradayMaintenance::Timeframe::FiveMinutes,IntradayBacktest::IntradayMaintenance::Timeframe::FourHours)
			/*std::make_pair(IntradayBacktest::IntradayMaintenance::Timeframe::FiveMinutes,IntradayBacktest::IntradayMaintenance::Timeframe::TwoHours),
			std::make_pair(IntradayBacktest::IntradayMaintenance::Timeframe::FifteenMinutes,IntradayBacktest::IntradayMaintenance::Timeframe::TwoHours)*/

	};
	std::vector<std::tuple<IntradayBacktest::IntradayMaintenance::Timeframe, IntradayBacktest::IntradayMaintenance::Timeframe,
	TradingRestrictions, int, StrategyMode>> mm = {
			std::make_tuple(IntradayBacktest::IntradayMaintenance::Timeframe::ThreeMinutes,IntradayBacktest::IntradayMaintenance::Timeframe::TwoHours,
					TradingRestrictions::OnlySell, 4, StrategyMode::MomentumMode),
			std::make_tuple(IntradayBacktest::IntradayMaintenance::Timeframe::ThreeMinutes,IntradayBacktest::IntradayMaintenance::Timeframe::TwoHours,
					TradingRestrictions::OnlyBuy, 4, StrategyMode::MomentumMode),
			std::make_tuple(IntradayBacktest::IntradayMaintenance::Timeframe::ThreeMinutes,IntradayBacktest::IntradayMaintenance::Timeframe::TwoHours,
					TradingRestrictions::OnlySell, 4, StrategyMode::MeanReversal),
			std::make_tuple(IntradayBacktest::IntradayMaintenance::Timeframe::ThreeMinutes,IntradayBacktest::IntradayMaintenance::Timeframe::TwoHours,
					TradingRestrictions::OnlyBuy, 4, StrategyMode::MeanReversal)
	};
	bool prod = true;
	for (const auto& item : mm)
	{
		auto tf1 = std::get<0>(item);
		auto tf2 = std::get<1>(item);
		auto rest = std::get<2>(item);
		auto candles = std::get<3>(item);
		auto mode = std::get<4>(item);
		launcher.MPIProxyBalanced("momentum__pS_" + std::to_string(candles) +"fc" + ConvertRestrictionsToString(rest) + ConvertStrategyModeToString(mode)+IntradayBacktest::IntradayMaintenance::ConvertTimeframeToString(tf1)+IntradayBacktest::IntradayMaintenance::ConvertTimeframeToString(tf2),
			tf1, tf2, std::string("binance"), startSample, finishSample, 30, 14, true, true, StrategyEnum::Momentum, prod, rest, candles, mode);
		//launcher.Test("marketModeChange_volPercentile20"+IntradayBacktest::IntradayMaintenance::ConvertTimeframeToString(tf),&StrategyLauncher::OrdinaryStrategyLauncher,
		//launcher.Test("momentum__pumpScanner_4onlysellmomentum"+IntradayBacktest::IntradayMaintenance::ConvertTimeframeToString(tf1)+IntradayBacktest::IntradayMaintenance::ConvertTimeframeToString(tf2),
		//		&StrategyLauncher::MomentumStrategyLauncher,
		//std::string("binance"), tf1, tf2,
		//		startSample, finishSample, false);
	}



	//launcher.Test(&IntradayBacktest::StrategyLauncher::StatArbSortedOptimizedBollinger,
	//										std::string("binance"), IntradayBacktest::IntradayMaintenance::Timeframe::OneMinute,
	//										static_cast<size_t>(40), static_cast<size_t>(1000), static_cast<size_t>(40), static_cast<size_t>(1000),
	//										static_cast<double>(3), static_cast<double>(6.75), startSample, finishSample,
	//										true, false, false);
	//auto deals = launcher.MPIProxyBalanced(std::string("binance"),startSample, finishSample, 30, 14, true, true, StrategyEnum::Ordinary);

	return 0;
}

