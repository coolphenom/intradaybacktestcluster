/*
 * TradingProxy.cpp
 *
 *  Created on: Mar 14, 2019
 *      Author: pavlotkachenko
 */

#include "TradingProxy.h"

#include <algorithm>
#include <math.h>

namespace{

double LinearCombination(const std::vector<std::vector<std::tuple<int,int,double>>>& info,
		const std::vector<double>& coeffs, const int& index)
{
	double val = 0;
	for (size_t i = 0; i < coeffs.size();++i)
	{
		val += std::get<2>(info[i][index]) * std::abs(coeffs[i]);
	}
	return val;
}
}

namespace IntradayBacktest {

TradingProxy::TradingProxy(const double& fee, const double& stopLoss, const double& takeProfit) :
m_fee(fee),
m_takeProfitLevel(takeProfit),
m_stopLossLevel(stopLoss)
{
	// TODO Auto-generated constructor stub
	m_stopLossOn = stopLoss == 0 ? false : true;
	m_takeProfitOn = takeProfit == 0 ? false : true;
}

double TradingProxy::GetProfitFactor() const
{
	double profit = 0;
	double loss = 0;
	for (const auto& item : m_deals)
	{
		double result = std::get<5>(item) >= 0 ? (-std::get<6>(item) + std::get<7>(item))*std::get<5>(item) - m_fee * std::get<5>(item)*(std::get<6>(item)+ std::get<7>(item)) :
				(std::get<6>(item) - std::get<7>(item))*(-std::get<5>(item)) + m_fee * std::get<5>(item)*(std::get<6>(item)+ std::get<7>(item));
		if (result >= 0)
			profit+=result;
		else
			loss+= -result;
	}
	return profit/loss;
}

std::vector<double> TradingProxy::GetProfitFactors() const
{
	std::vector<std::string> tickers;
	std::map<std::string,std::vector<std::tuple<std::string, int, int, int, int, double, double, double>>> dealsSeparated;
	std::map<std::string, double> profits;
	std::map<std::string, double> losses;
	auto iterator = m_deals.begin();
	while (iterator != m_deals.end())
	{
		auto deal = *iterator;
		std::string ticker = std::get<0>(*(iterator));
		if (std::count_if(tickers.begin(), tickers.end(),
				[&ticker](const std::string& item) {return item == ticker;}) == 0)
		{
			tickers.push_back(ticker);
		}

		if (tickers.size() > 0)
		{
			for (size_t i = 0; i < tickers.size(); ++i)
			{
				if (tickers[i] == ticker)
				{
					std::vector<std::tuple<std::string, int, int, int, int, double, double, double>>& dealsStr = dealsSeparated[ticker];
					dealsStr.push_back(deal);
					double result = std::get<5>(deal) >= 0 ? (-std::get<6>(deal) + std::get<7>(deal))*std::get<5>(deal) - m_fee * std::get<5>(deal)*(std::get<6>(deal)+ std::get<7>(deal)) :
							(std::get<6>(deal) - std::get<7>(deal))*(-std::get<5>(deal)) + m_fee * std::get<5>(deal)*(std::get<6>(deal)+ std::get<7>(deal));
					if (result >= 0)
						profits[ticker]+=result;
					else
						losses[ticker]+= -result;
				}
			}
		}
		++iterator;
	}
	std::vector<double> pfs;
	for (const auto& item: tickers)
		pfs.push_back(profits[item]/losses[item]);
	return pfs;
}

const std::vector<std::tuple<std::string, int, int, int, int, double, double, double>>& TradingProxy::DealsList() const
{
	return m_deals;
}

void TradingProxy::AddDealToList(const std::tuple<std::string, int, int, int, int, double, double, double>& deal)
{
	m_deals.push_back(deal);
}

void TradingProxy::BuySpread(const std::vector<std::string>& tickers, const double& moneyToAllocate, const std::vector<double>& coefficients, const int& time,
			std::map<std::string, std::tuple<int, int, double, double>>& sharesStored, const std::vector<std::vector<std::tuple<int,int,double>>>& info, bool longDeal)
{
	double units = moneyToAllocate/LinearCombination(info, coefficients, time);
	for(size_t i = 0; i < coefficients.size(); ++i)
	{
		if (coefficients[i] >= 0)
			Buy(tickers[i], units*coefficients[i], time, sharesStored, longDeal, info[i]);
		else
			Sell(tickers[i], -units*coefficients[i], time, sharesStored, !longDeal, info[i]);
	}
}

void TradingProxy::SellSpread(const std::vector<std::string>& tickers, const double& moneyToAllocate, const std::vector<double>& coefficients, const int& time,
			std::map<std::string, std::tuple<int, int, double, double>>& sharesStored, const std::vector<std::vector<std::tuple<int,int,double>>>& info, bool longDeal)
{
	double units = moneyToAllocate/LinearCombination(info, coefficients, time);
	for(size_t i = 0; i < coefficients.size(); ++i)
	{
		if (coefficients[i] >= 0)
			Sell(tickers[i], units*coefficients[i], time, sharesStored, longDeal, info[i]);
		else
			Buy(tickers[i], -units*coefficients[i], time, sharesStored, !longDeal, info[i]);
	}
}

/*template <typename Type>
double TradingProxy::GetStockPrice(const std::tuple<int,int,Type>& shareInfo)
{
	if (typeid(Type) == typeid(double))
	{
		return GetStockPrice(shareInfo);
	}
	else if (typeid (Type) == typeid(IntradayBacktest::IntradayMaintenance::StockInfo))
	{
		const IntradayBacktest::IntradayMaintenance::StockInfo* pStock = static_cast<const IntradayBacktest::IntradayMaintenance::StockInfo*>(&value);
		return pStock->GetClose();
	}
}*/

template <>
double TradingProxy::GetStockPrice(const std::tuple<int,int,double>& shareInfo)
{
	auto value = std::get<2>(shareInfo);
	return value;
}


template <>
double TradingProxy::GetStockPrice(const std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>& shareInfo)
{
	auto value = std::get<2>(shareInfo);
	return value.GetClose();
}

template <typename Type>
void TradingProxy::Buy(const std::string& ticker, const double& shares, const int& time, std::map<std::string, std::tuple<int, int, double, double>>& sharesStored, bool longDeal, const std::vector<std::tuple<int,int,Type>>& shareInfo)
{
	auto tup = shareInfo[time];
	double price = GetStockPrice(tup);
	if (longDeal)
	{
		//0 -- day
		//1 -- minute
		//2 -- price
		//3 -- volume
		sharesStored[ticker] = std::make_tuple(std::get<0>(tup), std::get<1>(tup), price, shares);
	}
	else
	{
		if (sharesStored.find(ticker) != sharesStored.end())
		{
			auto tuple = sharesStored[ticker];
			AddDealToList(std::make_tuple(ticker, std::get<0>(tuple), std::get<1>(tuple), std::get<0>(tup), std::get<1>(tup), std::get<3>(tuple), std::get<2>(tuple), price));
			sharesStored.erase(ticker);
		}
	}
}

template <typename Type>
void TradingProxy::Sell(const std::string& ticker, const double& shares, const int& time, std::map<std::string, std::tuple<int, int, double, double>>& sharesStored, bool longDeal, const std::vector<std::tuple<int,int,Type>>& shareInfo)
{
	auto tup = shareInfo[time];
	double price = GetStockPrice(tup);
	if (longDeal)
	{
		if (sharesStored.find(ticker) != sharesStored.end())
		{
			auto tuple = sharesStored[ticker];
			AddDealToList(std::make_tuple(ticker, std::get<0>(tuple), std::get<1>(tuple), std::get<0>(tup), std::get<1>(tup), std::get<3>(tuple), std::get<2>(tuple), price));
			sharesStored.erase(ticker);
		}
	}
	else
	{
		//0 -- day
		//1 -- minute
		//2 -- price
		//3 -- volume
		sharesStored[ticker] = std::make_tuple(std::get<0>(tup), std::get<1>(tup), price, -shares);
	}
}

template <typename Type>
void TradingProxy::TakeProfit(const int& time, const double& frac, const std::string& ticker, std::map<std::string, std::tuple<int, int, double, double>>& sharesStored,
		const std::vector<std::tuple<int,int,Type>>& shareInfo)
{

	//currently only for frac = 1
	//need to add fractional sale -- sale only a part, return the rest of the volume as an open deal
	//0 -- day
	//1 -- minute
	//2 -- price
	//3 -- volume
	auto deal = sharesStored[ticker];
	double volToSell = frac * std::get<3>(deal);
	auto tup = shareInfo[time];
	double price = GetStockPrice(tup);
	double priceOpen = std::get<2>(deal);
	double pnl = volToSell > 0 ? (price*(1-m_fee)/priceOpen*(1+m_fee) - 1)*100 : (priceOpen*(1-m_fee)/price*(1+m_fee) - 1)*100;
	if (pnl >= m_takeProfitLevel)
	{
		if (volToSell > 0)
		{
			Sell(ticker, volToSell, time, sharesStored, true, shareInfo);
		}
		else
		{
			Buy(ticker, volToSell, time, sharesStored, false, shareInfo);
		}
	}

}

template <typename Type>
void TradingProxy::StopLoss(const int& time, const double& frac, const std::string& ticker, std::map<std::string, std::tuple<int, int, double, double>>& sharesStored,
		const std::vector<std::tuple<int,int,Type>>& shareInfo)
{
	//currently only for frac = 1
	//need to add fractional sale -- sale only a part, return the rest of the volume as an open deal
	//0 -- day
	//1 -- minute
	//2 -- price
	//3 -- volume
	auto deal = sharesStored[ticker];
	double volToSell = frac * std::get<3>(deal);
	auto tup = shareInfo[time];
	double price = GetStockPrice(tup);
	double priceOpen = std::get<2>(deal);
	double pnl = volToSell > 0 ? 100*(price*(1-m_fee)/priceOpen*(1+m_fee) - 1) : 100*(priceOpen*(1-m_fee)/price*(1+m_fee) - 1);
	if (pnl <= m_stopLossLevel)
	{
		if (volToSell > 0)
		{
			Sell(ticker, volToSell, time, sharesStored, true, shareInfo);
		}
		else
		{
			Buy(ticker, volToSell, time, sharesStored, false, shareInfo);
		}
	}

}

bool TradingProxy::StopLossOn() const
{
	return m_stopLossOn;
}

bool TradingProxy::TakeProfitOn() const
{
	return m_takeProfitOn;
}

TradingProxy::~TradingProxy() {
	// TODO Auto-generated destructor stub
}

template void TradingProxy::Buy(const std::string& ticker, const double& shares, const int& time, std::map<std::string, std::tuple<int, int, double, double>>& sharesStored, bool longDeal, const std::vector<std::tuple<int,int,IntradayMaintenance::StockInfo>>& shareInfo);
template void TradingProxy::Sell(const std::string& ticker, const double& shares, const int& time, std::map<std::string, std::tuple<int, int, double, double>>& sharesStored, bool longDeal, const std::vector<std::tuple<int,int,IntradayMaintenance::StockInfo>>& shareInfo);
template void TradingProxy::TakeProfit(const int& time, const double& frac, const std::string& ticker, std::map<std::string, std::tuple<int, int, double, double>>& sharesStored,
		const std::vector<std::tuple<int,int,IntradayMaintenance::StockInfo>>& shareInfo);
template void TradingProxy::StopLoss(const int& time, const double& frac, const std::string& ticker, std::map<std::string, std::tuple<int, int, double, double>>& sharesStored,
		const std::vector<std::tuple<int,int,IntradayMaintenance::StockInfo>>& shareInfo);

} /* namespace IntradayBacktest */
