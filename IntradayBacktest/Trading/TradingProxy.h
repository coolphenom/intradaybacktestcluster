/*
 * TradingProxy.h
 *
 *  Created on: Mar 14, 2019
 *      Author: pavlotkachenko
 */

#ifndef TRADINGPROXY_H_
#define TRADINGPROXY_H_

#include "../IntradayMaintenance/StockInfo.h"

#include <vector>
#include <tuple>
#include <string>
#include <map>

namespace IntradayBacktest {

//TODO
//Change map into corresponding structure
class TradingProxy {
public:
	TradingProxy(const double& fee, const double& stopLoss = 0, const double& takeProfit = 0);
	double GetProfitFactor() const;
	std::vector<double> GetProfitFactors() const;
	const std::vector<std::tuple<std::string, int, int, int, int, double, double, double>>& DealsList() const;
	void AddDealToList(const std::tuple<std::string, int, int, int, int, double, double, double>& deal);
	void BuySpread(const std::vector<std::string>& tickers, const double& moneyToAllocate, const std::vector<double>& coefficients, const int& time,
			std::map<std::string, std::tuple<int, int, double, double>>& sharesStored, const std::vector<std::vector<std::tuple<int,int,double>>>& info, bool longDeal);
	void SellSpread(const std::vector<std::string>& tickers, const double& moneyToAllocate, const std::vector<double>& coefficients, const int& time,
			std::map<std::string, std::tuple<int, int, double, double>>& sharesStored, const std::vector<std::vector<std::tuple<int,int,double>>>& info, bool longDeal);
	template <typename Type>
	void Buy(const std::string& ticker, const double& shares, const int& time, std::map<std::string, std::tuple<int, int, double, double>>& sharesStored, bool longDeal, const std::vector<std::tuple<int,int,Type>>& shareInfo);
	template <typename Type>
	void Sell(const std::string& ticker, const double& shares, const int& time, std::map<std::string, std::tuple<int, int, double, double>>& sharesStored, bool longDeal, const std::vector<std::tuple<int,int,Type>>& shareInfo);
	template <typename Type>
	void TakeProfit(const int& time, const double& frac, const std::string& ticker, std::map<std::string, std::tuple<int, int, double, double>>& sharesStored,
			const std::vector<std::tuple<int,int,Type>>& shareInfo);
	template <typename Type>
	void StopLoss(const int& time, const double& frac, const std::string& ticker, std::map<std::string, std::tuple<int, int, double, double>>& sharesStored,
			const std::vector<std::tuple<int,int,Type>>& shareInfo);
	bool StopLossOn() const;
	bool TakeProfitOn() const;
	virtual ~TradingProxy();
private:
	template <typename Type>
	double GetStockPrice(const std::tuple<int,int,Type>& shareInfo);
	std::vector<std::tuple<std::string, int, int, int, int, double, double, double>> m_deals;
	double m_fee;
	double m_takeProfitLevel;
	double m_stopLossLevel;
	bool m_stopLossOn;
	bool m_takeProfitOn;
};

} /* namespace IntradayBacktest */

#endif /* TRADINGPROXY_H_ */
