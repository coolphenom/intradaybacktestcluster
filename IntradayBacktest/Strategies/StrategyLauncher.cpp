/*
 * StrategyLauncher.cpp
 *
 *  Created on: Mar 17, 2019
 *      Author: pavlotkachenko
 */

#include "StrategyLauncher.h"

#include "../DatabaseProcessing/DatabaseAnalysis.h"
#include "../Tests/JohansenTest.h"
#include "../src/LogDuration.h"
#include "../IntradayMaintenance/CustomExceptionHandler.h"
#include "../IntradayMaintenance/FileHelper.h"
#include "MPIHelper.h"

#include <bits/stdc++.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <random>
#include <utility>
#include <assert.h>
#include <vector>
#include <algorithm>
#include <exception>
#include <unordered_map>
#include <boost/regex.hpp>
#include <mpi.h>
#include <unistd.h>

StrategyLauncher::StrategyLauncher(const double& pfThreshold, const std::string& localDbIp, const int& localDbPort)
: m_pfThreshold(pfThreshold)
{
	Initialize();
	//SetLocalNetworkSettings(localDbIp, localDbPort);
	// TODO Auto-generated constructor stub
	GetMainCurrenciesQuotes();

}


//const std::vector<TickerPair>&

void StrategyLauncher::GetMainCurrenciesQuotes()
{
	usleep(m_myRank*500000);
	auto ip = GetLocalNetworkSettings().first;
	auto port = GetLocalNetworkSettings().second;
	IntradayBacktest::DatabaseProcessing processing("PAVLO",ip, port);
	std::vector<std::string> quotes = {"btc","eth","bnb"};
	for (const auto& item : quotes)
	{
		std::vector<std::tuple<int,double>> v = processing.GetDailyQuotes(item);
		//std::cout << v.size() << " daily quotes" << std::endl;
		std::unordered_map<int,double> values;
		for (const auto& it : v)
			values[std::get<0>(it)] = std::get<1>(it);
		m_currencies[item] = values;
	}
}

//std::vector<ExportData> StrategyLauncher::StatArbSortedOptimizedBollinger(const std::pair<std::string,std::string>& pair,  const std::string& database, const IntradayMaintenance::Timeframe& timeframe,
//			const size_t& minMaFrame, const size_t& maxMaFrame, const size_t& minStdFrame, const size_t& maxStdFrame,
//			const double& minStd, const double& maxStd, const boost::gregorian::date& startSample, const boost::gregorian::date& startOutOfSample,
//			const bool& optimizeBuySell, const bool& hourlyOptimize, const bool& forex)

std::tuple<std::vector<ExportData>, std::string> StrategyLauncher::OrdinaryStrategyLauncher(std::pair<std::string,std::string> ticker, std::string database,
		IntradayBacktest::IntradayMaintenance::Timeframe timeframe,
		boost::gregorian::date startSample, boost::gregorian::date startOutOfSample, bool forex)
{
	std::vector<ExportData> exportData;
	std::string jsonStrategy;
	std::string suffix = IntradayBacktest::IntradayMaintenance::TimeframeHelperSuffix(timeframe);
	IntradayBacktest::IntradayMaintenance::Timeframe seniorTimeframe = IntradayBacktest::IntradayMaintenance::GetSeniorTimeframe(timeframe);
	std::string suffixSenior = IntradayBacktest::IntradayMaintenance::TimeframeHelperSuffix(seniorTimeframe);
	std::string dbWithSuffix = database + suffix;
	std::string dbSeniorFrame = database + suffixSenior;
	if (suffixSenior == "_24h")
		suffixSenior = "24";
	IntradayBacktest::DatabaseAnalysis analysis("PAVLO",GetLocalNetworkSettings().first, GetLocalNetworkSettings().second);
	std::vector<std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>> info;
	info.push_back(analysis.GetTableCandleInformation(ticker.first+suffix, timeframe, dbWithSuffix, startSample, false));
	auto e = analysis.GetTickerReturns(ticker.first+suffix, timeframe, dbWithSuffix, startSample, IntradayBacktest::ReturnsEnum::NegativeReturns);
	//info.push_back(analysis.GetTableCandleInformation(ticker.first+suffixSenior, seniorTimeframe, dbSeniorFrame, startSample, false));
	analysis.SetValues(e);
	if (info[0].size() > 0  && info[1].size() > 0)
	{
		std::vector<std::tuple<size_t, size_t, double, double, double>> results;
		std::vector<std::tuple<std::string, int, int, int, int, double, double, double, std::vector<double>, size_t, size_t, double, double>> deals;
		std::vector<std::pair<int,int>> outOfSamplePeriods;
		outOfSamplePeriods.push_back(std::make_pair((int)0, (int)info[0].size()));
		std::cout << "Trading " << ticker.first+suffix + " " + suffixSenior << std::endl;
		std::cout << info[0].size() << " candles" << std::endl;
		try
		{
			{
				IntradayBacktest::LogDuration testLd("Market Mode " + ticker.first);
				SIGNALEXCEPTION(signalHandler);
				std::string mainCur = GetMainCurrency(ticker.first);
				OrdinaryStrategy test(analysis, ticker.first, info, std::make_pair(timeframe, seniorTimeframe),
						outOfSamplePeriods, AbstractFilterEnum::MarketModeFilter,
						{}, m_currencies[mainCur]);
				jsonStrategy = "{" +
						ToJson(std::make_pair(STRINGIZE(ticker), ticker), std::make_pair(STRINGIZE(database), database),
								std::make_pair(STRINGIZE(timeframe), timeframe),
								std::make_pair(STRINGIZE(startOutOfSample), startOutOfSample),
								std::make_pair(STRINGIZE(forex),forex)) +
						"\"strategy\""+":" + test.ToJSON() +"}";
				auto d = test.DealsList();
				std::cout << d.size() << " deals" << std::endl;
				//if (test.ProfitFactor() > m_pfThreshold)
				if (true)
				{
					std::cout << "Exporting " << d.size() << "deals" << std::endl;
					for (const auto& item : d)
					{
						function = "mpi_sender_data_loop";
						ExportData data;
						strcpy(data.ticker,std::get<0>(item).c_str());
						data.timeDayOADateEnter = std::get<1>(item);
						data.timeMinuteEnter = std::get<2>(item);
						data.timeDayOADateClose = std::get<3>(item);
						data.timeMinuteClose = std::get<4>(item);
						data.volume = std::get<5>(item);
						data.enterPrice = std::get<6>(item);
						data.closePrice = std::get<7>(item);
						data.coefficients[0] = 0;
						data.coefficients[1] = 0;
						data.ma = 0;
						data.std = 0;
						data.sigma = 0;
						exportData.push_back(data);
					}
				}
			}
		}
		catch (std::exception& ex)
		{
			std::cout << "Something wrong with test..." << std::endl;
		}

	}
	return std::make_tuple(exportData, jsonStrategy);
}

std::tuple<std::vector<ExportData>,std::string> StrategyLauncher::MomentumStrategyLauncher(std::pair<std::string,std::string> ticker, std::string database,  IntradayBacktest::IntradayMaintenance::Timeframe timeframeShort,
		IntradayBacktest::IntradayMaintenance::Timeframe timeframeLong,
		boost::gregorian::date startSample, boost::gregorian::date startOutOfSample, bool forex,
		const TradingRestrictions restr, const int candles, const StrategyMode mode)
{
	std::vector<ExportData> exportData;
	std::string jsonStrategy;
	std::string suffixShort = IntradayBacktest::IntradayMaintenance::TimeframeHelperSuffix(timeframeShort);
	std::string suffixLong = IntradayBacktest::IntradayMaintenance::TimeframeHelperSuffix(timeframeLong);
	std::string dbWithSuffixShort = database+suffixShort;
	std::string dbWithSuffixLong = database+suffixLong;
	IntradayBacktest::DatabaseAnalysis analysis("PAVLO",GetLocalNetworkSettings().first, GetLocalNetworkSettings().second);
	std::vector<std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>> info;
	info.push_back(analysis.GetTableCandleInformation(ticker.first+suffixShort, timeframeShort, dbWithSuffixShort, startSample, false));
	info.push_back(analysis.GetTableCandleInformation(ticker.first+suffixLong, timeframeLong, dbWithSuffixLong, startSample, false));
	analysis.SetCandles(info);
	analysis.SetValues(info[0]);
	if (info[0].size() > 0 && info[1].size() > 0)
	{
		std::vector<std::tuple<size_t, size_t, double, double, double>> results;
		std::vector<std::tuple<std::string, int, int, int, int, double, double, double, std::vector<double>, size_t, size_t, double, double>> deals;
		std::vector<std::pair<int,int>> outOfSamplePeriods;
		outOfSamplePeriods.push_back(std::make_pair((int)0, (int)info[0].size()));
			try
			{
				{
					IntradayBacktest::LogDuration testLd("Pump Scanner " + ticker.first);
					SIGNALEXCEPTION(signalHandler);
					std::string mainCur = GetMainCurrency(ticker.first);
					MomentumStrategy test(analysis, ticker.first, info, outOfSamplePeriods, AbstractFilterEnum::PumpScannerFilter, {}, m_currencies[mainCur],
							restr, candles, mode,
							0, 0);
					jsonStrategy = "{" +
							ToJson(std::make_pair(STRINGIZE(ticker), ticker), std::make_pair(STRINGIZE(database), database),
									std::make_pair(STRINGIZE(timeframeShort), timeframeShort),
									std::make_pair(STRINGIZE(timeframeLong), timeframeLong),
									std::make_pair(STRINGIZE(startSample), startSample),
									std::make_pair(STRINGIZE(startOutOfSample), startOutOfSample),
									std::make_pair(STRINGIZE(forex),forex)) +
							"\"strategy\""+":" + test.ToJSON() +"}";
					auto d = test.DealsList();
					std::cout << d.size() << " deals" << std::endl;
					if (test.ProfitFactor() > 0)
					//if (true)
					{
						std::cout << "Exporting " << d.size() << "deals" << std::endl;
						for (const auto& item : d)
						{
							function = "mpi_sender_data_loop";
							ExportData data;
							strcpy(data.ticker,std::get<0>(item).c_str());
							data.timeDayOADateEnter = std::get<1>(item);
							data.timeMinuteEnter = std::get<2>(item);
							data.timeDayOADateClose = std::get<3>(item);
							data.timeMinuteClose = std::get<4>(item);
							data.volume = std::get<5>(item);
							data.enterPrice = std::get<6>(item);
							data.closePrice = std::get<7>(item);
							data.coefficients[0] = 0;
							data.coefficients[1] = 0;
							data.ma = 0;
							data.std = 0;
							data.sigma = 0;
							exportData.push_back(data);
						}
					}
				}
			}
			catch (std::exception& ex)
			{
				std::cout << "Something wrong with test..." << std::endl;
			}

	}
	return std::make_tuple(exportData, jsonStrategy);
}


std::tuple<std::vector<ExportData>,std::string> StrategyLauncher::StatArbSortedOptimizedBollinger(std::pair<std::string,std::string> pair,  std::string database,
		IntradayBacktest::IntradayMaintenance::Timeframe timeframe,
			size_t minMaFrame, size_t maxMaFrame, size_t minStdFrame, size_t maxStdFrame,
			double minStd, double maxStd, boost::gregorian::date startSample, boost::gregorian::date startOutOfSample,
			bool optimizeBuySell, bool hourlyOptimize, bool forex)
{
	std::vector<ExportData> exportData;
	std::string jsonStrategy;

	std::string suffix = IntradayBacktest::IntradayMaintenance::TimeframeHelperSuffix(timeframe);
	std::string dbWithSuffix = database+suffix;
	IntradayBacktest::DatabaseAnalysis analysis("PAVLO",GetLocalNetworkSettings().first, GetLocalNetworkSettings().second);
	std::vector<std::vector<std::tuple<int,int,double>>> info;
	info.push_back(analysis.GetTableInformation(pair.first, IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum::ClosePrice, timeframe, dbWithSuffix, startSample, forex));
	info.push_back(analysis.GetTableInformation(pair.second, IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum::ClosePrice, timeframe, dbWithSuffix, startSample, forex));
	analysis.SynchronizeVectors(info, startSample);
	int startOAD = boost::gregorian::date_period(boost::gregorian::date(1899, 12, 31),startOutOfSample).length().days();
	size_t frames = std::count_if(info[0].begin(), info[0].end(), [&startOAD](const std::tuple<int,int,double>& tup){return std::get<0>(tup) <= startOAD;});
	if (info[0].size() != info[1].size())
	{
		std::cout << info[0].size() << " "<< info[1].size() << std::endl;
	}
	if (info[0].size() == info[1].size())
	{
		size_t allFrames = info[0].size();
		if (frames > 0)
		{
			std::vector<std::pair<size_t,size_t>> samplePeriods;
			std::vector<std::pair<size_t,size_t>> outOfSamplePeriods;
			if (!hourlyOptimize)
			{
				samplePeriods.push_back(std::make_pair(0, frames));
				outOfSamplePeriods.push_back(std::make_pair(frames, allFrames));
			}
			IntradayBacktest::JohansenTest test(info,samplePeriods[0].first, samplePeriods[0].second, 2);
			//if (!test.Success())
			//{
			//	std::cout << "Singular matrix" << std::endl;
			//	return exportData;
			//}

			const std::vector<std::tuple<int, int, double>>& spreadLoc = test.Spread();
			const std::vector<double>& coefs = test.Coefficients();
			assert(spreadLoc.size() > 0);
			analysis.SetValues(spreadLoc);
			std::vector<std::string> tickers = {pair.first, pair.second};
			if (optimizeBuySell)
			{
				std::vector<std::tuple<size_t, size_t, double, double, double>> resultsBuy;
				std::vector<std::tuple<size_t, size_t, double, double, double>> resultsSell;
				std::cout << "Test " << tickers[0] <<"+" << tickers[1] << std::endl;
				{
					try{
						IntradayBacktest::LogDuration test("ticker test");
						for (size_t i = minMaFrame; i < maxMaFrame; i+= m_stepMa)
						{
							for (size_t j = minStdFrame; j < maxStdFrame; j+= m_stepStd)
							{
								for (double k = minStd; k < maxStd; k+= m_stepSigma)
								{
									RunSampleTest<size_t,size_t,double,double>(analysis, resultsBuy, tickers, info, coefs, {std::make_pair(0, frames)}, AbstractFilterEnum::BollingerBandsFilter,std::make_tuple(i,j,k,0.0,TradingRestrictions::OnlyBuy));
									RunSampleTest<size_t,size_t,double,double>(analysis, resultsSell, tickers , info, coefs, {std::make_pair(0, frames)}, AbstractFilterEnum::BollingerBandsFilter,std::make_tuple(i,j,k,0.0,TradingRestrictions::OnlySell));
								}
							}
						}
					}
					catch (std::exception& ex)
					{
						std::cout << "Something wrong with sample test.." << std::endl;
					}
				}
				auto sortFunc = [](const std::tuple<size_t,size_t,double,double,double>& t1, const std::tuple<size_t,size_t,double,double,double>& t2){
									return std::get<4>(t1) > std::get<4>(t2);};
				auto sortFuncNew = [](const std::tuple<size_t,size_t,double,double,double, size_t>& t1, const std::tuple<size_t,size_t,double,double,double, size_t>& t2){
					return (std::get<4>(t1)-2)*std::get<5>(t1) > (std::get<4>(t2)-2)*std::get<5>(t2);};
				sort(resultsBuy.begin(),resultsBuy.end(), sortFunc);
				sort(resultsSell.begin(),resultsSell.end(), sortFunc);
				std::vector<std::tuple<std::string, int, int, int, int, double, double, double, std::vector<double>, size_t, size_t, double, double>> deals;
				std::tuple<std::string,int> buyTuple;
				if (resultsBuy.size()>0)
				{
					std::vector<std::tuple<size_t, size_t, double, double, double, size_t>> resultsOosBuy;
					for (const auto& item: resultsBuy)
						RunOutOfSampleTest(resultsOosBuy, analysis, deals, tickers, info, coefs, {std::make_pair(frames, allFrames)}, AbstractFilterEnum::BollingerBandsFilter,
								std::make_tuple(std::get<0>(item),std::get<1>(item),std::get<2>(item),std::get<3>(item), TradingRestrictions::OnlyBuy), false);
					sort(resultsOosBuy.begin(), resultsOosBuy.end(), sortFuncNew);
					auto best = *resultsOosBuy.begin();
					buyTuple = RunOutOfSampleTest(resultsOosBuy, analysis, deals, tickers, info, coefs, {std::make_pair(frames, allFrames)}, AbstractFilterEnum::BollingerBandsFilter,
										std::make_tuple(std::get<0>(best),std::get<1>(best),std::get<2>(best),std::get<3>(best), TradingRestrictions::OnlyBuy), true);
				}
				std::tuple<std::string,int> sellTuple;
				if (resultsSell.size()>0)
				{
					std::vector<std::tuple<size_t, size_t, double, double, double, size_t>> resultsOosSell;
					for (const auto& item: resultsSell)
						RunOutOfSampleTest(resultsOosSell, analysis, deals, tickers, info, coefs, {std::make_pair(frames, allFrames)}, AbstractFilterEnum::BollingerBandsFilter,
								std::make_tuple(std::get<0>(item),std::get<1>(item),std::get<2>(item),std::get<3>(item), TradingRestrictions::OnlySell), false);
					sort(resultsOosSell.begin(), resultsOosSell.end(), sortFuncNew);
					auto best = *resultsOosSell.begin();
					sellTuple = RunOutOfSampleTest(resultsOosSell, analysis, deals, tickers, info, coefs, {std::make_pair(frames, allFrames)}, AbstractFilterEnum::BollingerBandsFilter,
										std::make_tuple(std::get<0>(best),std::get<1>(best),std::get<2>(best),std::get<3>(best), TradingRestrictions::OnlySell), true);
				}
				std::cout << "Exporting " << deals.size() << "deals" << std::endl;
				int buyLeg = std::get<1>(buyTuple);
				int sellLeg = std::get<1>(sellTuple);
				std::string buy = std::get<0>(buyTuple);
				std::string sell = std::get<0>(sellTuple);
				std::string js = ToJson(std::make_pair(STRINGIZE(pair),pair), std::make_pair(STRINGIZE(database),database),
						std::make_pair(STRINGIZE(timeframe), timeframe), std::make_pair(STRINGIZE(minMaFrame), minMaFrame),
						std::make_pair(STRINGIZE(maxMaFrame), maxMaFrame), std::make_pair(STRINGIZE(minStdFrame), minStdFrame),
						std::make_pair(STRINGIZE(maxStdFrame), maxStdFrame),
						std::make_pair(STRINGIZE(minStd), minStd), std::make_pair(STRINGIZE(maxStd),maxStd),
						std::make_pair(STRINGIZE(startSample), startSample), std::make_pair(STRINGIZE(startOutOfSample), startOutOfSample),
						std::make_pair(STRINGIZE(optimizeBuySell), optimizeBuySell), std::make_pair(STRINGIZE(hourlyOptimize), hourlyOptimize),
						std::make_pair(STRINGIZE(forex), forex), std::make_pair(STRINGIZE(buyLeg), buyLeg),
						std::make_pair(STRINGIZE(buy), buy), std::make_pair(STRINGIZE(sellLeg), sellLeg),
						std::make_pair(STRINGIZE(sell), sell));
				js.pop_back();
				jsonStrategy = std::string("{") + js + std::string("}");
				for (const auto& item: deals)
				{
					function = "mpi_sender_data_loop";
					SIGNALEXCEPTION(signalHandler);
					try
					{
						ExportData data;
						strcpy(data.ticker,std::get<0>(item).c_str());
						data.timeDayOADateEnter = std::get<1>(item);
						data.timeMinuteEnter = std::get<2>(item);
						data.timeDayOADateClose = std::get<3>(item);
						data.timeMinuteClose = std::get<4>(item);
						data.volume = std::get<5>(item);
						data.enterPrice = std::get<6>(item);
						data.closePrice = std::get<7>(item);
						auto p = std::get<8>(item).data();
						data.coefficients[0] = *p;
						data.coefficients[1] = *(p+1);
						data.ma = static_cast<int>(std::get<9>(item));
						data.std = static_cast<int>(std::get<10>(item));
						data.sigma = std::get<11>(item);
						exportData.push_back(data);
					}
					catch (std::exception& ex)
					{
						std::cout <<"Exception in data processing" << std::endl;
					}

				}

			}
			else
			{
				throw std::runtime_error("not implemented");
			}
		}
		else
		{
			std::cout << "No Frames!" << std::endl;
		}
	}
	else
	{
		std::cout << "Non synchonized!!!" << std::endl;
	}

	return std::make_tuple(exportData,jsonStrategy);
}

StrategyLauncher::~StrategyLauncher() {
	MPI_Finalize();
}

/*template std::vector<ExportData> StrategyLauncher::MPIProxy<std::function<std::vector<ExportData>(const std::pair<std::string,std::string>&,const std::string&, const IntradayMaintenance::Timeframe&,
			const size_t&, const size_t&, const size_t&, const size_t&,
			const double&,const double&, const boost::gregorian::date&, const boost::gregorian::date&,
			const bool&, const bool&, const bool&)>,const std::pair<std::string,std::string>&,const std::string&, const IntradayMaintenance::Timeframe&,
			const size_t&, const size_t&, const size_t&, const size_t&,
			const double&,const double&, const boost::gregorian::date&, const boost::gregorian::date&,
			const bool&, const bool&, const bool&>(const std::string& database, const boost::gregorian::date& start, std::function<std::vector<ExportData>(const std::pair<std::string,std::string>&,const std::string&, const IntradayMaintenance::Timeframe&,
					const size_t&, const size_t&, const size_t&, const size_t&,
					const double&,const double&, const boost::gregorian::date&, const boost::gregorian::date&,
					const bool&, const bool&, const bool&)> str, const std::pair<std::string,std::string>&,const std::string&, const IntradayMaintenance::Timeframe&,
					const size_t&, const size_t&, const size_t&, const size_t&,
					const double&,const double&, const boost::gregorian::date&, const boost::gregorian::date&,
					const bool&, const bool&, const bool&);*/
//} /* namespace IntradayBacktest */
