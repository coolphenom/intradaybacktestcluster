/*
 * StrategyLauncher.h
 *
 *  Created on: Mar 17, 2019
 *      Author: pavlotkachenko
 */

#ifndef STRATEGYLAUNCHER_H_
#define STRATEGYLAUNCHER_H_

#include "../IntradayMaintenance/Timeframe.h"
#include "../DatabaseProcessing/Filters/AbstractFilter.h"
#include "../DatabaseProcessing/Filters/TradingRestrictions.h"
#include "../Exporter/IntradayExporter.h"
#include "../Exporter/AnalyticsHelper.h"
#include "../IntradayMaintenance/ExportData.h"
#include "../IntradayMaintenance/TimeHelper.h"
#include "../IntradayMaintenance/stl_extension.h"
#include "../HttpRequestHelper/StrategyRequestHelper.h"
#include "../IntradayMaintenance/StatisticsHelper.h"
#include "../DatabaseProcessing/Filters/Indicators/SMA.h"
#include "../DatabaseProcessing/Filters/Indicators/RSI.h"
#include "../DatabaseProcessing/Filters/VolatilityPercentile.h"
#include "StatArb.h"
#include "MomentumStrategy.h"
#include "OrdinaryStrategy.h"
#include "StrategyEnum.h"
#include "MPIHelper.h"

#include "DispatcherBalancedRole.h"
#include "ExporterBalancedRole.h"
#include "SlaveBalancedRole.h"

#include <vector>
#include <queue>
#include <utility>
#include <string>
#include <ctime>
#include <thread>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <mpi.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

namespace
{
	//int rankGlobal;
	template<typename Type>
	std::string ToJson(std::pair<const char*,std::pair<Type, Type>> pair)
	{
		std::string json;
		json = json + "\"" + std::string(pair.first) +"\"" + ":" + "[" + "\"" + std::to_string(pair.second.first) + "\"" +"," +
				+ "\"" + std::to_string(pair.second.second) +"\"" + "],";
		return json;
	}

	std::string ToJson(std::pair<const char*, std::pair<std::string, std::string>> pair)
	{
		std::string json;
		json = json + "\"" + std::string(pair.first) +"\"" + ":" + "[" + "\"" + pair.second.first + "\"" +"," +
				+ "\"" + pair.second.second +"\"" + "],";
		return json;
	}

	template <typename Type>
	std::string ToJson(std::pair<const char*,Type> arg)
	{
		std::string json;
		json = json + "\"" + std::string(arg.first) + "\"" +":" + "\"" + std::to_string(arg.second) + "\"" +",";
		return json;
	}

	std::string ToJson(std::pair<const char*,IntradayBacktest::IntradayMaintenance::Timeframe> arg)
	{
		std::string json;
		json = json + "\"" + std::string(arg.first) + "\"" +":" + "\"" + IntradayBacktest::IntradayMaintenance::ConvertTimeframeToString(arg.second) + "\"" +",";
		return json;
	}

	std::string ToJson(std::pair<const char*,std::string> arg)
	{
		std::string json;
		if (*(arg.second.begin()) != '{')
			json = json + "\"" + std::string(arg.first)+ "\"" +":" + "\"" + arg.second + "\"" +",";
		else
			json = json + "\"" + std::string(arg.first)+ "\"" +":"  + arg.second  +",";
		return json;
	}

	std::string ToJson(std::pair<const char*, boost::gregorian::date> arg)
	{
		std::string json;
		json = json + "\"" + std::string(arg.first) + "\"" +":" + "\"" + boost::gregorian::to_simple_string(arg.second) + "\"" +",";
		return json;
	}

	template <typename First, typename ... Rest>
	std::string ToJson(std::pair<const char*, First> arg, Rest... rest)
	{
		std::string json;
		json = json + ToJson(arg) + ToJson(rest...);
		return json;
	}

	void errhandler_function(MPI_Comm* communicator, int* errorCode,...)
	{
		std::cout << "Error handled:" << *errorCode << " in rank "<< rankGlobal << std::endl;
		std::string message = std::string(__FILE__)+std::string(" runtime error in ") + function;
		std::cout << message << std::endl;
		//MPI_Abort(*communicator, *errorCode);
	}
}

class StrategyLauncher {
public:
	void Initialize()
	{
		MPI_Errhandler errhandler;
		MPI_Init(nullptr, nullptr);
		MPI_Comm_rank(MPI_COMM_WORLD, &m_myRank);
		MPI_Comm_size(MPI_COMM_WORLD, &m_processes);
		MPI_Comm_create_errhandler(errhandler_function,&errhandler);
		MPI_Comm_set_errhandler(MPI_COMM_WORLD,errhandler);
	}

	StrategyLauncher(const double& pfThreshold, const std::string& localDbIp, const int& localDbPort);
	std::tuple<std::vector<ExportData>,std::string> StatArbSortedOptimizedBollinger(std::pair<std::string,std::string> pair, std::string database, IntradayBacktest::IntradayMaintenance::Timeframe timeframe,
				size_t minFrame, size_t maxFrame, size_t minStdFrame, size_t maxStdFrame,
				double minStd, double maxStd, boost::gregorian::date startSample, boost::gregorian::date startOutOfSample,
				bool optimizeBuySell, bool hourlyOptimize, bool forex = false);
	std::tuple<std::vector<ExportData>,std::string> MomentumStrategyLauncher(std::pair<std::string,std::string> ticker, std::string database,  IntradayBacktest::IntradayMaintenance::Timeframe timeframeShort,
			IntradayBacktest::IntradayMaintenance::Timeframe timeframeLong,
			boost::gregorian::date startSample, boost::gregorian::date startOutOfSample, bool forex,
			const TradingRestrictions restr, const int candles, const StrategyMode mode);
	std::tuple<std::vector<ExportData>, std::string> OrdinaryStrategyLauncher(std::pair<std::string,std::string> ticker, std::string database,  IntradayBacktest::IntradayMaintenance::Timeframe timeframe,
			boost::gregorian::date startSample, boost::gregorian::date startOutOfSample, bool forex);


	template <typename... Types1, typename... Types2>
	void Test(const std::string& exportFolder, std::tuple<std::vector<ExportData>, std::string> (StrategyLauncher::*strategy)(std::pair<std::string,std::string>, Types1...), Types2&&... types)
	{
		std::vector<TickerPair> pairs = GetPairsDuplicated(std::string("binance"), boost::gregorian::date(2018,1,1));
		auto rng = std::default_random_engine {};
		std::shuffle(std::begin(pairs), std::end(pairs), rng);
		std::vector<IntradayBacktest::AnalyticsHelper*> analytics;
		IntradayBacktest::IntradayMaintenance::Timeframe tf1 = IntradayBacktest::IntradayMaintenance::Timeframe::ThreeMinutes;
		IntradayBacktest::IntradayMaintenance::Timeframe tf2 = IntradayBacktest::IntradayMaintenance::Timeframe::TwoHours;
		IntradayBacktest::DatabaseAnalysis analysis("PAVLO",GetLocalNetworkSettings().first, GetLocalNetworkSettings().second);
		std::string database = "binance";
		std::string suffixShort = IntradayBacktest::IntradayMaintenance::TimeframeHelperSuffix(tf1);
		std::string suffixLong = IntradayBacktest::IntradayMaintenance::TimeframeHelperSuffix(tf2);
		std::string dbWithSuffixShort = database + suffixShort;
		std::string dbWithSuffixLong = database + suffixLong;
		for (const auto& item: pairs)
		{
			auto d = GetExportData(this, strategy, std::make_pair(item.ticker1, item.ticker2), types...);
			IntradayBacktest::IntradayExporter exp;
			std::vector<std::vector<double>> indicatorsShort;
			std::vector<std::vector<double>> indicatorsLong;
			std::vector<std::vector<std::vector<double>>> indicators;
			std::vector<std::unordered_map<std::tuple<int,int>,int>> mappers;
			if (std::get<0>(d).size() > 0)
			{
				ExportData sample = std::get<0>(d)[0];
				/*auto startSample = boost::gregorian::date(2018,01,01);
				std::string ticker = std::string(sample.ticker);
				std::vector<std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>> info;
				info.push_back(analysis.GetTableCandleInformation(ticker+suffixShort, tf1, dbWithSuffixShort, startSample, false));
				analysis.SetValues(info[0]);
				int size = info[0].size();
				SMA smaShort(analysis, 10);
				SMA smaShort15(analysis, 15);
				SMA smaShort20(analysis, 20);
				SMA smaShort25(analysis, 25);
				SMA smaShort30(analysis, 30);
				RSI rsiShort3(analysis, 3, info[0]);
				RSI rsiShort5(analysis, 5, info[0]);
				RSI rsiShort(analysis, 7, info[0]);
				RSI rsiShort9(analysis, 9, info[0]);
				RSI rsiShort11(analysis, 11, info[0]);
				RSI rsiShort13(analysis, 13, info[0]);
				VolatilityPercentile volPerc5(5, analysis);
				VolatilityPercentile volPerc7(7, analysis);
				VolatilityPercentile volPerc9(9, analysis);
				VolatilityPercentile volPerc11(11, analysis);
				VolatilityPercentile volPerc13(13, analysis);
				VolatilityPercentile volPerc15(15, analysis);
				VolatilityPercentile volPerc17(17, analysis);
				VolatilityPercentile volPerc19(19, analysis);
				std::unordered_map<std::tuple<int,int>,int> tfMapper1 = GetTimeframeMapper(tf1, std::make_tuple(std::get<0>(info[0][0]),std::get<1>(info[0][0])),
						std::make_tuple(std::get<0>(info[0][size-1]),std::get<1>(info[0][size-1])), size);
				std::unordered_map<std::tuple<int,int>,std::tuple<int,int>> mapperSwapper =
						GetSwapTimeframeMapper(tf1, tf2, std::make_tuple(std::get<0>(info[0][0]),std::get<1>(info[0][0])),
								std::make_tuple(std::get<0>(info[0][size-1]), std::get<1>(info[0][size-1])));
				indicatorsShort.push_back(smaShort.GetValues());
				indicatorsShort.push_back(smaShort15.GetValues());
				indicatorsShort.push_back(smaShort20.GetValues());
				indicatorsShort.push_back(smaShort25.GetValues());
				indicatorsShort.push_back(smaShort30.GetValues());
				indicatorsShort.push_back(rsiShort3.GetValues());
				indicatorsShort.push_back(rsiShort.GetValues());
				indicatorsShort.push_back(rsiShort9.GetValues());
				indicatorsShort.push_back(rsiShort11.GetValues());
				indicatorsShort.push_back(rsiShort13.GetValues());
				indicatorsShort.push_back(volPerc5.GetValues());
				indicatorsShort.push_back(volPerc7.GetValues());
				indicatorsShort.push_back(volPerc9.GetValues());
				indicatorsShort.push_back(volPerc11.GetValues());
				indicatorsShort.push_back(volPerc13.GetValues());
				indicatorsShort.push_back(volPerc15.GetValues());
				indicatorsShort.push_back(volPerc17.GetValues());
				indicatorsShort.push_back(volPerc19.GetValues());
				info.push_back(analysis.GetTableCandleInformation(ticker+suffixLong, tf2, dbWithSuffixLong, startSample, false));
				analysis.SetValues(info[1]);
				SMA smaLong(analysis, 10);
				SMA smaLong15(analysis, 15);
				SMA smaLong20(analysis, 20);
				SMA smaLong25(analysis, 25);
				SMA smaLong30(analysis, 30);
				RSI rsiLong(analysis, 7, info[1]);
				RSI rsiLong9(analysis, 9, info[1]);
				RSI rsiLong11(analysis, 11, info[1]);
				RSI rsiLong13(analysis, 13, info[1]);
				VolatilityPercentile volPerc5long(5, analysis);
				VolatilityPercentile volPerc7long(7, analysis);
				VolatilityPercentile volPerc9long(9, analysis);
				VolatilityPercentile volPerc11long(11, analysis);
				VolatilityPercentile volPerc13long(13, analysis);
				VolatilityPercentile volPerc15long(15, analysis);
				VolatilityPercentile volPerc17long(17, analysis);
				VolatilityPercentile volPerc19long(19, analysis);
				size = info[1].size();
				indicatorsLong.push_back(smaLong.GetValues());
				indicatorsLong.push_back(smaLong15.GetValues());
				indicatorsLong.push_back(smaLong20.GetValues());
				indicatorsLong.push_back(smaLong25.GetValues());
				indicatorsLong.push_back(smaLong30.GetValues());
				indicatorsLong.push_back(rsiLong.GetValues());
				indicatorsLong.push_back(rsiLong9.GetValues());
				indicatorsLong.push_back(rsiLong11.GetValues());
				indicatorsLong.push_back(rsiLong13.GetValues());
				indicatorsShort.push_back(volPerc5.GetValues());
				indicatorsShort.push_back(volPerc7long.GetValues());
				indicatorsShort.push_back(volPerc9long.GetValues());
				indicatorsShort.push_back(volPerc11long.GetValues());
				indicatorsShort.push_back(volPerc13long.GetValues());
				indicatorsShort.push_back(volPerc15long.GetValues());
				indicatorsShort.push_back(volPerc17long.GetValues());
				indicatorsShort.push_back(volPerc19long.GetValues());
				std::unordered_map<std::tuple<int,int>,int> tfMapper2 = GetTimeframeMapper(tf2, std::make_tuple(std::get<0>(info[1][0]),std::get<1>(info[1][0])),
									std::make_tuple(std::get<0>(info[1][size-1]),std::get<1>(info[1][size-1])), size);
				std::vector<std::string> columns = {"smaShort10", "smaShort15", "smaShort20","smaShort25", "smaShort30",
						"rsiShort5","rsiShort7", "rsiShort9", "rsiShort11", "rsiShort13",
						"volPerc5","volPerc7","volPerc9","volPerc11","volPerc13","volPerc15","volPerc17","volPerc19",
						"smaLong10", "smaLong15","smaLong20","smaLong25","smaLong30",
						"rsiLong7", "rsiLong9", "rsiLong11","rsiLong13",
						"volPerc5long","volPerc7long","volPerc9long","volPerc11long","volPerc13long",
						"volPerc15long","volPerc17long","volPerc19long"};
				indicators.push_back(indicatorsShort);
				indicators.push_back(indicatorsLong);
				mappers.push_back(tfMapper1);
				mappers.push_back(tfMapper2);*/
				exp.Export(std::get<0>(d), exportFolder, analytics);// indicators, mappers, columns, mapperSwapper);
			}
		}

		AnalyzeResults(analytics);

		while (analytics.size() > 0)
		{
			size_t size = analytics.size();
			auto item = analytics[size - 1];
			analytics.pop_back();
			delete item;
		}
		//ParallelForeach(pairs.begin(), pairs.end(), [&strategy, types...] (const TickerPair& item){
		//	auto d = GetExportData(strategy, std::make_pair(item.ticker1, item.ticker2), types...);
		//							IntradayBacktest::IntradayExporter exp;
		//});
	}

	template <typename... Types1, typename... Types2>
		std::vector<ExportData> MPIProxyBalanced(const std::string& exportFolder, const IntradayBacktest::IntradayMaintenance::Timeframe& tf1,
				const IntradayBacktest::IntradayMaintenance::Timeframe& tf2, const std::string& database, const boost::gregorian::date& startSample,
				const boost::gregorian::date& finishSample, size_t&& outOfSampleSegment, size_t&& liveTradingSegment, bool liveTest, bool onlyExport, StrategyEnum strEnum, const bool& prod,
				const TradingRestrictions& restr, const int& candles, const StrategyMode& mode)
		{
			std::vector<ExportData> tradingDeals;
			auto strategy = &StrategyLauncher::MomentumStrategyLauncher;
			if (!prod)
			{
				Test(exportFolder, strategy, database, tf1, tf2, startSample, finishSample, false,
						restr, candles, mode);
				return tradingDeals;
			}

			//struct sigaction act;
			//act.__sigaction_handler = sigterm_handler;
			//sigaction(SIGTERM, &act, NULL);

			boost::gregorian::date_duration duration(outOfSampleSegment);
			boost::gregorian::date_duration trading(liveTradingSegment);
			size_t daysToLiveTrade = liveTradingSegment;
			if (liveTest)
			{
				finishSample = Today() - duration;
			}

			MPI_Status status;
			int  my_rank = m_myRank; /* rank of process */
			int  p = m_processes;       /* number of processes */
			int tag = 0;
			rankGlobal = my_rank;
			ExportData myData;
			MPI_Datatype EXPORT_TYPE = DeclareExportData(myData);
			TickerPair example;
			MPI_Datatype TICKER_TYPE = DeclareTickerPair(example);


			if (my_rank == 0)
			{
				if (liveTest)
				{
					std::vector<std::vector<ExportData>> exportedData;
					MPIExporterBalancedRole(database, startSample, TICKER_TYPE, EXPORT_TYPE, p, exportedData, true);
					if (!onlyExport)
					{
						double pfThreshold = 2;
						double fee = 0.001;
						std::cout << "Trading out of sample from " << finishSample + duration << " to " << finishSample + duration << std::endl;
						IntradayBacktest::IntradayExporter exp;
						exp.DataForLiveTrading(exportedData, fee, pfThreshold);
					}
					else
					{
						std::vector<IntradayBacktest::AnalyticsHelper*> h;
						std::cout << "Exporting trades.. " << std::endl;
						IntradayBacktest::IntradayExporter exp;
						IntradayBacktest::DatabaseAnalysis analysis("PAVLO",GetLocalNetworkSettings().first, GetLocalNetworkSettings().second);
						std::string suffixShort = IntradayBacktest::IntradayMaintenance::TimeframeHelperSuffix(tf1);
						std::string suffixLong = IntradayBacktest::IntradayMaintenance::TimeframeHelperSuffix(tf2);
						std::string dbWithSuffixShort = database + suffixShort;
						std::string dbWithSuffixLong = database + suffixLong;
						for (const auto& item: exportedData)
						{
							if (item.size() > 0)
							{
								/*std::vector<std::vector<double>> indicators;
								ExportData sample = item[0];
								std::string ticker = std::string(sample.ticker);
								std::vector<std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>> info;
								info.push_back(analysis.GetTableCandleInformation(ticker+suffixShort, tf1, dbWithSuffixShort, startSample, false));
								analysis.SetValues(info[0]);
								int size = info[0].size();
								SMA smaShort(analysis, 10);
								std::unordered_map<std::tuple<int,int>,int> tfMapper = GetTimeframeMapper(tf1, std::make_tuple(std::get<0>(info[0][0]),std::get<1>(info[0][0])),
										std::make_tuple(std::get<0>(info[0][size-1]),std::get<1>(info[0][size-1])), size);
								indicators.push_back(smaShort.GetValues());
								info.push_back(analysis.GetTableCandleInformation(ticker+suffixLong, tf2, dbWithSuffixLong, startSample, false));
								analysis.SetValues(info[1]);
								SMA smaLong(analysis, 10);*/
								exp.Export(item, exportFolder, h);
							}
						}
						AnalyzeResults(h);
						while (h.size() > 0)
						{
							size_t size = h.size();
							auto item = h[size - 1];
							h.pop_back();
							delete item;
						}
					}
				}
				else
				{
					//std::vector<IntradayBacktest::AnalyticsHelper*> analytics;
					//std::vector<std::vector<ExportData>> exportedData;
					//MPIExporterBalancedRole(database, startSample, TICKER_TYPE, EXPORT_TYPE, p, exportedData, strEnum);
					/*bool run = true;
					while (run)
					{
						std::vector<std::vector<ExportData>> exportedData;
						MPIExporterBalancedRole(database, startSample, TICKER_TYPE, EXPORT_TYPE, p, exportedData, strEnum);
						MPI_Recv(&run, 1, MPI_C_BOOL, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
						double pfThreshold = 2;
						double fee = 0.001;
						int oaDate = boost::gregorian::date_period(boost::gregorian::date(1899, 12, 31), finishSample + duration).length().days();
						std::cout << "Trading out of sample from " << finishSample + duration << " to " << finishSample + duration + trading << std::endl;
						for (const auto& list: exportedData)
						{
							double profit = 0;
							double loss = 0;
							for (const auto& item : list)
							{
								if (item.timeDayOADateEnter < oaDate)
								{
									double result = item.volume > 0 ? (item.closePrice - item.enterPrice)* item.volume -
												fee*item.volume*(item.closePrice+item.enterPrice) : 0;
										if (result >= 0)
											profit+=result;
										else
											loss+= -result;
								}
							}
							double pf = profit/loss;
							std::vector<ExportData> deals;
							if (pf > pfThreshold)
								for (const auto& item: list)
									if (item.timeDayOADateEnter > oaDate && item.timeDayOADateEnter < oaDate+daysToLiveTrade && item.volume > 0)
										deals.push_back(item);
							std::copy(deals.begin(), deals.end(), std::back_inserter(tradingDeals));
						}
						finishSample = finishSample + trading;
					}*/
				}

			}
			else if (my_rank == 1)
			{
				if (liveTest)
				{
					MPIDispatcherBalancedRole(database, startSample, TICKER_TYPE, EXPORT_TYPE, p, strEnum);
				}
				else
				{
					bool run = true;
					while (run)
					{
						MPIDispatcherBalancedRole(database, startSample, TICKER_TYPE, EXPORT_TYPE, p, strEnum);
						MPI_Recv(&run, 1, MPI_C_BOOL, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
					}
				}

			}
			else
			{
				if (liveTest)
				{
					/*MPISlaveBalancedRole(my_rank, TICKER_TYPE, EXPORT_TYPE, p,  &IntradayBacktest::StrategyLauncher::StatArbSortedOptimizedBollinger,
																std::string("binance"), IntradayBacktest::IntradayMaintenance::Timeframe::OneMinute,
																static_cast<size_t>(100), static_cast<size_t>(1000), static_cast<size_t>(40), static_cast<size_t>(1000),
																static_cast<double>(1), static_cast<double>(3), startSample, finishSample,
																true, false, false);*/
					MPISlaveBalancedRole(this, my_rank, TICKER_TYPE, EXPORT_TYPE, p, strEnum,  strategy,
						std::string("binance"), tf1, tf2, startSample, finishSample, false, restr, candles, mode);
					//MPISlaveBalancedRole(this, my_rank, TICKER_TYPE, EXPORT_TYPE, p, strEnum, &StrategyLauncher::OrdinaryStrategyLauncher,
						//std::string("binance"), tf,
						//startSample, finishSample, false);
					//std::tuple<std::vector<ExportData>,std::string> StrategyLauncher::MomentumStrategyLauncher(std::pair<std::string,std::string> ticker, std::string database,  IntradayBacktest::IntradayMaintenance::Timeframe timeframeShort,
					//		IntradayBacktest::IntradayMaintenance::Timeframe timeframeLong,
					//		boost::gregorian::date startSample, boost::gregorian::date startOutOfSample, bool forex)
				}
				else
				{
					while (finishSample <= Today())
					{
						//MPISlaveBalancedRole(my_rank, TICKER_TYPE, EXPORT_TYPE, p,  &IntradayBacktest::StrategyLauncher::StatArbSortedOptimizedBollinger,
						//					std::string("binance"), IntradayBacktest::IntradayMaintenance::Timeframe::OneMinute,
						//					static_cast<size_t>(40), static_cast<size_t>(1000), static_cast<size_t>(40), static_cast<size_t>(1000),
						//					static_cast<double>(3), static_cast<double>(6.75), startSample, finishSample,
						//					true, false, false);
						MPISlaveBalancedRole(this, my_rank, TICKER_TYPE, EXPORT_TYPE, p, strEnum, strategy,
							std::string("binance"), tf1, tf2, startSample, finishSample, false, restr, candles, mode);
						finishSample = finishSample + trading;

						if (my_rank == 2)
						{
							std::cout << finishSample << " in slave computers" << std::endl;
							bool continueCycle = true;
							MPI_Send(&continueCycle, 1, MPI_C_BOOL, 0, tag, MPI_COMM_WORLD);
							MPI_Send(&continueCycle, 1, MPI_C_BOOL, 1, tag, MPI_COMM_WORLD);
						}
					}
					bool continueCycle = false;
					MPI_Send(&continueCycle, 1, MPI_C_BOOL, 0, tag, MPI_COMM_WORLD);
					MPI_Send(&continueCycle, 1, MPI_C_BOOL, 1, tag, MPI_COMM_WORLD);
				}

			}
			return tradingDeals;
		}


	template <typename... Types1, typename... Types2>
	std::vector<ExportData> MPIProxy(const std::string& database, const boost::gregorian::date& startSample,
			boost::gregorian::date& finishSample, size_t&& outOfSampleSegment, size_t&& liveTradingSegment)
	{

		int  my_rank; /* rank of process */
		int  p;       /* number of processes */
		int tag = 0;
		MPI_Status status;
		MPI_Errhandler errhandler;
		MPI_Init(nullptr, nullptr);
		MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
		MPI_Comm_size(MPI_COMM_WORLD, &p);
		MPI_Comm_create_errhandler(errhandler_function,&errhandler);
		MPI_Comm_set_errhandler(MPI_COMM_WORLD,errhandler);
		ExportData myData;
		MPI_Datatype EXPORT_TYPE = DeclareExportData(myData);
		TickerPair example;
		MPI_Datatype TICKER_TYPE = DeclareTickerPair(example);

		boost::gregorian::date_duration duration(outOfSampleSegment);
		boost::gregorian::date_duration trading(liveTradingSegment);
		size_t daysToLiveTrade = liveTradingSegment;
		std::vector<ExportData> tradingDeals;
		if (my_rank == 0)
		{
			bool run = true;
			while (run)
			{
				std::vector<std::vector<ExportData>> exportedData;
				MPIDispatcherRole(database, startSample, TICKER_TYPE, EXPORT_TYPE, p, exportedData);
				MPI_Recv(&run, 1, MPI_C_BOOL, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
				double pfThreshold = 2;
				double fee = 0.001;
				int oaDate = boost::gregorian::date_period(boost::gregorian::date(1899, 12, 31), finishSample + duration).length().days();
				std::cout << "Trading out of sample from " << finishSample + duration << " to " << finishSample + duration + trading << std::endl;
				for (const auto& list: exportedData)
				{
					double profit = 0;
					double loss = 0;
					for (const auto& item : list)
					{
						if (item.timeDayOADateEnter < oaDate)
						{
							double result = item.volume > 0 ? (item.closePrice - item.enterPrice)* item.volume -
										fee*item.volume*(item.closePrice+item.enterPrice) : 0;
								if (result >= 0)
									profit+=result;
								else
									loss+= -result;
						}
					}
					double pf = profit/loss;
					std::vector<ExportData> deals;
					if (pf > pfThreshold)
						for (const auto& item: list)
							if (item.timeDayOADateEnter > oaDate && item.timeDayOADateEnter < oaDate+daysToLiveTrade && item.volume > 0)
								deals.push_back(item);
					std::copy(deals.begin(), deals.end(), std::back_inserter(tradingDeals));
				}
				finishSample = finishSample + trading;
			}
		}
		else
		{

			while (finishSample < boost::gregorian::date(2019,05,01))
			{
				MPISlaveRole(my_rank, TICKER_TYPE, EXPORT_TYPE, p,  &StrategyLauncher::StatArbSortedOptimizedBollinger,
									std::string("binance"), IntradayBacktest::IntradayMaintenance::Timeframe::OneMinute,
									static_cast<size_t>(40), static_cast<size_t>(1000), static_cast<size_t>(40), static_cast<size_t>(1000),
									static_cast<double>(2), static_cast<double>(4.5), startSample, finishSample,
									true, false, false);
				finishSample = finishSample + trading;

				if (my_rank == 1)
				{
					std::cout << finishSample << " in slave computers" << std::endl;
					bool continueCycle = true;
					MPI_Send(&continueCycle, 1, MPI_C_BOOL, 0, tag, MPI_COMM_WORLD);
				}
			}
			bool continueCycle = false;
			MPI_Send(&continueCycle, 1, MPI_C_BOOL, 0, tag, MPI_COMM_WORLD);
		}
		MPI_Finalize();
		return tradingDeals;
	}

	virtual ~StrategyLauncher();
	//std::string GetMainCurrency(const std::string& ticker);
private:
	std::unordered_map<std::string,std::vector<std::string>> GetTickersAsDictionary(const std::string& database, const boost::gregorian::date& start);
	std::map<std::string,std::unordered_map<int,double>> m_currencies;
	int m_myRank;
	int m_processes;
	void GetMainCurrenciesQuotes();
	const double m_pfThreshold;
	//TODO  make template
	//template <typename... TypesIn, typename... TypesOut>
//	void RunSampleTest(std::vector<std::tuple<TypesOut...>>& results, const std::vector<std::vector<std::tuple<int,int,double>>>& info,
//			const std::vector<double>& coefficients, const std::vector<std::pair<int,int>>& timePeriods, const AbstractFilterEnum& filterEnum, const std::tuple<TypesIn...>&& argsIn);
	template <typename...Types>
	std::string RunSampleTest(const IntradayBacktest::DatabaseAnalysis& analysis, std::vector<std::tuple<Types...,double>>& results, const std::vector<std::string>& tickers, const std::vector<std::vector<std::tuple<int,int,double>>>& info,
					const std::vector<double>& coefficients, const std::vector<std::pair<int,int>>& timePeriods, const AbstractFilterEnum& filterEnum, const std::tuple<Types...,TradingRestrictions>& argsIn)
	{
		std::string mainCur = GetMainCurrency(tickers[0]);
		StatArb<double> test(analysis, tickers, coefficients, info, timePeriods, filterEnum, std::vector<AbstractFilter*>(), argsIn, m_currencies[mainCur]);
		if (test.ProfitFactor() > m_pfThreshold)
		{
			results.push_back(std::make_tuple(std::get<0>(argsIn),std::get<1>(argsIn), std::get<2>(argsIn),std::get<3>(argsIn),test.ProfitFactor()));
		}
		return test.ToJSON();
	}

	template <typename...Types>
	std::tuple<std::string, int> RunOutOfSampleTest(std::vector<std::tuple<Types...,double, size_t>>& results, const IntradayBacktest::DatabaseAnalysis& analysis, std::vector<std::tuple<std::string, int, int, int, int, double, double, double, std::vector<double>, Types...>>& deals, const std::vector<std::string>& tickers, const std::vector<std::vector<std::tuple<int,int,double>>>& info,
					const std::vector<double>& coefficients, const std::vector<std::pair<int,int>>& timePeriods, const AbstractFilterEnum& filterEnum,const std::tuple<Types...,TradingRestrictions>& argsIn, const bool& writeDeals)
	{
		std::string mainCur = GetMainCurrency(tickers[0]);
		StatArb<double> test(analysis, tickers, coefficients, info, timePeriods, filterEnum, std::vector<AbstractFilter*>(), argsIn, m_currencies[mainCur]);
		results.push_back(std::make_tuple(std::get<0>(argsIn),std::get<1>(argsIn), std::get<2>(argsIn),std::get<3>(argsIn),test.ProfitFactor(), test.DealsList().size()));
		if (writeDeals)
		{
			for (const auto& item: test.DealsList())
			{
				deals.push_back(std::make_tuple(std::get<0>(item),std::get<1>(item), std::get<2>(item),std::get<3>(item), std::get<4>(item), std::get<5>(item), std::get<6>(item), std::get<7>(item),
						coefficients, std::get<0>(argsIn),std::get<1>(argsIn), std::get<2>(argsIn),std::get<3>(argsIn)));
			}
		}
		std::vector<double> pfs = test.ProfitFactors();
		auto iter = std::max_element(pfs.begin(), pfs.end());
		return std::make_tuple(test.ToJSON(), iter-pfs.begin());
	}

	//std::vector<ExportData> GetExportData(std::function<std::vector<ExportData>(const IntradayBacktest::StrategyLauncher&, const std::pair<std::string,std::string>&, Types...)> str, const std::pair<std::string,std::string>& pair, Types&&... types)

public:
	size_t m_stepMa;
	size_t m_stepStd;
	double m_stepSigma;
};

//} /* namespace IntradayBacktest */

#endif /* STRATEGYLAUNCHER_H_ */
