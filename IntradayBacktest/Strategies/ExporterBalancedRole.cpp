/*
 * ExporterBalancedRole.cpp
 *
 *  Created on: Jan 26, 2020
 *      Author: root
 */

#include "ExporterBalancedRole.h"

void MPIExporterBalancedRole(const std::string& database, const boost::gregorian::date& start,
				MPI_Datatype tickerType, MPI_Datatype exportType, const int& numOfProcesses, std::vector<std::vector<ExportData>>& exportedData, const bool& live = false)
{
	SIGNALEXCEPTION(signalHandler);
	MPI_Status status;
	int tag=0;
	size_t numberOfPairs = 0;
	std::cout<< "Hello from rank 0 Exporter" << std::endl;
	size_t completed = 0;
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Recv(&numberOfPairs, 1, MPI_UINT32_T, 1, tag, MPI_COMM_WORLD, &status);
	MPI_Barrier(MPI_COMM_WORLD);
	while (completed < numberOfPairs)
	{
		function = "mpi_exporter_loop";
		try
		{
			std::cout << "Receiving info from various processes.." << std::endl;
			int count = 0;
			ExportData* myDataExported = new ExportData[200000];
			std::cout << "Allocated buffer for export.." << std::endl;
			MPI_Recv(myDataExported, 200000, exportType, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
			MPI_Get_count(&status, exportType, &count);
			std::cout << "Received" << count << " deals from "<< status.MPI_SOURCE << " buffer!!! " << std::endl;
			if (count < 0)
			{
				throw std::runtime_error("Error while receiving...");
			}
			std::cout << "Received " << count <<" deals from unknown process" << std::endl;

			std::vector<ExportData> data;
			for (int i = 0; i < count; ++i)
			{
				data.push_back(myDataExported[i]);
			}
			delete[] myDataExported;
			if (data.size() > 0)
			{
				std::string cur = GetMainCurrency(data[0].ticker);
				if (cur == "btc")
				{
					if (data.size() > m_minDealsBtc)
						exportedData.push_back(data);
				}
				else if (cur == "eth")
				{
					if (data.size() > m_minDealsEth)
						exportedData.push_back(data);
				}
				else if (cur == "bnb")
				{
					if (data.size() > m_minDealsBnb)
						exportedData.push_back(data);
				}
				else
				{
					if (data.size() > m_minDealsUsdt)
						exportedData.push_back(data);
				}
				std::cout << "Exported " << data.size() << "deals" << std::endl;
			}
			++completed;
			std::cout << "Exported " << completed <<"/" << numberOfPairs << std::endl;
		}
		catch (std::exception& ex)
		{
			std::cout << "MPI exception while receiving.." << std::endl;
			std::cout << ex.what() << std::endl;
		}

	}
	std::cout << "Exporter has completed his job" << std::endl;
	MPI_Barrier(MPI_COMM_WORLD);
}



