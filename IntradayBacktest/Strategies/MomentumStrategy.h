/*
 * MomentumStrategy.h
 *
 *  Created on: Jul 17, 2019
 *      Author: root
 */

#ifndef MOMENTUMSTRATEGY_H_
#define MOMENTUMSTRATEGY_H_

#include "AbstractStrategy.h"

class MomentumStrategy: public IntradayBacktest::AbstractStrategy<IntradayBacktest::IntradayMaintenance::StockInfo> {
public:
	MomentumStrategy(const IntradayBacktest::DatabaseAnalysis& trader,const std::string& ticker,  const std::vector<std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>>& info,
			const std::vector<std::pair<int,int>>& timePeriods, const AbstractFilterEnum& filter,const std::vector<AbstractFilter*> secondaryFilters, const std::unordered_map<int,double>& values,
			const TradingRestrictions& restr, const int& candles, const StrategyMode& mode,
			const double& stopLoss = 0, const double& takeProfit = 0);
	virtual void Trade(const IntradayBacktest::DatabaseAnalysis& trader, const std::vector<std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>>& info, const std::vector<std::pair<int,int>>& periods, const double& spread) override;
	virtual ~MomentumStrategy();
	virtual std::string ToJSON() override;
};

#endif /* MOMENTUMSTRATEGY_H_ */
