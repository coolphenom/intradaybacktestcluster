/*
 * OrdinaryStrategy.h
 *
 *  Created on: Nov 16, 2019
 *      Author: root
 */

#ifndef ORDINARYSTRATEGY_H_
#define ORDINARYSTRATEGY_H_

#include "AbstractStrategy.h"

#include <vector>
#include <tuple>

class OrdinaryStrategy : public IntradayBacktest::AbstractStrategy<IntradayBacktest::IntradayMaintenance::StockInfo> {
public:
	OrdinaryStrategy(const IntradayBacktest::DatabaseAnalysis& trader,const std::string& ticker,  const std::vector<std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>>& info,
			const std::pair<IntradayBacktest::IntradayMaintenance::Timeframe, IntradayBacktest::IntradayMaintenance::Timeframe>& framesUsed, const std::vector<std::pair<int,int>>& timePeriods, const AbstractFilterEnum& filter,
			const std::vector<AbstractFilter*> secondaryFilters, const std::unordered_map<int,double>& values,
			const double& stopLoss = 0, const double& takeProfit = 0);
	virtual void Trade(const IntradayBacktest::DatabaseAnalysis& trader,
				const std::vector<std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>>& info,
				const std::vector<std::pair<int,int>>& periods, const double& spread) override;
	virtual std::string ToJSON() override;
	virtual ~OrdinaryStrategy();
	int MaxDailyQuotes();
	int MaxQuotes(const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& quotes);
};

#endif /* ORDINARYSTRATEGY_H_ */
