/*
 * DispatcherBalancedRole.h
 *
 *  Created on: Jan 26, 2020
 *      Author: root
 */

#ifndef DISPATCHERBALANCEDROLE_H_
#define DISPATCHERBALANCEDROLE_H_

#pragma once

#include "../IntradayMaintenance/Timeframe.h"
#include "../DatabaseProcessing/Filters/AbstractFilter.h"
#include "../DatabaseProcessing/Filters/TradingRestrictions.h"
#include "../Exporter/IntradayExporter.h"
#include "../Exporter/AnalyticsHelper.h"
#include "../IntradayMaintenance/ExportData.h"
#include "../IntradayMaintenance/TimeHelper.h"
#include "../IntradayMaintenance/stl_extension.h"
#include "../HttpRequestHelper/StrategyRequestHelper.h"
#include "StatArb.h"
#include "MomentumStrategy.h"
#include "OrdinaryStrategy.h"
#include "StrategyEnum.h"
#include "MPIHelper.h"

#include <vector>
#include <queue>
#include <utility>
#include <string>
#include <ctime>
#include <thread>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <mpi.h>

void MPIDispatcherBalancedRole(const std::string& database, const boost::gregorian::date& start,
				MPI_Datatype tickerType, MPI_Datatype exportType, const int& numOfProcesses, StrategyEnum strEnum);

void MPIDispatcherRole(const std::string& database, const boost::gregorian::date& start,
			MPI_Datatype tickerType, MPI_Datatype exportType, const int& numOfProcesses, std::vector<std::vector<ExportData>>& exportedData);

#endif /* DISPATCHERBALANCEDROLE_H_ */
