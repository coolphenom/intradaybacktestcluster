/*
 * MPIHelper.cpp
 *
 *  Created on: Jan 26, 2020
 *      Author: root
 */

#include "MPIHelper.h"
#include "../IntradayMaintenance/CustomExceptionHandler.h"
#include "../DatabaseProcessing/DatabaseProcessing.h"
#include "../IntradayMaintenance/TimeHelper.h"

#include <boost/regex.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <unordered_map>
#include <bits/stdc++.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <utility>

void FreeDataType(MPI_Datatype* pExportData)
{
	MPI_Type_free(pExportData);
}


std::unordered_map<std::string,std::vector<std::string>> GetTickersAsDictionary(const std::string& database, const boost::gregorian::date& start)
{
	double usdtThreshold = 600000;
	double btcThreshold = 500;
	double ethThreshold = 2000;
	double bnbThreshold = 10000;
	auto ipPort = GetLocalNetworkSettings();
	struct stat buffer;
	std::string file("export/preprocessed/tickers" + boost::gregorian::to_iso_string(Today()) +".txt");
	if (stat(file.c_str(), &buffer) == 0)
	{
		std::unordered_map<std::string,std::vector<std::string>> tickersGrouped;
		std::ifstream opn;
		opn.open(file, std::ios::in);
		std::string str;
		while (getline(opn,str))
		{
			std::string s1;
			std::string s2;
			std::istringstream ss(str);
			getline(ss, s1, ',');
			std::vector<std::string> tickers;
			while (getline(ss, s2, ','))
			{
				tickers.push_back(s2);
			}
			tickersGrouped[s1] = tickers;
		}
		return tickersGrouped;
	}
	else
	{
		IntradayBacktest::DatabaseProcessing processor("PAVLO", ipPort.first, ipPort.second);
		const std::vector<std::string>& res = processor.GetTables(database);
		std::unordered_map<std::string, double> tickersWithAvVolume;
		ParallelForeach(res.begin(), res.end(), [&database,&start,&tickersWithAvVolume,&ipPort](const std::string& ticker)
		{

			IntradayBacktest::DatabaseProcessing pr("PAVLO", ipPort.first, ipPort.second);
			std::string query = "SELECT AVG(AvVolume) FROM (SELECT SUM(Volume)*AVG(ClosePrice) As AvVolume FROM " + database +"." + ticker +
					" WHERE Date>='" + boost::gregorian::to_iso_extended_string(start) +"'"+
					" GROUP BY Date) AS S";
			long double vol = pr.GetDoubleValueQuery(query, database)[0];
			std::cout << "Got data about " << ticker << std::endl;
			tickersWithAvVolume[ticker] = static_cast<double>(vol);
		});
		std::unordered_map<std::string,std::vector<std::string>> tickersGrouped;
		tickersGrouped["btc"] = std::vector<std::string>();
		tickersGrouped["eth"] = std::vector<std::string>();
		tickersGrouped["bnb"] = std::vector<std::string>();
		tickersGrouped["usdt"] = std::vector<std::string>();
		tickersGrouped["pax"] = std::vector<std::string>();
		tickersGrouped["usdc"] = std::vector<std::string>();
		tickersGrouped["tusd"] = std::vector<std::string>();
		for (const auto& item : tickersWithAvVolume)
		{
			std::vector<std::string> tickersToExclude = {"npxsbtc","bttbtc","hotbtc","ncashbtc","dentbtc","mftbtc","stormbtc","keybtc",
					"scbtc","tnbbtc","poebtc","funbtc","vetbtc","fuelbtc","lendbtc"};
			if (std::find(tickersToExclude.begin(), tickersToExclude.end(), item.first) != tickersToExclude.end())
			{
				continue;
			}
			std::string currency = GetMainCurrency(item.first);
			if (currency == "btc")
			{
				if (item.second > btcThreshold)
				{
					std::vector<std::string>& vec = tickersGrouped.at("btc");
					vec.push_back(item.first);
				}
			}
			else if (currency == "eth")
			{
				if (item.second > ethThreshold)
				{
					std::vector<std::string>& vec = tickersGrouped.at("eth");
					vec.push_back(item.first);
				}
			}
			else if (currency == "bnb")
			{
				if (item.second > bnbThreshold)
				{
					std::vector<std::string>& vec = tickersGrouped.at("bnb");
					vec.push_back(item.first);
				}

			}
			else if (currency == "usdt")
			{
				if (item.second > usdtThreshold)
				{
					std::vector<std::string>& vec = tickersGrouped.at("usdt");
					vec.push_back(item.first);
				}
			}
			else if (currency == "pax")
			{
				if (item.second > usdtThreshold)
				{
					std::vector<std::string>& vec = tickersGrouped.at("pax");
					vec.push_back(item.first);
				}
			}
			else if (currency == "usdc")
			{
				if (item.second > usdtThreshold)
				{
					std::vector<std::string>& vec = tickersGrouped.at("usdc");
					vec.push_back(item.first);
				}
			}
			else if (currency == "tusd")
			{
				if (item.second > usdtThreshold)
				{
					std::vector<std::string>& vec = tickersGrouped.at("tusd");
					vec.push_back(item.first);
				}
			}
			else if (currency == "xrp")
			{

			}
		}
		std::ofstream wrt;
		std::string dir("export/preprocessed");
		if (mkdir(dir.c_str(), 0777) == -1)
		{
			std::cerr << "Error " << std::strerror(errno);
			std::cerr << std::endl;
		}
		wrt.open("export/preprocessed/tickers" + boost::gregorian::to_iso_string(Today()) +".txt", std::ios::out);
		for (const auto& it: tickersGrouped)
		{
			wrt << it.first << ",";
			for (const auto& item:it.second)
			{
				wrt << item << ",";
			}
			wrt << "\n";
		}
		wrt.close();
		return tickersGrouped;
	}
}

std::vector<TickerPair> GetTickers(const std::string& database, const boost::gregorian::date& start)
{
	struct stat atrib;
	if (stat("export/tickers.txt", &atrib) != 0)
	{
		auto tickersGrouped = GetTickersAsDictionary(database, start);
		std::vector<TickerPair> pairs;
		for (const auto& item: tickersGrouped)
		{
			for (const auto& it: item.second)
			{
				TickerPair pair;
				strcpy(pair.ticker1, it.c_str());
				strcpy(pair.ticker2, it.c_str());
				pairs.push_back(pair);
			}
		}
		std::ofstream wrt;
		wrt.open("export/tickers.txt", std::ios::out);
		for (const auto& it: pairs)
		{
			wrt << it.ticker1 << "," << it.ticker2 << std::endl;
		}
		wrt.close();
		return pairs;
	}
	else
	{
		std::vector<TickerPair> pairs;
		std::ifstream opn;
		opn.open("export/tickers.txt", std::ios::in);
		std::string str;
		while (getline(opn,str))
		{
			std::string s1;
			std::string s2;
			std::istringstream ss(str);
			getline(ss, s1, ',');
			getline(ss, s2);
			TickerPair pair;
			strcpy(pair.ticker1, s1.c_str());
			strcpy(pair.ticker2, s2.c_str());
			pairs.push_back(pair);
		}
		return pairs;
	}

}

std::vector<TickerPair> GetPairs(const std::string& database, const boost::gregorian::date& start, const size_t& take)
{
	auto tickersGrouped = GetTickersAsDictionary(database, start);
	std::vector<TickerPair> pairs;
	for (const auto& item: tickersGrouped)
	{
		for (size_t i = 0; i < item.second.size(); ++i)
		{
			for (size_t j = i + 1; j < item.second.size(); ++j)
			{
				TickerPair pair;
				strcpy(pair.ticker1, item.second[i].c_str());
				strcpy(pair.ticker2, item.second[j].c_str());
				pairs.push_back(pair);
			}
		}
	}
	if (take != 0)
	{
		std::vector<TickerPair> pairsSlice(pairs.begin(), pairs.begin()+take);
		return pairsSlice;
	}
	return pairs;
}

std::vector<TickerPair> GetPairs(const std::string& database, const boost::gregorian::date& start, StrategyEnum strEnum, const size_t& take)
{
	std::vector<TickerPair> pairs;
	switch(strEnum)
	{
		case StatArbitrage:
		{
			pairs = GetPairs(database, start, take);
		}
		break;
		case Momentum:
		{
			pairs = GetPairsDuplicated(database, start);
		}
		break;
		case Ordinary:
		{
			pairs = GetPairsDuplicated(database, start);
		}
	}
	return pairs;
}

std::vector<TickerPair> GetPairsDuplicated(const std::string& database, const boost::gregorian::date& start)
{
	auto tickersGrouped = GetTickersAsDictionary(database, start);
	std::vector<TickerPair> pairs;
	for (const auto& item: tickersGrouped)
	{
		for (size_t i = 0; i < item.second.size(); ++i)
		{
			TickerPair pair;
			strcpy(pair.ticker1, item.second[i].c_str());
			strcpy(pair.ticker2, item.second[i].c_str());
			pairs.push_back(pair);
		}
	}
	return pairs;
}

MPI_Datatype DeclareExportData(const ExportData& myobject)
{
	function = __func__;
	SIGNALEXCEPTION(signalHandler);
	try
	{
		MPI_Datatype MPI_EXPORT_DATA;
		int structlen = 12;
		int blocklengths[structlen];
		MPI_Datatype types[structlen];
		MPI_Aint displacements[structlen];
		blocklengths[0] = 12; types[0] = MPI_CHAR;
		displacements[0] = (size_t)&(myobject.ticker[0]) - (size_t)&myobject;
		blocklengths[1] = 1; types[1] = MPI_INT32_T;
		displacements[1] = (size_t)&(myobject.timeDayOADateEnter) - (size_t)&myobject;
		blocklengths[2] = 1; types[2] = MPI_INT32_T;
		displacements[2] = (size_t)&(myobject.timeMinuteEnter) - (size_t)&myobject;
		blocklengths[3] = 1; types[3] = MPI_INT32_T;
		displacements[3] = (size_t)&(myobject.timeDayOADateClose) - (size_t)&myobject;
		blocklengths[4] = 1; types[4] = MPI_INT32_T;
		displacements[4] = (size_t)&(myobject.timeMinuteClose) - (size_t)&myobject;
		blocklengths[5] = 1; types[5] = MPI_DOUBLE;
		displacements[5] = (size_t)&(myobject.volume) - (size_t)&myobject;
		blocklengths[6] = 1; types[6] = MPI_DOUBLE;
		displacements[6] = (size_t)&(myobject.enterPrice) - (size_t)&myobject;
		blocklengths[7] = 1; types[7] = MPI_DOUBLE;
		displacements[7] = (size_t)&(myobject.closePrice) - (size_t)&myobject;
		blocklengths[8] = 1; types[8] = MPI_INT32_T;
		displacements[8] = (size_t)&(myobject.ma) - (size_t)&myobject;
		blocklengths[9] = 1; types[9] = MPI_INT32_T;
		displacements[9] = (size_t)&(myobject.std) - (size_t)&myobject;
		blocklengths[10] = 1; types[10] = MPI_DOUBLE;
		displacements[10] = (size_t)&(myobject.sigma) - (size_t)&myobject;
		blocklengths[11] = 2; types[11] = MPI_DOUBLE;
		displacements[11] = (size_t)&(myobject.coefficients[0]) - (size_t)&myobject;
		MPI_Type_create_struct(structlen, blocklengths, displacements, types, &MPI_EXPORT_DATA);
		MPI_Type_commit(&MPI_EXPORT_DATA);
		return MPI_EXPORT_DATA;
	}
	catch (std::exception& ex)
	{
		std::cout << "Error during export data MPI declaration" << std::endl;
	}
}


MPI_Datatype DeclareArray(const MPI_Datatype exportDataType, const int& count)
{
	function = __func__;
	SIGNALEXCEPTION(signalHandler);
	try
	{
		MPI_Datatype exportDataVectorType;
		MPI_Type_contiguous(count, exportDataType, &exportDataVectorType);
		MPI_Type_commit(&exportDataVectorType);
		return exportDataVectorType;
	}
	catch (std::exception& ex)
	{
		std::cout << "Error during array MPI declaration" << std::endl;
	}
}

MPI_Datatype DeclareTickerPair(const TickerPair& pair)
{
	function = __func__;
	SIGNALEXCEPTION(signalHandler);
	try
	{
		MPI_Datatype MPI_TICKER_PAIR;
		int structlen =2;
		int blocklengths[structlen];
		MPI_Datatype types[structlen];
		MPI_Aint displacements[structlen];
		blocklengths[0] = 12; types[0] = MPI_CHAR;
		displacements[0] = (size_t)&(pair.ticker1[0]) - (size_t)&pair;
		blocklengths[1] = 12; types[1] = MPI_CHAR;
		displacements[1] = (size_t)&(pair.ticker2[0]) - (size_t)&pair;
		MPI_Type_create_struct(structlen, blocklengths, displacements, types, &MPI_TICKER_PAIR);
		MPI_Type_commit(&MPI_TICKER_PAIR);
		return MPI_TICKER_PAIR;
	}
	catch (std::exception& ex)
	{
		std::cout << "Error during ticker pair MPI declaration" << std::endl;
	}
}

std::string GetMainCurrency(const std::string& ticker)
{
	std::string tick = ticker;
	std::vector<std::string> keyWords = {"btc","usdt","eth","bnb","pax","usdc","xrp","tusd"};
	for (const auto& keyWord : keyWords)
	{
		boost::smatch result;
		std::string regPattern = "(\\D+)(" + keyWord + ")";
		boost::regex regEx(regPattern);
		if (boost::regex_match(tick, result, regEx))
		{
			return keyWord;
		}
	}
	return "";
}

void AnalyzeResults(const std::vector<IntradayBacktest::AnalyticsHelper*>& vec)
{
	if (vec.size() > 0)
	{
		std::string dir = vec[0]->GetDirectory();
		std::string file = dir +"/" + "analytics.csv";
		std::ofstream fil(file);
		fil << "Ticker,ProfitFactor,SharpeRatio,AverageReturn,AverageProfit,AverageLoss,SuccessfulDeals,LastDealEnterDay,"
				"LastDealEnterMinute\n";
		for (const auto& item: vec)
		{
			fil << item->GetTicker() << "," << std::to_string(item->GetProfitFactor()) << "," <<
					std::to_string(item->GetSharpeRatio()) << "," << std::to_string(item->GetAverageReturn())
					<< "," << std::to_string(item->GetAverageProfit()) << ","
					<< std::to_string(item->GetAverageLoss()) << ","
					<< std::to_string(item->GetSuccessfulDealsPercent()) << ","
					<< std::to_string(item->GetLastDealDay()) << ","
					<<	std::to_string(item->GetLastDealMinute()) << std::endl;
		}
		fil.close();
	}

}
