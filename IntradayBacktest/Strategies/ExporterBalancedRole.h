/*
 * ExporterBalancedRole.h
 *
 *  Created on: Jan 26, 2020
 *      Author: root
 */

#ifndef EXPORTERBALANCEDROLE_H_
#define EXPORTERBALANCEDROLE_H_

#pragma once

#include "../IntradayMaintenance/Timeframe.h"
#include "../DatabaseProcessing/Filters/AbstractFilter.h"
#include "../DatabaseProcessing/Filters/TradingRestrictions.h"
#include "../Exporter/IntradayExporter.h"
#include "../Exporter/AnalyticsHelper.h"
#include "../IntradayMaintenance/ExportData.h"
#include "../IntradayMaintenance/TimeHelper.h"
#include "StatArb.h"
#include "MomentumStrategy.h"
#include "OrdinaryStrategy.h"
#include "StrategyEnum.h"
#include "MPIHelper.h"

#include <vector>
#include <queue>
#include <utility>
#include <string>
#include <ctime>
#include <thread>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <mpi.h>

// rank 0
void MPIExporterBalancedRole(const std::string& database, const boost::gregorian::date& start,
				MPI_Datatype tickerType, MPI_Datatype exportType, const int& numOfProcesses, std::vector<std::vector<ExportData>>& exportedData, const bool& live = false);



#endif /* EXPORTERBALANCEDROLE_H_ */
