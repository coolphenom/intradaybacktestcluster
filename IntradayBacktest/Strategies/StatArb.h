/*
 * StatArb.h
 *
 *  Created on: Mar 14, 2019
 *      Author: pavlotkachenko
 */

#ifndef STATARB_H_
#define STATARB_H_

#include "AbstractStrategy.h"
#include "../DatabaseProcessing/Filters/AbstractFilter.h"
#include "../DatabaseProcessing/DatabaseAnalysis.h"
#include "../Trading/TradingProxy.h"
#include "../IntradayMaintenance/CustomExceptionHandler.h"

#include "StatArb.h"
#include "../DatabaseProcessing/Filters/Signal.h"
#include "../DatabaseProcessing/Filters/BollingerBands.h"
#include "../DatabaseProcessing/Filters/DonchianChannel.h"

#include <csignal>
#include <exception>
#include <boost/regex.hpp>
#include <sstream>
#include <algorithm>
#include <vector>
#include <tuple>
#include <string>
#include <utility>
#include <unordered_map>

template <typename T>
class StatArb: public IntradayBacktest::AbstractStrategy<T> {
public:
	template <typename ... Types>
	StatArb(const IntradayBacktest::DatabaseAnalysis& trader, const std::vector<std::string>& tickers, const std::vector<double>& coefficients, const std::vector<std::vector<std::tuple<int,int,double>>>& info,
			const std::vector<std::pair<int,int>>& timePeriods, const AbstractFilterEnum& filter, const std::vector<AbstractFilter*> secondaryFilters, const std::tuple<Types...>& args, const std::unordered_map<int,double>& values,
			const double& stopLoss = 0, const double& takeProfit = 0) :
			IntradayBacktest::AbstractStrategy<T>(0.001, stopLoss, takeProfit),
			m_tickers{tickers}
	{
		m_coefficients.push_back(coefficients);
		switch (filter)
		{
		case AbstractFilterEnum::BollingerBandsFilter:
		{
			this->abstractFilter = new BollingerBands(args);
		}
		break;
		case AbstractFilterEnum::DonchianChannelFilter:
		{
			this->abstractFilter = new DonchianChannel(args);
		}
		break;
		default:
			break;
		}
		m_mainDailyCurrencyQuotes = values;
		this->GetMainCurrency("");
		Trade(trader, info, timePeriods, 0);
	}

	/*const std::vector<std::tuple<std::string, int, int, int, int, double, double, double>>& DealsList()
	{
		return m_tradingProxy->DealsList();
	}*/

/*	void SetMainCurrency(const std::unordered_map<int,double>& values)
	{
		m_mainDailyCurrencyQuotes = values;
	}*/

	/*double ProfitFactor() const
	{
		return m_tradingProxy->GetProfitFactor();
	}*/

	void Trade(const IntradayBacktest::DatabaseAnalysis& trader, const std::vector<std::vector<std::tuple<int,int,T>>>& info, const std::vector<std::pair<int,int>>& periods, const double& spread) override
	{
		function = __func__;
		SIGNALEXCEPTION(signalHandler);
		std::map<std::string, std::tuple<int, int, double, double>> sharesStored;
		for (size_t k = 0; k < periods.size(); ++k)
		{
			try{
				signal(SIGSEGV,signalHandler);
				size_t st = periods[k].first;
				size_t fin = periods[k].second;
				if (fin > trader.GetValues().size())
				{
					fin = trader.GetValues().size() - 1;
				}
				size_t day = 0;
				bool filter = false;
				Signal signal = Signal::NoSignal;
				for (size_t i = st; i < fin; ++i)
				{
					const std::vector<double>& coefficients = GetCoefficients(i);
					if (sharesStored.size() == 0)
					{
						if (CheckSecondaryFilter(i))
						{
							signal = this->abstractFilter->GetEntrySignal(i, trader, spread);
							if (signal == Signal::LongBuy || signal == Signal::ShortSell)
							{
								int day = std::get<0>(trader.GetValues()[i]);
								double money = CalculateMoneyEquivalent(day);
								switch (signal)
								{
									case Signal::LongBuy:
									{
										this->m_tradingProxy->BuySpread(m_tickers, money, coefficients, i, sharesStored, info, true);
									}
									break;
									case Signal::ShortSell:
									{
										this->m_tradingProxy->SellSpread(m_tickers, money, coefficients, i, sharesStored, info, false);
									}
									break;
									default:
										break;
								}
							}
						}
					}
					else
					{
						if (this->abstractFilter->GetCloseSignal(i, signal, trader) != Signal::NoSignal)
						{
							int day = std::get<0>(trader.GetValues()[i]);
							double money = CalculateMoneyEquivalent(day);
							signal = this->abstractFilter->GetCloseSignal(i, signal, trader);
							switch (signal)
							{
								case Signal::LongSell:
								{
									this->m_tradingProxy->SellSpread(m_tickers, money, coefficients, i, sharesStored, info, true);
								}
								break;
								case Signal::ShortBuy:
								{
									this->m_tradingProxy->BuySpread(m_tickers, money, coefficients, i, sharesStored, info, false);
								}
								break;
								default:
									break;
							}
						}
					}
				}
			}
			catch (const std::exception& ex){
				std::cout << "Exception while trading" << std::endl;
				std::cout << ex.what() << std::endl;
			}
			if (sharesStored.size() > 0)
				sharesStored.clear();
		}
	}

	double CalculateMoneyEquivalent(const int& day)
	{
		if (m_mainCurrency != "")
		{
			return 10000/m_mainDailyCurrencyQuotes[day];
		}
		else
		{
			return 10000;
		}
	}

	virtual void GetMainCurrency(const std::string& tick) override
	{
		std::vector<std::string> keyWords = {"btc","usdt","eth","bnb"};
		std::string mainCur;
		for (const auto& ticker : m_tickers)
		{
			std::string temp;
			for (const auto& keyWord : keyWords)
			{
				boost::smatch result;
				std::string regPattern = "(\\D+)(" + keyWord + ")";
				boost::regex regEx(regPattern);
				if (boost::regex_match(ticker, result, regEx))
				{
					temp = keyWord;
					break;
				}
			}
			if (mainCur.empty())
			{
				mainCur = temp;
			}
			else
			{
				if (mainCur != temp)
				{
					m_mainCurrency = "";
					return;
				}
			}
			m_mainCurrency = mainCur;
		}
	}

	bool CheckSecondaryFilter(const int& time)
	{
		bool result = true;
		std::for_each(this->secondaryFilters.begin(), this->secondaryFilters.end(),[&result,&time](AbstractFilter* filter) {result *= filter->IsEnabled(time);});
		return result;
	}

	const std::vector<double>& GetCoefficients(const int& index)
	{
		if (m_coefficients.size() > 1)
		{
			return m_coefficients[index];
		}
		else
		{
			return m_coefficients[0];
		}
	}

	~StatArb() {
		delete this->abstractFilter;
	}
	virtual std::string ToJSON() override
	{
		std::string json;
		json = json + "{\"" + "name" + "\"" + ":" +"\"" + "StatArb" + "\"" + ",";
		json = json + "\"" + "tickers" + "\"" + ":" +"[";
		for (const auto& item: m_tickers)
			json = json +"\""+item +"\"" + ",";
		json.pop_back();
		json = json + "]" + ",";
		json = json + "\"" + "coeffs" + "\"" + ":" +"[";
		for (const auto& item: m_coefficients)
		{
			json = json + "[";
			for (const auto& it : item)
				json = json + "\"" +std::to_string(it) + "\"" + ",";
			json.pop_back();
			json = json + "],";
		}
		json.pop_back();
		json = json +"],";
		json = json + "\"" + "filter" + "\"" + ":"  + this->abstractFilter->ToJSON();
		json = json + "}";
		return json;
	}
private:
	std::vector<std::vector<double>> m_coefficients;
	std::vector<std::string> m_tickers;
	std::string m_mainCurrency;
	std::unordered_map<int,double> m_mainDailyCurrencyQuotes;
};

#endif /* STATARB_H_ */
