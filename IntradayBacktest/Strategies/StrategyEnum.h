/*
 * StrategyEnum.h
 *
 *  Created on: Nov 10, 2019
 *      Author: root
 */

#ifndef STRATEGYENUM_H_
#define STRATEGYENUM_H_


enum StrategyEnum
{
	StatArbitrage,
	Momentum,
	Ordinary
};


#endif /* STRATEGYENUM_H_ */
