/*
 * MomentumStrategy.cpp
 *
 *  Created on: Jul 17, 2019
 *      Author: root
 */

#include "../IntradayMaintenance/CustomExceptionHandler.h"
#include "../DatabaseProcessing/Filters/PumpScanner.h"
#include "../IntradayMaintenance/StatisticsHelper.h"
#include "../DatabaseProcessing/Filters/Indicators/SMA.h"
#include "../DatabaseProcessing/Filters/Indicators/RSI.h"
#include "MomentumStrategy.h"

#include <csignal>
#include <exception>


MomentumStrategy::MomentumStrategy(const IntradayBacktest::DatabaseAnalysis& trader,const std::string& ticker,  const std::vector<std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>>& info,
		const std::vector<std::pair<int,int>>& timePeriods, const AbstractFilterEnum& filter,const std::vector<AbstractFilter*> secondaryFilters, const std::unordered_map<int,double>& values,
		const TradingRestrictions& restr, const int& candles, const StrategyMode& mode,
		const double& stopLoss, const double& takeProfit) : AbstractStrategy(0.001, stopLoss, takeProfit)
{
	//StatisticsHelper h(trader, 20);
	//auto zScores = h.GetZScoreBollingerTest();
	//auto std = h.GetStd();
	//SMA* vol = new SMA(trader, 20);
	//vol->Compare(zScores, "zScore");
	//vol->Compare(std, "Std");
	//vol->Compare(info[0]);
	std::vector<AbstractIndicator*> inds;
	RSI* rsi = new RSI(trader, 9, info[0]);
	inds.push_back(rsi);
	switch (filter)
	{
		case AbstractFilterEnum::PumpScannerFilter:
		{
			this->abstractFilter = new PumpScanner(inds, IntradayBacktest::IntradayMaintenance::StockInfoFieldsEnum::ClosePrice,
					2, candles, 1, restr, mode);
		}
		break;
		default:
		break;
	}
	m_tickers = {ticker};
	m_mainDailyCurrencyQuotes = values;
	this->GetMainCurrency(ticker);
	if (ticker == "powrbnb")
	{
		std::cout << "here" << std::endl;
		std::cout << "here" << std::endl;
		m_tickers = {ticker};
	}
	Trade(trader, info, timePeriods, 0);
}

void MomentumStrategy::Trade(const IntradayBacktest::DatabaseAnalysis& trader, const std::vector<std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>>& info, const std::vector<std::pair<int,int>>& periods, const double& spread)
{
	//if (trader.GetCandles().size() > 0)
	if (this->m_mainCurrency == "bnb")
	{
		std::cout << "Hello" << std::endl;
	}
	size_t ii = 0;
	size_t jj = 0;
	if (info.size() > 0)
	{
		function = __func__;
		SIGNALEXCEPTION(signalHandler);
		std::map<std::string, std::tuple<int, int, double, double>> sharesStored;
		for (size_t k = 0; k < periods.size(); ++k)
		{
			try
			{
				size_t st = periods[k].first;
				size_t fin = periods[k].second;
				size_t day = 0;
				Signal signal = Signal::NoSignal;
				auto shortCandles = info[0];
				auto longCandles = info[1];
				size_t longCandlesSize = longCandles.size();
				size_t shortCandlesSize = shortCandles.size();
				if (longCandlesSize > 0 && shortCandlesSize > 0)
				{
					auto longCandlesIt = longCandles.begin();
					auto longCandlesEnd = longCandles.end();
					int currentDay = 0;
					int currentMin = 0;
					size_t j = 0;
					for(size_t i = st; i < fin; ++i)
					{
						Start:
						ii = i;
						if (sharesStored.size() == 0)
						{
							while (std::get<0>(shortCandles[i]) < currentDay ||
									(std::get<0>(shortCandles[i]) == currentDay && std::get<1>(shortCandles[i]) <= currentMin) && i < fin)
							{
								++i;
								if (i >= fin)
									break;
							}
							if (i >= fin)
								break;
							signal = this->abstractFilter->GetEntrySignal(i, trader, 0);
							if (signal == Signal::LongBuy || signal == Signal::ShortSell)
							{
								int day = std::get<0>(shortCandles[i]);
								currentDay = day;
								currentMin = std::get<1>(shortCandles[i]);
								double money = CalculateMoneyEquivalent(day);
								double units =  money/std::get<2>(shortCandles[i]).GetClose();
								switch (signal)
								{
									case Signal::LongBuy:
									{
										m_tradingProxy->Buy(this->GetTickers()[0], units, i, sharesStored, true, shortCandles);
									}
									break;
									case Signal::ShortSell:
									{
										m_tradingProxy->Sell(this->GetTickers()[0], units, i, sharesStored, false, shortCandles);
									}
									break;
									default:
									break;
								}
							}
						}
						else
						{
							if (longCandlesIt != longCandles.end())
							{
								while (std::get<0>(longCandles[j]) < currentDay || (std::get<0>(longCandles[j]) == currentDay && std::get<1>(longCandles[j]) <= currentMin) && j < longCandlesSize)
								{
									++j;
									jj = j;
									++longCandlesIt;
									if (longCandlesIt == longCandlesEnd)
										break;
								}
								if (longCandlesIt == longCandlesEnd)
									break;
								int kk = i;
								while (longCandlesIt != longCandles.end())
								{
									if (this->m_tradingProxy->StopLossOn())
									{
										//Check for TakeProfit/StopLoss here
										while (std::get<1>(shortCandles[kk]) <= std::get<1>(longCandles[j]))
										{
											this->m_tradingProxy->StopLoss(kk, 1, this->GetTickers()[0], sharesStored, shortCandles);
											if (sharesStored.empty())
											{
												this->abstractFilter->ModifyFilter();
												i = kk;
												goto Start;
											}
											else
											{
												++kk;
												if (kk >= shortCandles.size())
													return;
											}

										}
									}

									Signal closeSignal = this->abstractFilter->GetCloseSignal(j, signal, trader);
									if (closeSignal != Signal::NoSignal)
									{
										currentDay = std::get<0>(longCandles[j]);
										currentMin = std::get<1>(longCandles[j]);
										double money = CalculateMoneyEquivalent(currentDay);
										double units =  money/std::get<2>(longCandles[j]).GetClose();
										switch (closeSignal)
										{
											case Signal::LongSell:
											{
												this->m_tradingProxy->Sell(this->GetTickers()[0], units, j, sharesStored, true, longCandles);
											}
											break;
											case Signal::ShortBuy:
											{
												this->m_tradingProxy->Buy(this->GetTickers()[0], -units, j, sharesStored, false, longCandles);
											}
											break;
										}
										break;
									}
									if (this->abstractFilter->Modified())
										this->abstractFilter->ModifyFilter();
									++longCandlesIt;
									++j;
									jj = j;
								}
							}

						}
					}
				}

			}
			catch (std::exception& ex)
			{
				std::cout << "Exception while trading" << std::endl;
				std::cout << ex.what() << std::endl;
			}

		}
	}
}

MomentumStrategy::~MomentumStrategy() {
	// TODO Auto-generated destructor stub
	delete this->abstractFilter;
}

std::string MomentumStrategy::ToJSON()
{
	std::string json;
	json = json + "{\"" + "name" + "\"" + ":" +"\"" + "Momentum" + "\"" + ",";
	json = json + "\"" + "tickers" + "\"" + ":" +"[";
	for (const auto& item: m_tickers)
		json = json + "\"" + item + "\"" + ",";
	json.pop_back();
	json = json + "]" + ",";
	json = json + "\"" + "filter" + "\"" + ":"  + this->abstractFilter->ToJSON();
	json = json + "}";
	return json;
}

