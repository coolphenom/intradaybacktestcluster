/*
 * AbstractStrategy.h
 *
 *  Created on: Mar 14, 2019
 *      Author: pavlotkachenko
 */

#ifndef ABSTRACTSTRATEGY_H_
#define ABSTRACTSTRATEGY_H_

#include "../DatabaseProcessing/Filters/AbstractFilter.h"
#include "../Trading/TradingProxy.h"
#include "../IntradayMaintenance/StockInfo.h"

#include <vector>
#include <tuple>
#include <string>
#include <unordered_map>
#include <boost/regex.hpp>

namespace IntradayBacktest{

template <typename T>
class AbstractStrategy {
public:
	AbstractStrategy(const double& fee, const double& stopLoss, const double& takeProfit);
	virtual ~AbstractStrategy();
	const std::vector<std::tuple<std::string, int, int, int, int, double, double, double>>& DealsList() const;
	double ProfitFactor() const;
	std::vector<double> ProfitFactors() const;
	virtual void Trade(const IntradayBacktest::DatabaseAnalysis& trader, const std::vector<std::vector<std::tuple<int,int,T>>>& info, const std::vector<std::pair<int,int>>& periods, const double& spread) = 0;
	virtual std::string ToJSON() = 0;
protected:
	double CalculateMoneyEquivalent(const int& day);
	virtual void GetMainCurrency(const std::string& ticker);
	bool CheckSecondaryFilter(const int& time);
	const std::vector<std::string>& GetTickers() const;
protected:
	AbstractFilter* abstractFilter;
	std::vector<AbstractFilter*> secondaryFilters;
	IntradayBacktest::TradingProxy* m_tradingProxy;
	std::string m_mainCurrency;
	std::unordered_map<int,double> m_mainDailyCurrencyQuotes;
	std::vector<std::string> m_tickers;
};

}


#endif /* ABSTRACTSTRATEGY_H_ */
