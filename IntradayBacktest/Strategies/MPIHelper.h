/*
 * MPIHelper.h
 *
 *  Created on: Jan 26, 2020
 *      Author: root
 */

#ifndef MPIHELPER_H_
#define MPIHELPER_H_

#include "../Exporter/AnalyticsHelper.h"
#include "StrategyEnum.h"

#include <thread>
#include <string>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <mpi.h>
#include <boost/date_time/gregorian/gregorian.hpp>

#define FUNCTION(func) #func
#define STRINGIZE(arg) #arg

struct TickerPair
{
	char ticker1[12];
	char ticker2[12];
	TickerPair() {}
	TickerPair(const std::string& tick1, const std::string& tick2)
	{
		strcpy(ticker1, tick1.c_str());
		strcpy(ticker2, tick2.c_str());
	}
};

std::vector<TickerPair> GetPairsDuplicated(const std::string& database, const boost::gregorian::date& start);
std::vector<TickerPair> GetTickers(const std::string& database, const boost::gregorian::date& start);
std::vector<TickerPair> GetPairs(const std::string& database, const boost::gregorian::date& start, const size_t& take = 0);
std::vector<TickerPair> GetPairs(const std::string& database, const boost::gregorian::date& start, StrategyEnum strEnum, const size_t& take = 0);
void FreeDataType(MPI_Datatype* pExportData);
MPI_Datatype DeclareExportData(const ExportData& myobject);
MPI_Datatype DeclareArray(const MPI_Datatype exportDataType, const int& count);
MPI_Datatype DeclareTickerPair(const TickerPair& pair);
std::string GetMainCurrency(const std::string& ticker);
void AnalyzeResults(const std::vector<IntradayBacktest::AnalyticsHelper*>& vec);

template <typename Iterator, typename Functor>
void ParallelForeach(Iterator first, Iterator last, Functor func)
{
	const size_t nthreads = std::thread::hardware_concurrency();
	std::vector<std::thread> threads(nthreads);
	for(int t = 0;t < nthreads; ++t)
	    {
	      threads[t] = std::thread(std::bind(
	        [&](Iterator f, Iterator l)
	        {
	    	  while(f < l)
			  {
	    		  func(*f);
	    		  f += nthreads;
			  }
	        },first + t % nthreads,last));
	    }
	    std::for_each(threads.begin(),threads.end(),[](std::thread& x){x.join();});
}

#endif /* MPIHELPER_H_ */
