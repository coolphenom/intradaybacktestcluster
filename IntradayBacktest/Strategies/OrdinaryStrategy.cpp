/*
 * OrdinaryStrategy.cpp
 *
 *  Created on: Nov 16, 2019
 *      Author: root
 */
#include "../IntradayMaintenance/CustomExceptionHandler.h"
#include "../DatabaseProcessing/Filters/Indicators/EMA.h"
#include "../DatabaseProcessing/Filters/Indicators/SSMA.h"
#include "../DatabaseProcessing/Filters/Indicators/RSI.h"
#include "../DatabaseProcessing/Filters/VolatilityPercentile.h"
#include "../DatabaseProcessing/Filters/MarketModeChange.h"
#include "../DatabaseProcessing/Filters/TripleMarketModeFilter.h"
#include "../src/LogDuration.h"
#include "OrdinaryStrategy.h"

#include <csignal>
#include <exception>

OrdinaryStrategy::OrdinaryStrategy(const IntradayBacktest::DatabaseAnalysis& trader,const std::string& ticker,  const std::vector<std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>>& info,
		const std::pair<IntradayBacktest::IntradayMaintenance::Timeframe, IntradayBacktest::IntradayMaintenance::Timeframe>& framesUsed,
		const std::vector<std::pair<int,int>>& timePeriods, const AbstractFilterEnum& filter,const std::vector<AbstractFilter*> secondaryFilters, const std::unordered_map<int,double>& values,
		const double& stopLoss, const double& takeProfit) : AbstractStrategy(0.001, stopLoss, takeProfit)
{
	std::vector<AbstractIndicator*> vec;
	//SSMA* ind1 = new SSMA(trader,10, info[0]);
	//EMA* ind2 = new EMA(trader,30, info[0]);
	EMA* ind1 = new EMA(trader,7,info[0]);
	RSI* ind2 = new RSI(trader, 8, info[0]);
	VolatilityPercentile* ind3 = new VolatilityPercentile(25, trader);
	std::unordered_map<size_t,size_t> timeframeMapper; //= GetTimeframeMapper(framesUsed.first, framesUsed.second, info[0], info[1]);
	m_mainDailyCurrencyQuotes = values;
	//RSI* indRSISenior = new RSI(trader, 8, info[1]);
	vec.push_back(ind1);
	vec.push_back(ind2);
	vec.push_back(ind3);
	//vec.push_back(indRSISenior);
	//ind3->Compare(info[0]);
	switch (filter)
	{
		case AbstractFilterEnum::MarketModeFilter:
		{
			this->abstractFilter = new MarketModeChange(vec, timeframeMapper, StrategyMode::MeanReversal);
		}
		break;
		case AbstractFilterEnum::TripleMarketModeFilt:
		{
			this->abstractFilter = new TripleMarketModeFilter(vec, StrategyMode::MeanReversal);
		}
		break;
		default:
			throw std::runtime_error("filter is incompatible with this strategy");
	}
	m_tickers = {ticker};
	this->GetMainCurrency(ticker);
	Trade(trader, info, timePeriods, 0);
}

int OrdinaryStrategy::MaxDailyQuotes()
{
	int day = 0;
	for (const auto& item: m_mainDailyCurrencyQuotes)
	{
		if (item.first > day)
			day = item.first;
	}
	return day;
}

int OrdinaryStrategy::MaxQuotes(const std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>& quotes)
{
	int day = 0;
	for (const auto& item: quotes)
	{
		if (std::get<0>(item) > day)
			day = std::get<0>(item);
	}
	return day;
}

//TODO Rework trading
void OrdinaryStrategy::Trade(const IntradayBacktest::DatabaseAnalysis& trader, const std::vector<std::vector<std::tuple<int,int,IntradayBacktest::IntradayMaintenance::StockInfo>>>& info,
		const std::vector<std::pair<int,int>>& periods, const double& spread)
{
	if (info.size() > 0)
	{
		function = __func__;
		SIGNALEXCEPTION(signalHandler);
		std::map<std::string, std::tuple<int, int, double, double>> sharesStored;
		for (size_t k = 0; k < periods.size(); ++k)
		{
			try
			{
				size_t start = periods[k].first;
				size_t finish = periods[k].second;
				Signal signal = Signal::NoSignal;
				for (size_t i = start; i < finish; ++i)
				{
					Continue:
					if (sharesStored.size() == 0)
					{
						if (this->abstractFilter->GetEntrySignal(i) != Signal::NoSignal)
						{
							signal = this->abstractFilter->GetEntrySignal(i);
							int day = std::get<0>(info[0][i]);
							double money = CalculateMoneyEquivalent(day);
							double units =  money/std::get<2>(info[0][i]).GetClose();
							if (units == std::numeric_limits<double>::infinity())
							{
								std::cout << "Infinity" << std::endl;
								CalculateMoneyEquivalent(day);
							}
							switch (signal)
							{
								case Signal::LongBuy:
								{
									m_tradingProxy->Buy(this->GetTickers()[0], units, i, sharesStored, true, info[0]);
								}
								break;
								case Signal::ShortSell:
								{
									m_tradingProxy->Sell(this->GetTickers()[0], units, i, sharesStored, false, info[0]);
								}
								break;
								default:
								break;
							}
						}
					}
					else
					{
						if (this->abstractFilter->GetCloseSignal(i, signal) != Signal::NoSignal)
						{
							signal = this->abstractFilter->GetCloseSignal(i, signal);
							int day = std::get<0>(info[0][i]);
							double money = CalculateMoneyEquivalent(day);
							double units =  money/std::get<2>(info[0][i]).GetClose();
							switch (signal)
							{
								case Signal::LongSell:
								{
									this->m_tradingProxy->Sell(this->GetTickers()[0], units, i, sharesStored, true, info[0]);
								}
								break;
								case Signal::ShortBuy:
								{
									this->m_tradingProxy->Buy(this->GetTickers()[0], units, i, sharesStored, false, info[0]);
								}
								break;
								default:
								break;
							}
							if (i == finish)
								break;
							goto Continue;
						}
					}
				}
			}
			catch (std::exception& ex)
			{
				std::cout << "Exception while trading" << std::endl;
				std::cout << ex.what() << std::endl;
			}

		}
	}
}

std::string OrdinaryStrategy::ToJSON()
{
	std::string json;
	json = json + "{\"" + "name" + "\"" + ":" +"\"" + "OrdinaryStrategy" + "\"" + ",";
	json = json + "\"" + "tickers" + "\"" + ":" +"[";
	for (const auto& item: m_tickers)
		json = json + "\"" + item + "\"" + ",";
	json.pop_back();
	json = json + "]" + ",";
	json = json + "\"" + "filter" + "\"" + ":"  + this->abstractFilter->ToJSON();
	json = json + "}";
	return json;
}

OrdinaryStrategy::~OrdinaryStrategy() {
	// TODO Auto-generated destructor stub
	delete this->abstractFilter;
}

