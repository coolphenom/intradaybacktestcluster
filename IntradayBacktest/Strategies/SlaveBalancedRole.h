/*
 * SlaveBalancedRole.h
 *
 *  Created on: Jan 26, 2020
 *      Author: root
 */

#ifndef SLAVEBALANCEDROLE_H_
#define SLAVEBALANCEDROLE_H_

#pragma once

#include "../IntradayMaintenance/Timeframe.h"
#include "../DatabaseProcessing/Filters/AbstractFilter.h"
#include "../DatabaseProcessing/Filters/TradingRestrictions.h"
#include "../Exporter/IntradayExporter.h"
#include "../Exporter/AnalyticsHelper.h"
#include "../IntradayMaintenance/ExportData.h"
#include "../IntradayMaintenance/TimeHelper.h"
#include "../HttpRequestHelper/StrategyRequestHelper.h"
#include "StatArb.h"
#include "MomentumStrategy.h"
#include "OrdinaryStrategy.h"
#include "StrategyEnum.h"
#include "MPIHelper.h"

#include <vector>
#include <queue>
#include <utility>
#include <string>
#include <ctime>
#include <thread>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <mpi.h>

class StrategyLauncher;

template <typename... Types1, typename... Types2>
std::tuple<std::vector<ExportData>,std::string> GetExportData(const StrategyLauncher* inst, std::tuple<std::vector<ExportData>,std::string> (StrategyLauncher::*strategy)(std::pair<std::string,std::string>, Types1...), std::pair<std::string,std::string> pair, Types2&&... types)
{
	return (inst->*strategy)(pair, std::forward<Types2>(types)...);
}

template <typename... Types1, typename... Types2>
void MPISlaveBalancedRole(const StrategyLauncher* inst, const int& myRank, MPI_Datatype tickerType, MPI_Datatype exportType,
		const int& numberOfProcesses, StrategyEnum strEnum,
		std::tuple<std::vector<ExportData>,std::string> (StrategyLauncher::*strategy)(std::pair<std::string,std::string>, Types1...), Types2&&... types)
{
	function = "mpi_sender_loop_" + std::to_string(myRank);
	SIGNALEXCEPTION(signalHandler);
	MPI_Status status;
	int tag=0;
	std::cout << "Hello from the process number " << myRank << std::endl;
	size_t pNumberReceive = 0;
	//TickerPair* pTickerPairs = nullptr;
	//MPI_Recv(&tickerPairs, pNumberReceive, tickerType, 0, tag, MPI_COMM_WORLD, &status);
	MPI_Barrier(MPI_COMM_WORLD);
	size_t pairs;
	MPI_Recv(&pairs, 1, MPI_UINT32_T, 1, tag, MPI_COMM_WORLD, &status);
	std::cout << "Slave " << myRank << " is ready to work" << std::endl;
	MPI_Barrier(MPI_COMM_WORLD);
	usleep(myRank*1000000);
	while (pairs > 0)
	{
		FinalRequest:
		TickerPair item;
		MPI_Send(&myRank, 1, MPI_UINT32_T, 1, tag, MPI_COMM_WORLD);
		std::cout << "Slave " << myRank << " sent request to dispatcher" << std::endl;
		MPI_Recv(&item, 1, tickerType, 1, tag, MPI_COMM_WORLD, &status);
		std::cout << "Slave " << myRank << " got his job" << std::endl;
		MPI_Recv(&pairs, 1, MPI_UINT32_T, 1, tag, MPI_COMM_WORLD, &status);
		std::cout << "Slave " << myRank << " started doing his job" << std::endl;
		if (std::string(item.ticker1)=="test")
		{
			break;
		}
		else if (pairs == 0)
		{
			std::cout << "This is me, that strange case" << std::endl;
			//break;
		}
		ExportData* pMyData;
		//ExportData* pMyData2;
		/*if (&item != nullptr)
		{*/
		std::cout << "Exporting test :" << std::string(item.ticker1) << " " << std::string(item.ticker2) << std::endl;
		auto dat = GetExportData(inst, strategy, std::make_pair((item).ticker1, (item).ticker2), types...);
		Retry:
		try
		{
			size_t sz = std::get<0>(dat).size();
			//size_t size1 = sz % 2 == 0 ? sz/2 : sz/2 + 1;
			//size_t size2 = sz/2;
			auto resultsVec = std::get<0>(dat);
			std::vector<ExportData> resultHalf(resultsVec.begin(), resultsVec.begin()+sz);
			//std::vector<ExportData> resultHalf2(resultsVec.begin()+size1 + 1, resultsVec.end());
			std::cout << "Got some results.." << std::endl;
			pMyData = resultHalf.data();
			//pMyData2 = resultHalf2.data();
			std::string json = std::get<1>(dat);
			if (json.size() > 0)//live
			{
				Strategy strategy(strEnum, "id", json);
				StrategyRequestHelper h;
				std::cout << strategy.ToJSON() << std::endl;
				h.PostRequest(strategy.ToJSON());
			}
			MPI_Datatype CONTIGUOUS_TYPE1 = DeclareArray(exportType, resultHalf.size());
			//MPI_Datatype CONTIGUOUS_TYPE2 = DeclareArray(exportType, resultHalf2.size());
			if (std::get<0>(dat).size() < 0)
				throw std::runtime_error("wrong size!");
			std::cout << "Sending " << resultHalf.size() << " deals from process " << myRank << " to process #0" << std::endl;
			MPI_Send(pMyData, 1, CONTIGUOUS_TYPE1, 0, tag, MPI_COMM_WORLD);
			std::cout << "Sent data from process " << myRank<< std::endl;
			std::cout << "Exported "<< resultHalf.size() << " of" << (item).ticker1 << "from slave "<< myRank
					<< std::endl;
			FreeDataType(&CONTIGUOUS_TYPE1);
			if (pairs == 0)
			{
				goto FinalRequest;
			}
		}
		catch (std::exception& ex)
		{
			std::cout << "MPI exception while sending.." << std::endl;
			std::cout << ex.what() << std::endl;
			goto Retry;
		}
		/*}
		else
		{
			pMyData = nullptr;
			char * pJson = nullptr;
			MPI_Datatype CONTIGUOUS_TYPE = DeclareArray(exportType, 0);
			MPI_Send(pMyData, 1, CONTIGUOUS_TYPE, 0, tag, MPI_COMM_WORLD);
			MPI_Send(pJson, 1, MPI_CHAR, 0, tag, MPI_COMM_WORLD);
		}*/

	}
	std::cout << "Process " << myRank << " is done " << std::endl;
	MPI_Barrier(MPI_COMM_WORLD);
}

template <typename... Types1, typename... Types2>
void MPISlaveRole(const int& myRank, MPI_Datatype tickerType, MPI_Datatype exportType,
		const int& numberOfProcesses,
		std::tuple<std::vector<ExportData>,std::string> (StrategyLauncher::*strategy)(std::pair<std::string,std::string>, Types1...), Types2&&... types)
{
	function = "mpi_sender_loop";
	SIGNALEXCEPTION(signalHandler);
	MPI_Status status;
	int tag=0;
	std::cout << "Hello from the process number " << myRank << std::endl;
	size_t pNumberReceive = 0;
	//TickerPair* pTickerPairs = nullptr;
	std::cout << "Process " << myRank << " receiving from process #0..." << std::endl;
	MPI_Recv(&pNumberReceive, 1, MPI_UINT32_T, 0, tag, MPI_COMM_WORLD, &status);
	TickerPair tickerPairs[pNumberReceive+1];
	std::cout << "Process " << myRank << " received first package from process #0..." << std::endl;
	std::cout << "Process " << myRank << ":There will be " << pNumberReceive << " pairs" << std::endl;
	MPI_Recv(&tickerPairs, pNumberReceive, tickerType, 0, tag, MPI_COMM_WORLD, &status);
	std::cout << "Received from process #1 " << pNumberReceive << " pairs" << std::endl;

	std::vector<size_t> pairsToProcess;
	size_t temp = myRank;
	if (numberOfProcesses*(myRank - 1) < pNumberReceive)
		pairsToProcess.push_back(numberOfProcesses*(myRank - 1));
	while (temp < pNumberReceive)
	{
		pairsToProcess.push_back(temp);
		temp += numberOfProcesses;
	}
	std::cout << "Getting trading info.." << std::endl;
	for (const auto& item: pairsToProcess)
	{
		ExportData* pMyData;
		std::cout << "Exporting test #" << item << ":" << std::string(tickerPairs[item].ticker1) << " " << std::string(tickerPairs[item].ticker2) << std::endl;
		auto dat = GetExportData(strategy, std::make_pair(tickerPairs[item].ticker1, tickerPairs[item].ticker2), types...);
		Retry:
		try
		{
			std::cout << "Got some results.." << std::endl;
			pMyData = std::get<0>(dat).data();
			MPI_Datatype CONTIGUOUS_TYPE = DeclareArray(exportType, std::get<0>(dat).size());
			std::cout << "Sending" << std::get<0>(dat).size() << "deals from process " << myRank << " to process #0" << std::endl;
			MPI_Send(pMyData, 1, CONTIGUOUS_TYPE, 0, tag, MPI_COMM_WORLD);
			std::cout << "Sent data from process " << myRank<< std::endl;
			FreeDataType(&CONTIGUOUS_TYPE);
		}
		catch (std::exception& ex)
		{
			std::cout << "MPI exception while sending.." << std::endl;
			goto Retry;
		}

	}
	std::cout << "Process " << myRank << " is done and has exported " << pairsToProcess.size() << "pairs" << std::endl;
	MPI_Barrier(MPI_COMM_WORLD);
}


#endif /* SLAVEBALANCEDROLE_H_ */
