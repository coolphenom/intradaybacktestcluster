/*
 * AbstractStrategy.cpp
 *
 *  Created on: Mar 14, 2019
 *      Author: pavlotkachenko
 */

#include "AbstractStrategy.h"

namespace IntradayBacktest
{

template <class T>
AbstractStrategy<T>::AbstractStrategy(const double& fee, const double& stopLoss, const double& takeProfit) {
	m_tradingProxy = new TradingProxy(fee, stopLoss, takeProfit);
}

template <class T>
const std::vector<std::tuple<std::string, int, int, int, int, double, double, double>>& AbstractStrategy<T>::DealsList() const
{
	return m_tradingProxy->DealsList();
}

template <class T>
double AbstractStrategy<T>::ProfitFactor() const
{
	return m_tradingProxy->GetProfitFactor();
}

template <class T>
std::vector<double> AbstractStrategy<T>::ProfitFactors() const
{
	return m_tradingProxy->GetProfitFactors();
}

/*template <typename ... Types>
void AbstractStrategy::DeclareFilter(const AbstractFilterEnum& filter, Types... args)
{
	switch (filter)
	{
	case AbstractFilterEnum::BollingerBands:
	{
		args.
		abstractFilter = BollingerBands()
	}
	break;
	case AbstractFilterEnum::DonchianChannel:
	{

	}
	break;
	default:
		break;
	}
}*/

template <class T>
const std::vector<std::string>& AbstractStrategy<T>::GetTickers() const
{
	return m_tickers;
}

template <class T>
void AbstractStrategy<T>::GetMainCurrency(const std::string& ticker)
{
	std::vector<std::string> keyWords = {"btc","usdt","eth","bnb","usdc","usds","pax"};
	std::string mainCur;
	std::string temp;
	for (const auto& keyWord : keyWords)
	{
		boost::smatch result;
		std::string regPattern = "(\\D+)(" + keyWord + ")";
		boost::regex regEx(regPattern);
		if (boost::regex_match(ticker, result, regEx))
		{
			temp = keyWord;
			break;
		}
	}
	if (mainCur.empty())
	{
		mainCur = temp;
	}
	else
	{
		if (mainCur != temp)
		{
			m_mainCurrency = "";
			return;
		}
	}
	m_mainCurrency = mainCur;
}

template <class T>
double AbstractStrategy<T>::CalculateMoneyEquivalent(const int& day)
{
	int d = day;
	if (m_mainCurrency != "" && m_mainCurrency != "usdt" && m_mainCurrency != "pax" &&
			m_mainCurrency != "usds" && m_mainCurrency != "usdc")
	{
		double val = m_mainDailyCurrencyQuotes[d];
		if (val == 0)
		{
			while (val == 0)
			{
				d--;
				val = m_mainDailyCurrencyQuotes[d];
			}
		}
		return 10000/val;
	}
	else
	{
		return 10000;
	}
}

template <class T>
bool AbstractStrategy<T>::CheckSecondaryFilter(const int& time)
{
	bool result = true;
	std::for_each(this->secondaryFilters.begin(), this->secondaryFilters.end(),[&result,&time](AbstractFilter* filter) {result *= filter->IsEnabled(time);});
	return result;
}

template <class T>
AbstractStrategy<T>::~AbstractStrategy() {
	delete m_tradingProxy;
}

template class AbstractStrategy<double>;
template class AbstractStrategy<IntradayMaintenance::StockInfo>;
}



