/*
 * DispatcherBalancedRole.cpp
 *
 *  Created on: Jan 26, 2020
 *      Author: root
 */

#include "DispatcherBalancedRole.h"

void MPIDispatcherBalancedRole(const std::string& database, const boost::gregorian::date& start,
				MPI_Datatype tickerType, MPI_Datatype exportType, const int& numOfProcesses, StrategyEnum strEnum)
{
	SIGNALEXCEPTION(signalHandler);
	function = "mpi_dispatcher_loop";
	MPI_Status status;
	int tag=0;
	size_t numberOfPairs;
	std::cout<< "Hello from rank 1 Dispatcher" << std::endl;
	size_t completed = 0;
	//Dispatcher
	TickerPair* pTickerData;
	std::vector<TickerPair> pairs = GetPairs(database, start, strEnum);
	std::cout << "There are " << pairs.size() << " samples in this test" << std::endl;
	std::deque<TickerPair> deque(pairs.begin(),pairs.end());
	numberOfPairs = deque.size();
	MPI_Barrier(MPI_COMM_WORLD);
	//
	//
	MPI_Send(&numberOfPairs, 1, MPI_UINT32_T, 0, tag, MPI_COMM_WORLD);
	for (size_t i = 2; i < numOfProcesses; ++i)
	{
		MPI_Send(&numberOfPairs, 1, MPI_UINT32_T, i, tag, MPI_COMM_WORLD);
	}
	std::cout << "Dispatcher has sent initial information.." << std::endl;
	std::queue<TickerPair> queue(deque);
	MPI_Barrier(MPI_COMM_WORLD);
	//MPI_Recv(&tickerPairs, pNumberReceive, tickerType, 0, tag, MPI_COMM_WORLD, &status);
	while (queue.size() > 0)
	{
		std::cout << "Dispatcher is receiving requests..." << std::endl;
		size_t senderRank = 0;
		MPI_Recv(&senderRank, 1, MPI_UINT32_T, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
		std::cout << "Dispatcher has received request from " << senderRank << std::endl;
		auto pos = queue.front();
		MPI_Send(&pos, 1, tickerType, senderRank, tag, MPI_COMM_WORLD);
		queue.pop();
		auto newSize = queue.size();
		MPI_Send(&newSize, 1, MPI_UINT32_T, senderRank, tag, MPI_COMM_WORLD);
	}
	std::cout << "Dispatcher has run out of tasks" << std::endl;
	int processesDone = 0;
	for (size_t i = 0; i < numOfProcesses; ++i)
	{
		if (processesDone == numOfProcesses -2)
			break;
		std::cout <<"Dispatcher is waiting for a final request..." << std::endl;
		size_t senderRank;
		MPI_Recv(&senderRank, 1, MPI_UINT32_T, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
		std::cout <<"Dispatcher is sending a final request..." << std::endl;
		++processesDone;
		TickerPair item("test","test");
		MPI_Send(&item, 1, tickerType, senderRank, tag, MPI_COMM_WORLD);
		size_t queueSize = 0;
		MPI_Send(&queueSize, 1, MPI_UINT32_T, senderRank, tag, MPI_COMM_WORLD);
	}
	std::cout << "Dispatcher has completed his job" << std::endl;
	MPI_Barrier(MPI_COMM_WORLD);
}

void MPIDispatcherRole(const std::string& database, const boost::gregorian::date& start,
			MPI_Datatype tickerType, MPI_Datatype exportType, const int& numOfProcesses, std::vector<std::vector<ExportData>>& exportedData)
{
	MPI_Status status;
	int tag=0;
	size_t* p_numberOfPairs;
	std::cout<< "Hello from rank 0" << std::endl;
	size_t completed = 0;
	//Dispatcher
	TickerPair* pTickerData;
	std::vector<TickerPair> pairs = GetPairs(database,start);
	size_t s = pairs.size();
	p_numberOfPairs = &s;
	pTickerData = pairs.data();
	MPI_Datatype CONTIGUOUS_TYPE = DeclareArray(tickerType, pairs.size());
	for (size_t i = 1; i < numOfProcesses; ++i)
	{
		std::cout << "Sending info to rank " << i << std::endl;
		MPI_Send(p_numberOfPairs, 1, MPI_UINT32_T, i, tag, MPI_COMM_WORLD);
		MPI_Send(pTickerData, 1, CONTIGUOUS_TYPE, i, tag, MPI_COMM_WORLD);
	}
	std::cout << "Info sent to all processes.." << std::endl;
	FreeDataType(&CONTIGUOUS_TYPE);
	while (completed < *p_numberOfPairs)
	{
		function = "mpi_dispatcher_loop";
		SIGNALEXCEPTION(signalHandler);
		try
		{
			std::cout << "Receiving info from various processes.." << std::endl;
			int count = 0;
			ExportData myDataExported[10000];
			MPI_Recv(&myDataExported, 10000, exportType, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
			MPI_Get_count(&status, exportType, &count);
			std::cout << "Received " << count <<" deals from unknown process" << std::endl;
			ExportData d[count];
			for (size_t i = 0; i < count; ++i)
			{
				d[i] = myDataExported[i];
			}
			//TODO
			std::vector<ExportData> data(d, d+count);
			if (data.size() > 0)
			{
				std::string cur = GetMainCurrency(data[0].ticker);
				if (cur == "btc")
				{
					if (data.size() > m_minDealsBtc)
						exportedData.push_back(data);
				}
				else if (cur == "eth")
				{
					if (data.size() > m_minDealsEth)
						exportedData.push_back(data);
				}
				else if (cur == "bnb")
				{
					if (data.size() > m_minDealsBnb)
						exportedData.push_back(data);
				}
				else
				{
					if (data.size() > m_minDealsUsdt)
						exportedData.push_back(data);
				}
			}


			++completed;
			std::cout << "Exported " << completed <<"/" << *p_numberOfPairs << std::endl;
		}
		catch (std::exception& ex)
		{
			std::cout << "MPI exception while receiving.." << std::endl;
		}

	}
	MPI_Barrier(MPI_COMM_WORLD);
}




