/*
 * JohansenTest.cpp
 *
 *  Created on: Mar 18, 2019
 *      Author: pavlotkachenko
 */

#include "JohansenTest.h"
#include "../IntradayMaintenance/CustomExceptionHandler.h"

#include <algorithm>
#include <numeric>
#include <cmath>
#include <string>
#include <stdexcept>
#include <iostream>
#include <csignal>

#define JOHTESTEXCEPTION SIGNALEXCEPTION(signalHandler)

namespace{

double LinearCombination(const std::vector<std::vector<std::tuple<int,int,double>>>& info, const size_t& ind, const std::vector<double>& coefs)
{
	double val = 0;
	for (size_t i = 0; i < coefs.size(); ++i)
	{
		val += std::get<2>(info[i][ind])*coefs[i];
	}
	return val;
}

size_t gsl_matrix_get_size1(gsl_matrix* m)
{
	return m->size1;
}

size_t gsl_matrix_get_size2(gsl_matrix* m)
{
	return m->size2;
}

size_t gsl_vector_complex_getsize(gsl_vector_complex* v)
{
	return v->size;
}

size_t gsl_matrix_complex_getsize1(gsl_matrix_complex* v)
{
	return v->size1;
}

size_t gsl_matrix_complex_getsize2(gsl_matrix_complex* v)
{
	return v->size2;
}

gsl_vector* gsl_vector_get_real(gsl_vector_complex* v)
{
	gsl_vector* vAlloc = gsl_vector_alloc(v->size);
	vAlloc->data = v->data;
	vAlloc->stride = v->stride * 2;
	vAlloc->size = v->size;
	return vAlloc;
}

//
void MatrixDivideByElement(gsl_matrix* xMat, const double& val)
{
	size_t nr = gsl_matrix_get_size1(xMat);
	size_t nc = gsl_matrix_get_size2(xMat);
	gsl_matrix* tmpMat = gsl_matrix_alloc(nr, nc);
	gsl_matrix_set_all(tmpMat, val);
	gsl_matrix_div_elements(xMat, tmpMat);
	gsl_matrix_free(tmpMat);
}

//
gsl_matrix* MatrixInverse(gsl_matrix* inMat, const bool& write = false)
{
	function = __func__;
	JOHTESTEXCEPTION;
	double tolerance = 1e-36;
	size_t size1 = gsl_matrix_get_size1(inMat);
	gsl_matrix* invert_me = gsl_matrix_alloc(size1, size1);
	gsl_matrix* outMat = gsl_matrix_alloc(size1, size1);
	gsl_permutation* perm = gsl_permutation_alloc(size1);
	int sign;
	gsl_matrix_memcpy(invert_me, inMat);
	gsl_linalg_LU_decomp(invert_me, perm, &sign);
	gsl_set_error_handler_off();
	double det = gsl_linalg_LU_det(invert_me, sign);
	if (det > tolerance)
	{
		try
		{
			gsl_linalg_LU_invert(invert_me, perm, outMat);
		}
		catch (std::exception& ex)
		{
			std::cout << "Error while inverting" << std::endl;
		}
		gsl_matrix_free(invert_me);
		gsl_permutation_free(perm);

		return outMat;
	}
	else
	{
		std::cout << "Matrix is singular" << std::endl;
		gsl_matrix_free(invert_me);
		gsl_permutation_free(perm);
		throw std::runtime_error("runtime_error");
	}
}

//
gsl_matrix* MatrixTransposeImpl(gsl_matrix* m)
{
	function = __func__;
	JOHTESTEXCEPTION;
	try
	{
		size_t nr = gsl_matrix_get_size1(m);
		size_t nc = gsl_matrix_get_size2(m);
		gsl_matrix* retmat = gsl_matrix_alloc(nc, nr);
		gsl_matrix_transpose_memcpy(retmat, m);
		return retmat;
	}
	catch (std::exception& ex)
	{
		std::cout << "Exception while transposing" << std::endl;
	}

}

//
gsl_matrix* MatrixMultiply(gsl_matrix* A, gsl_matrix* B, const bool& deallocateA, const bool& deallocateB, const bool& write = false)
{
	function = __func__;
	JOHTESTEXCEPTION;
	try
	{
		size_t nrA = gsl_matrix_get_size1(A);
		size_t ncB = gsl_matrix_get_size2(B);
		gsl_matrix* resMat = gsl_matrix_alloc(nrA, ncB);

		gsl_blas_dgemm(CBLAS_TRANSPOSE_t::CblasNoTrans, CBLAS_TRANSPOSE_t::CblasNoTrans, 1.0, A, B, 0.0, resMat);
		if (deallocateA)
			gsl_matrix_free(A);
		if (deallocateB)
			gsl_matrix_free(B);
		return resMat;
	}
	catch (std::exception& ex)
	{
		std::cout << "Exception while multiplying matrices" << std::endl;
	}

}

//
gsl_matrix* MatrixDivide(gsl_matrix* xMat, gsl_matrix* yMat, const bool& deallocateX, const bool& deallocateY)
{
	function = __func__;
	JOHTESTEXCEPTION;
	gsl_matrix* xTranspose = MatrixTransposeImpl(xMat);
	gsl_matrix* tmp1 = MatrixMultiply(xTranspose, xMat, false, false);
	try
	{
		gsl_matrix* tmp2 = MatrixInverse(tmp1);
		gsl_matrix* tmp3 = MatrixMultiply(tmp2, xTranspose, false, false);
		gsl_matrix* tmp4 = MatrixMultiply(tmp3, yMat, false, false);
		gsl_matrix_free(tmp3);
		gsl_matrix_free(tmp2);
		gsl_matrix_free(tmp1);
		gsl_matrix_free(xTranspose);
		if (deallocateX)
			gsl_matrix_free(xMat);
		if (deallocateY)
			gsl_matrix_free(yMat);
		return tmp4;
	}
	catch (std::exception& ex)
	{
		if (deallocateX)
			gsl_matrix_free(xMat);
		if (deallocateY)
			gsl_matrix_free(yMat);
		gsl_matrix_free(xTranspose);
		gsl_matrix_free(tmp1);
		std::cout << "Exception while dividing matrices" << std::endl;
		throw std::runtime_error("runtime error");
	}

}


//
std::vector<double> GslComplexToAbsVector(gsl_vector_complex* x)
{
	size_t n = gsl_vector_complex_getsize(x);
	std::vector<double> retvec(n);
	gsl_vector* real = gsl_vector_get_real(x);
	for (size_t i = 0; i < n; ++i)
	{
		retvec[i] = gsl_vector_get(real,i);
	}
	gsl_vector_free(real);
	return retvec;
}

//
std::vector<std::vector<double>> GslToMatrix(gsl_matrix* x)
{
	size_t nr = gsl_matrix_get_size1(x);
	size_t nc = gsl_matrix_get_size2(x);
	std::vector<std::vector<double>> retmat(nr);
	for (size_t i = 0; i < nr; ++i)
	{
		std::vector<double> r(nc);
		for (size_t j = 0; j < nc; ++j)
		{
			r[j] = gsl_matrix_get(x, i, j);
		}
		retmat[i] = r;
	}
	return retmat;
}

//
std::vector<std::vector<double>> GslComplexToAbsMatrix(gsl_matrix_complex* x)
{
	function = __func__;
	JOHTESTEXCEPTION;
	try
	{
		size_t nr = gsl_matrix_complex_getsize1(x);
		size_t nc = gsl_matrix_complex_getsize2(x);
		std::vector<std::vector<double>> retmat(nc);
		for (size_t j = 0; j < nc; ++j)
		{
			std::vector<double> ret(nr);
			gsl_vector_complex* complAlloc = gsl_vector_complex_alloc(nr);
			gsl_matrix_complex_get_row(complAlloc, x, j);
			gsl_vector* vecAlloc = gsl_vector_get_real(complAlloc);
			for(size_t i = 0; i < nr; ++i)
			{
				ret[i] = gsl_vector_get(vecAlloc, i);
			}
			gsl_vector_free(vecAlloc);
			gsl_vector_complex_free(complAlloc);
			retmat[j] = ret;
		}
		return retmat;
	}
	catch (std::exception& ex)
	{
		std::cout << "Exception while converting from complex to absolute" << std::endl;
	}

}

//
gsl_matrix* MatrixToGslMatrix(const std::vector<std::vector<double>>& inMat)
{
	size_t nc = inMat.size();
	size_t nr = inMat[0].size();
	gsl_matrix* x = gsl_matrix_alloc(nr, nc);
	for (size_t i = 0; i < nc; ++i)
	{
		std::vector<double> temp = inMat[i];
		for (size_t j = 0; j < nr; ++j)
		{
			gsl_matrix_set(x, j, i, temp[j]);
		}
	}
	return x;
}

}

namespace IntradayBacktest {

enum TestResult
{
	LessThan90 = 0,
	From90To95 = 1,
	From95To99 = 2,
	MoreThan99 = 3
};

struct MaxEigenData
{
	size_t No;
	double TestStatistic;
	double CriticalValue90;
	double CriticalValue95;
	double CriticalValue99;
	TestResult Result;
};

JohansenTest::JohansenTest(const std::vector<std::vector<std::tuple<int,int,double>>>& values, const size_t& start, const size_t& finish, const int& lags) :
		m_nLags(lags),
		m_success(false){
	// TODO Auto-generated constructor stub
	PerformTest(values, start, finish);
	DeclareSpread(m_coeffs, values);
}

const bool& JohansenTest::Success() const
{
	return m_success;
}

JohansenTest::~JohansenTest() {
	// TODO Auto-generated destructor stub
}

const std::vector<double>& JohansenTest::Coefficients() const
{
	return m_coeffs;
}

const std::vector<std::tuple<int, int, double>>& JohansenTest::Spread() const
{
	return m_spread;
}

void JohansenTest::PerformTest(const std::vector<std::vector<std::tuple<int,int,double>>>& values, const size_t& start, const size_t& finish)
{
	std::vector<std::vector<double>> vals(values.size());
	for (size_t i = 0; i < values.size(); ++i)
	{
		std::vector<double> selected;
		for (size_t j = start; j < finish; ++j)
		{
			selected.push_back(std::get<2>(values[i][j]));
		}
		vals[i] = selected;
	}
	auto res = PerformMaxEigenvalueTest(vals, m_nLags);
	m_coeffs = std::get<1>(res);
	if (!std::get<2>(res))
	{
		std::cout <<"Test failed!" << std::endl;
	}
	m_success = true;
}

//
std::vector<std::vector<double>> JohansenTest::DeMean(const std::vector<std::vector<double>>& xMat)
{
	std::vector<std::vector<double>> retList(xMat.size());
	for (size_t i = 0; i < xMat.size(); ++i)
	{
		const std::vector<double>& currentRow = xMat[i];
		std::vector<double> row;
		double average = (double)std::accumulate(currentRow.begin(), currentRow.end(), 0.0)/currentRow.size();
		for (auto& item : currentRow)
			row.push_back(item - average);
		retList[i] = row;
	}
	return retList;
}

//
std::vector<std::vector<double>> JohansenTest::GetMatrixDifference(const std::vector<std::vector<double>>& xMat)
{
	std::vector<std::vector<double>> retList(xMat.size());
	for (size_t i = 0; i < xMat.size(); ++i)
	{
		const std::vector<double>& current = xMat[i];
		std::vector<double> newSeries(current.size() - 1);
		for (size_t j = 0; j < newSeries.size(); ++j)
			newSeries[j] = current[j+1]-current[j];
		retList[i] = newSeries;
	}
	return retList;
}

//
std::vector<std::vector<double>> JohansenTest::GetMatrixLagged(const std::vector<std::vector<double>>& xMat, const size_t& lags)
{
	std::vector<std::vector<double>> retMat;
	size_t nCols = xMat.size()*lags;
	size_t counter = 0;
	size_t counter2 = 0;
	for (size_t i = 0; i < nCols; ++i)
	{
		const std::vector<double>& current = xMat[counter2];
		std::vector<double> newSeries(current.size() - lags);
		for (size_t j = 0; j < newSeries.size(); ++j)
		{
			newSeries[j] = current[j + lags - counter - 1];
		}
		retMat.push_back(newSeries);
		++counter;
		if (counter >= lags)
		{
			counter = 0;
			++counter2;
		}
	}
	return retMat;
}

//
std::vector<std::vector<double>> JohansenTest::GetSubMatrix(const std::vector<std::vector<double>>& xMat, int beginRow, int endRow, int beginCol, int endCol)
{
	if (beginRow == -1) beginRow = 0;
	if (endRow == -1) endRow =xMat[0].size() - 1;
	if (beginCol == -1) beginCol = 0;
	if (endCol == -1) endCol = xMat.size() - 1;
	std::vector<std::vector<double>> retList;
	for (size_t i = beginCol; i <= endCol; ++i)
	{
		const std::vector<double>& current = xMat[i];
		std::vector<double> newSeries(endRow - beginRow + 1);
		for (size_t j = 0; j < newSeries.size(); ++j)
			newSeries[j] = current[j + beginRow];
		retList.push_back(newSeries);
	}
	return retList;
}

std::tuple<double, std::vector<double>,bool> JohansenTest::PerformMaxEigenvalueTest(const std::vector<std::vector<double>>& values, const size_t& nLags)
{
	function = __func__;
	JOHTESTEXCEPTION;
	try
	{
		std::vector<std::vector<double>> xMat = DeMean(values);
			auto dxMat = GetMatrixDifference(xMat);
			auto dxLaggedMatrix = GetMatrixLagged(dxMat, nLags);
			auto dxLaggedDemeanedMatrix = DeMean(dxLaggedMatrix);
			auto dxDemeanedMatrix = DeMean(GetSubMatrix(dxMat,nLags, -1, -1, -1));
			size_t nrx = dxLaggedDemeanedMatrix[0].size();
			size_t ncx = dxLaggedDemeanedMatrix.size();
			size_t nry = dxDemeanedMatrix[0].size();
			size_t ncy = dxDemeanedMatrix.size();

			gsl_matrix* tmp1 = MatrixDivide(MatrixToGslMatrix(dxLaggedDemeanedMatrix), MatrixToGslMatrix(dxDemeanedMatrix), true, true);
			nrx = dxLaggedDemeanedMatrix[0].size();
			ncy = dxDemeanedMatrix.size();
			gsl_matrix* fittedRegressionDX = MatrixMultiply(MatrixToGslMatrix(dxLaggedDemeanedMatrix), tmp1, true, false);

			size_t nr = dxDemeanedMatrix[0].size();
			size_t nc = dxDemeanedMatrix.size();

			gsl_matrix* residualRegressionDX = MatrixToGslMatrix(dxDemeanedMatrix);
			gsl_matrix_sub(residualRegressionDX, fittedRegressionDX);

			nrx = xMat[0].size();
			auto tmp4 = GetSubMatrix(xMat, 1, nrx - nLags - 1, -1, -1);
			auto xDemeanedMatrix = DeMean(tmp4);

			gsl_matrix* tmp6 = MatrixDivide(MatrixToGslMatrix(dxLaggedDemeanedMatrix),MatrixToGslMatrix(xDemeanedMatrix), true, true);

			gsl_matrix* fittedRegressionX = MatrixMultiply(MatrixToGslMatrix(dxLaggedDemeanedMatrix), tmp6, true, false);
			gsl_matrix* residualsRegressionX = MatrixToGslMatrix(xDemeanedMatrix);
			gsl_matrix_sub(residualsRegressionX, fittedRegressionX);

			gsl_matrix* tmp9 = MatrixMultiply(MatrixTransposeImpl(residualsRegressionX), residualsRegressionX, true, false);
			MatrixDivideByElement(tmp9, (double)gsl_matrix_get_size1(residualsRegressionX));
			gsl_matrix* Skk = tmp9;

			gsl_matrix*	tmp10 = MatrixMultiply(MatrixTransposeImpl(residualsRegressionX), residualRegressionDX, true, false);
			MatrixDivideByElement(tmp10, (double)gsl_matrix_get_size1(residualsRegressionX));
			gsl_matrix* Sk0 = tmp10;

			gsl_matrix*	tmp11 = MatrixMultiply(MatrixTransposeImpl(residualRegressionDX), residualRegressionDX, true, false);
			MatrixDivideByElement(tmp11, (double)gsl_matrix_get_size1(residualRegressionDX));
			gsl_matrix* S00 = tmp11;

			auto t1 = MatrixMultiply(Sk0, MatrixInverse(S00, true), false, true);
			auto t2 = MatrixMultiply(t1,MatrixTransposeImpl(Sk0), true, true);
			auto t3 = MatrixInverse(Skk);
			gsl_matrix* eigenInputMatrix = MatrixMultiply(t3, t2, true, true);


			size_t n = gsl_matrix_get_size1(eigenInputMatrix);

			gsl_vector_complex* evalPtr = gsl_vector_complex_alloc(n);
			gsl_matrix_complex* ematPtr = gsl_matrix_complex_alloc(n,n);
			auto wrkSpaceptr = gsl_eigen_nonsymmv_alloc(n);
			gsl_eigen_nonsymmv(eigenInputMatrix, evalPtr, ematPtr, wrkSpaceptr);
			gsl_eigen_nonsymmv_free(wrkSpaceptr);

			gsl_eigen_nonsymmv_sort(evalPtr, ematPtr, gsl_eigen_sort_t::GSL_EIGEN_SORT_ABS_DESC);

			auto eigenValuesVec = GslComplexToAbsVector(evalPtr);
			auto eigenVecMatrix = GslComplexToAbsMatrix(ematPtr);

			size_t nSamples = gsl_matrix_get_size1(residualsRegressionX);
			size_t nVariables = gsl_matrix_get_size2(residualsRegressionX);
			std::vector<MaxEigenData> outStats;
			size_t counter = 0;
			for (size_t i = 0; i < eigenValuesVec.size(); ++i)
			{
				MaxEigenData eigData;
				eigData.No = i;
				double LRmaxeigenvalue = - nSamples*std::log(1.0 - eigenValuesVec[i]);
				eigData.TestStatistic = LRmaxeigenvalue;
				eigData.CriticalValue90 = maxEigenCriticalValue[nVariables - counter - 1][0];
				eigData.CriticalValue95 = maxEigenCriticalValue[nVariables - counter - 1][1];
				eigData.CriticalValue99 = maxEigenCriticalValue[nVariables - counter - 1][2];
				if (LRmaxeigenvalue < eigData.CriticalValue90)
					eigData.Result = TestResult::LessThan90;
				else if (LRmaxeigenvalue < eigData.CriticalValue95)
					eigData.Result = TestResult::From90To95;
				else if (LRmaxeigenvalue < eigData.CriticalValue99)
					eigData.Result = TestResult::From95To99;
				else
					eigData.Result = TestResult::MoreThan99;
				++counter;
				outStats.push_back(eigData);
			}
			std::vector<double> eigenvector(nc);
			for (size_t i = 0; i < eigenvector.size(); ++i)
			{
				eigenvector[i] = eigenVecMatrix[i][0];
			}
			eigenValuesVec.clear();
			eigenVecMatrix.clear();
			double testStat = outStats[0].TestStatistic;
			xDemeanedMatrix.clear();
			tmp4.clear();
			dxMat.clear();
			dxLaggedMatrix.clear();
			dxLaggedDemeanedMatrix.clear();
			dxDemeanedMatrix.clear();
			xMat.clear();
			gsl_matrix_free(S00);
			gsl_matrix_free(Sk0);
			gsl_matrix_free(Skk);
			gsl_matrix_free(fittedRegressionX);
			gsl_matrix_free(fittedRegressionDX);
			gsl_matrix_free(residualsRegressionX);
			gsl_matrix_free(residualRegressionDX);
			gsl_matrix_free(tmp1);
			gsl_matrix_free(tmp6);
			gsl_vector_complex_free(evalPtr);
			gsl_matrix_complex_free(ematPtr);
			gsl_matrix_free(eigenInputMatrix);
			return std::make_tuple(testStat, eigenvector, true);
	}
	catch (std::exception& ex)
	{
		std::vector<double> v = {1,1};
		std::cout << "Exception while carrying out eigenvalue test" << std::endl;
		return std::make_tuple(0, v ,false);
	}

}

void JohansenTest::DeclareSpread(const std::vector<double>& coefficients, const std::vector<std::vector<std::tuple<int,int,double>>>& info)
{
	m_spread = std::vector<std::tuple<int,int,double>>(info[0].size());
	for (size_t i = 0; i < info[0].size(); ++i)
	{
		double val = LinearCombination(info, i, coefficients);
		m_spread[i] = std::make_tuple(std::get<0>(info[0][i]),std::get<1>(info[0][i]), val);
	}
}

} /* namespace IntradayBacktest */
