/*
 * JohansenTest.h
 *
 *  Created on: Mar 18, 2019
 *      Author: pavlotkachenko
 */

#ifndef JOHANSENTEST_H_
#define JOHANSENTEST_H_

#include <cstdint>
#include <vector>
#include <tuple>
#include <utility>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_matrix_complex_double.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_vector_complex.h>

namespace
{
	void MatrixDivideByElement(gsl_matrix* xMat, const double& val);
	gsl_matrix* MatrixDivide(gsl_matrix* xMat, gsl_matrix* yMat, const bool& deallocateX, const bool& deallocateY);
	gsl_matrix* MatrixInverse(gsl_matrix* inMat, const bool& write);
	std::vector<double> GslComplexToAbsVector(gsl_vector_complex* x);
	std::vector<std::vector<double>> GslToMatrix(gsl_matrix* x);
	std::vector<std::vector<double>> GslComplexToAbsMatrix(gsl_matrix_complex* x);
	gsl_matrix* MatrixToGslMatrix(const std::vector<std::vector<double>>& inMat);
	gsl_matrix* MatrixTransposeImpl(gsl_matrix* m);
	gsl_matrix* MatrixMultiply(gsl_matrix* A, gsl_matrix* B, const bool& deallocateA, const bool& deallocateB, const bool& write);

	//gsl functions
	size_t gsl_matrix_get_size1(gsl_matrix* m);
	size_t gsl_matrix_get_size2(gsl_matrix* m);
	size_t gsl_vector_complex_getsize(gsl_vector_complex* v);
	size_t gsl_matrix_complex_getsize1(gsl_matrix_complex* v);
	size_t gsl_matrix_complex_getsize2(gsl_matrix_complex* v);
	gsl_vector* gsl_vector_get_real(gsl_vector_complex* v);
}

namespace IntradayBacktest {


class JohansenTest {
public:
	JohansenTest(const std::vector<std::vector<std::tuple<int,int,double>>>& values, const size_t& start, const size_t& finish, const int& lags);
	const std::vector<std::tuple<int, int, double>>& Spread() const;
	const std::vector<double>& Coefficients() const;
	const bool& Success() const;
	virtual ~JohansenTest();
private:
	std::vector<double> m_coeffs;
	int m_nLags;
	bool m_success;
	std::vector<std::tuple<int, int, double>> m_spread;
	std::vector<std::vector<double>> DeMean(const std::vector<std::vector<double>>& xMat);
	std::vector<std::vector<double>> GetMatrixDifference(const std::vector<std::vector<double>>& xMat);
	std::vector<std::vector<double>> GetMatrixLagged(const std::vector<std::vector<double>>& xMat, const size_t& lags);
	std::vector<std::vector<double>> GetSubMatrix(const std::vector<std::vector<double>>& xMat, int beginRow, int endRow, int beginCol, int endCol);
	void PerformTest(const std::vector<std::vector<std::tuple<int,int,double>>>& values, const size_t& start, const size_t& finish);
	std::tuple<double, std::vector<double>,bool> PerformMaxEigenvalueTest(const std::vector<std::vector<double>>& values, const size_t& nLags);
	void DeclareSpread(const std::vector<double>& coefficients, const std::vector<std::vector<std::tuple<int,int,double>>>& info);
	const std::vector<std::vector<double>> maxEigenCriticalValue =
	{
			{2.7055, 3.8415, 6.6349},
			{12.2971, 14.2639, 18.52},
			{18.8928, 21.1314, 25.865},
			{25.1236, 27.5858, 32.7172},
			{31.2379, 33.8777, 39.3693},
			{37.2786, 40.0763, 45.8662},
			{43.2947, 46.2299, 52.3069},
			{49.2855, 52.3622, 58.6634},
			{55.2412, 58.4332, 64.996},
			{61.2041, 64.504, 71.2525},
			{67.1307, 70.5392, 77.4877},
			{73.0563, 76.5734, 83.7105}
	};
};

} /* namespace IntradayBacktest */

#endif /* JOHANSENTEST_H_ */
