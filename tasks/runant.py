#!/usr/bin/python
import os
import re

import xml.etree.ElementTree

def read_packages(paths):
    packages = []
    for path in paths:
        files = os.listdir(path)
        basename = os.path.basename(path)
        regex_pattern = "^("+basename+")-(.+)-(.+)-(.+)(.rpm)$"
        for file in files:
            m = re.match(regex_pattern, file)
            if (m) :
                packages.append(basename + "-" + m.group(2))
            else :
                pattern = "^("+basename+")-(.+)-(.+)(.rpm)$"
                m = re.match(pattern, file)
                if (m) :
                    packages.append(basename + "-" + m.group(2))
                else :
                    pattern = "^(.+)-(.+)-(.+)(.rpm)$"
                    m = re.match(pattern, file)
                    if (m) :
                        packages.append(m.group(1))
    return packages


def read_xml_file(filename, list_of_packages):
    root_path = os.path.abspath(os.path.join(filename, os.pardir))
    et = xml.etree.ElementTree.parse(filename)
    root = et.getroot()
    for package in list_of_packages:
        pack = xml.etree.ElementTree.SubElement(root, "package")
        pack.text = package
        pack.tail = "\n";
    for child in root:
        if child.tag == "post":
            root.remove(child)
            root.append(child)
            break
    et.write("/export/rocks/install/site-profiles/7.0/nodes/extend-compute.xml")

paths = ["/root/Desktop/boost", "/root/Desktop/gsl"]
read_xml_file("/export/rocks/install/site-profiles/7.0/nodes/extend-compute.xml",read_packages(paths))
